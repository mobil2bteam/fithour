//
//  PRVSupportViewController.h
//  FitnessApp
//
//  Created by Ruslan on 11/2/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PRVSupportViewController : UIViewController
@property (assign, nonatomic) BOOL isFeedback;
@property (nonatomic, assign) NSInteger rating;
@end
