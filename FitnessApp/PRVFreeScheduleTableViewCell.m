//
//  PRVFreeScheduleTableViewCell.m
//  FitnessApp
//
//  Created by Ruslan on 10/27/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVFreeScheduleTableViewCell.h"
#import "PRVOptionsServerModel.h"
#import "PRVHoursCollectionViewCell.h"

static NSString * const kCollectionViewCellIdentifier = @"PRVHoursCollectionViewCell";

@interface PRVFreeScheduleTableViewCell() <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, strong) PRVScheduleServerModel *schedule;

@end

@implementation PRVFreeScheduleTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.hoursCollectionView registerNib:[PRVHoursCollectionViewCell prv_nib] forCellWithReuseIdentifier:kCollectionViewCellIdentifier];
    ((UICollectionViewFlowLayout *)self.hoursCollectionView.collectionViewLayout).itemSize = CGSizeMake(100, 50);
    self.hoursCollectionView.dataSource = self;
    self.hoursCollectionView.delegate = self;
    self.dateLabel.textColor = [UIColor primaryColor];
}

- (void)configureCellForSchedule:(PRVScheduleServerModel *)schedule{
    self.schedule = schedule;
    self.dayLabel.text = [[schedule formattedDay] uppercaseString];
    self.dateLabel.text = [schedule formattedData];
    [self.hoursCollectionView reloadData];
}

- (void)prepareForReuse{
    [super prepareForReuse];
    self.schedule = nil;
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.schedule.hours.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    PRVHoursCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCollectionViewCellIdentifier forIndexPath:indexPath];
    PRVHourServerModel *hour = self.schedule.hours[indexPath.item];
    cell.hoursLabel.text = [NSString stringWithFormat:@"%@-%@", hour.from, hour.to];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (self.completionCallback) {
        self.completionCallback(self.schedule, self.schedule.hours[indexPath.item]);
    }
}
@end
