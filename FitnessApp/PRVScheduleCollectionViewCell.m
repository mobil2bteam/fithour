//
//  PRVSheduleCollectionViewCell.m
//  FitnessApp
//
//  Created by Ruslan on 7/31/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVScheduleCollectionViewCell.h"

@implementation PRVScheduleCollectionViewCell


- (void)prepareForReuse{
    [super prepareForReuse];
    self.coachView.hidden = YES;
    self.durationLabel.text = @"0 мин.";
    self.timeStartLabel.text = @"-:-";
    self.placesLabel.text = @"Нет";
}

@end
