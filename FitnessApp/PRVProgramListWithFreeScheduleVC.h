//
//  PRVProgramListWithFreeScheduleVC.h
//  FitnessApp
//
//  Created by Ruslan on 26.11.2017.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PRVDataSource;
@class PRVClubServerModel;
@class PRVGroupServerModel;

@interface PRVProgramListWithFreeScheduleVC : UIViewController
@property (nonatomic, strong) PRVClubServerModel *club;
@property (nonatomic, strong) PRVGroupServerModel *group;

@property (nonatomic, strong) PRVDataSource *daysCollectionViewDataSource;
@property (weak, nonatomic) IBOutlet UICollectionView *daysCollectionView;
@end
