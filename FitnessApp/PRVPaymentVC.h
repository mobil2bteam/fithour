//
//  PRVPaymentVC.h
//  FitnessApp
//
//  Created by Ruslan on 9/5/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PRVTicketSeverModel;

@interface PRVPaymentVC : UIViewController
@property (nonatomic, strong) PRVTicketSeverModel *ticket;
@property (nonatomic, copy) void (^paymentCompletion)(PRVTicketSeverModel *ticket);

@end
