//
//  PRVProfileVC.m
//  FitnessApp
//
//  Created by Ruslan on 7/20/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVProfileVC.h"
#import "PRVAppManager.h"
#import "PRVUserServerModel.h"
#import "PRVTicketListVC.h"
#import "PRVLogoutAlertVC.h"
#import "PRVChangePasswordAlertVC.h"
#import "PRVServerManager.h"

@interface PRVProfileVC ()

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *balanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UILabel *numberLabel;
@property (weak, nonatomic) IBOutlet UIButton *ticketsButton;

@end

@implementation PRVProfileVC

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationItem.title = @"Личный кабинет";
    [self.ticketsButton setTitleColor:[UIColor primaryColor] forState:UIControlStateNormal];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.nameLabel.text = [[PRVAppManager sharedInstance] getUser].info.name;
    self.emailLabel.text = [[PRVAppManager sharedInstance] getUser].info.email;
    self.numberLabel.text = [NSString stringWithFormat:@"№%@", [[PRVAppManager sharedInstance] getUser].info.number];
    self.balanceLabel.text = [NSString stringWithFormat:@"%ld руб.", (long)[[PRVAppManager sharedInstance] getUser].info.balance];
    [self loadTickets];
}

#pragma mark - Methods

- (void)loadTickets{
    self.ticketsButton.userInteractionEnabled = NO;
    __weak typeof(self) weakSelf = self;
    [[PRVServerManager sharedManager] loadTicketsOnSuccess:^{
        typeof(self) strongSelf = weakSelf;
        strongSelf.ticketsButton.userInteractionEnabled = YES;
    }];
}

#pragma mark - Actions

- (IBAction)myTicketsButtonPressed:(id)sender {
    PRVTicketListVC *vc = [[PRVTicketListVC alloc] prv_initWithNib];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)changePasswordButtonPressed:(id)sender {
    PRVChangePasswordAlertVC *vc = [[PRVChangePasswordAlertVC alloc] prv_initWithNib];
    vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:vc animated:YES completion:nil];
}

- (IBAction)exitButtonPressed:(id)sender {
    __weak typeof(self) weakSelf = self;
    PRVLogoutAlertVC *vc = [[PRVLogoutAlertVC alloc] prv_initWithNib];
    vc.logoutCallback = ^{
        typeof(self) strongSelf = weakSelf;
        [strongSelf.tabBarController setSelectedIndex:0];
    };
    vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:vc animated:YES completion:nil];
}

@end
