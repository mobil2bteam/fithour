//
//  PRVTicketCancelViewController.m
//  FitnessApp
//
//  Created by Ruslan on 21.02.2018.
//  Copyright © 2018 Ruslan Palapa. All rights reserved.
//

#import "PRVTicketCancelViewController.h"

@interface PRVTicketCancelViewController ()

@end

@implementation PRVTicketCancelViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden = NO;
}

#pragma mark - Actions

- (IBAction)okButtonPressed:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
