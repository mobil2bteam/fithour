//
//  PRVCommentTableViewCell+Configuration.h
//  FitnessApp
//
//  Created by Ruslan on 8/25/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//
@class PRVReviewServerModel;

#import "PRVCommentTableViewCell.h"

@interface PRVCommentTableViewCell (Configuration)

- (void)configureCellForReview:(PRVReviewServerModel *)review;

@end
