//
//  PRVDataManager.m
//  FitnessApp
//
//  Created by Ruslan on 9/11/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVDataManager.h"
#import "PRVClubDataModel+CoreDataProperties.h"
#import <MagicalRecord/MagicalRecord.h>

@implementation PRVDataManager

- (BOOL)isExistClubForId:(NSInteger)clubId{
    PRVClubDataModel *existClub = [PRVClubDataModel MR_findFirstByAttribute:@"clubID" withValue:@(clubId)];
    return existClub != nil;
}

- (NSArray <PRVClubDataModel *> *)favoriteClubs{
    return [PRVClubDataModel MR_findAll];
}

- (void)removeClub:(PRVClubDataModel *)club completionBlock:(PRVDataSaveCompletionBlock)completionBlock{
    [club MR_deleteEntity];
    [[NSManagedObjectContext MR_defaultContext]MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError * _Nullable error) {
        if (error) {
            completionBlock(error);
        } else {
            completionBlock(nil);
        }
    }];
}

- (void)addClubWithId:(NSInteger)clubId name:(NSString *)name address:(NSString *)address rating:(CGFloat)rating image:(NSString *)image completionBlock:(PRVDataSaveCompletionBlock)completionBlock{
    PRVClubDataModel *club = [PRVClubDataModel MR_createEntity];
    club.clubID = clubId;
    club.image = image;
    club.address = address;
    club.rating = rating;
    club.name = name;
    [[NSManagedObjectContext MR_defaultContext]MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError * _Nullable error) {
        if (error) {
            completionBlock(error);
        } else {
            completionBlock(nil);
        }
    }];
}

@end
