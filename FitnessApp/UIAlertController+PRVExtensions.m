//
//  UIAlertController+PRVExtensions.m
//  FitnessApp
//
//  Created by Ruslan on 8/21/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "UIAlertController+PRVExtensions.h"

@implementation UIAlertController (PRVExtensions)

+ (UIAlertController *)prv_alertWithTitle:(NSString *)title message:(NSString *)message
                    cancelButtonTitle:(NSString *)cancelButtonTitle cancelBlock:(void (^)())cancelBlock{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:cancelButtonTitle style:UIAlertActionStyleDefault handler:cancelBlock ? ^(UIAlertAction *action) {
        cancelBlock();
    } : nil];
    [alertController addAction:action];
    return alertController;
}

- (void)prv_addActionWithTitle:(NSString *)title style:(UIAlertActionStyle)style actionBlock:(void (^)())actionBlock{
    UIAlertAction *action = [UIAlertAction actionWithTitle:title style:style handler:actionBlock];
    [self addAction:action];
}

@end
