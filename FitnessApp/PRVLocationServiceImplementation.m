//
//  PRVLocationServiceImplementation.m
//  FitnessApp
//
//  Created by Ruslan on 9/7/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVLocationServiceImplementation.h"
#import "PRVLocationServiceDelegate.h"

@interface PRVLocationServiceImplementation()

@property (nonatomic, strong) CLLocationManager *locationManager;

@end

@implementation PRVLocationServiceImplementation

- (instancetype)init{
    self = [super init];
    if (self) {
        CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
        if (status == kCLAuthorizationStatusDenied) {
            [_locationManager requestWhenInUseAuthorization];
        } else {
            _locationManager = [[CLLocationManager alloc] init];
            _locationManager.delegate = self;
            [_locationManager startUpdatingLocation];
        }
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
        [_locationManager startUpdatingLocation];
    }
    
    return self;
}

- (void)obtainUserLocation {
    [self.locationManager requestLocation];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    CLLocation *currentLocation = [locations lastObject];
    NSLog(@"yes - %f - %f", currentLocation.coordinate.latitude, currentLocation.coordinate.longitude);
    [self.delegate locationService:self didUpdateLocation:currentLocation];
    [self.locationManager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {

}

@end
