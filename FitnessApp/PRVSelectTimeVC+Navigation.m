//
//  PRVSelectTimeVC+Navigation.m
//  FitnessApp
//
//  Created by Ruslan on 9/20/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVSelectTimeVC+Navigation.h"
#import "PRVProgramConfirmVC.h"
#import "PRVClubsServerModel.h"

@implementation PRVSelectTimeVC (Navigation)

- (void)showProgramConfirmViewControllerForDate:(NSDate *)date time:(NSString *)time{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"d.MM.y";
    PRVProgramConfirmVC *vc = [[PRVProgramConfirmVC alloc] prv_initWithNib];
    vc.club = self.club;
    vc.date = [formatter stringFromDate:self.selectedDate];
    vc.time = time;
    vc.price = self.club.price_free;
    vc.enrollType = PRVEnrollTicketTypeFreeSchedule;
    [self.navigationController pushViewController:vc animated:YES];
}

@end
