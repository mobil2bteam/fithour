//
//  PRVServerManager.m
//  FitnessApp
//
//  Created by Ruslan on 7/10/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVServerManager.h"
#import "PRVCacheManager.h"
#import "AFNetworking.h"
#import "Promise.h"
#import "PRVCache+CoreDataProperties.h"
#import "PRVOptionsServerModel.h"
#import "PRVClubsServerModel.h"
#import "AFHTTPSessionOperation.h"
#import "PRVUserServerModel.h"
#import "PRVAppManager.h"
#import "PRVCoachScheduleServerModel.h"


@interface PRVServerManager ()

@property (strong, nonatomic) AFHTTPSessionManager *requestOperationManager;

@end


@implementation PRVServerManager

+ (instancetype)sharedManager{
    static PRVServerManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[PRVServerManager alloc] init];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@""]];
        manager.requestOperationManager = [[AFHTTPSessionManager alloc] initWithBaseURL:url];
        [manager.requestOperationManager.requestSerializer setTimeoutInterval:180];
        manager.requestOperationManager.responseSerializer= [AFJSONResponseSerializer serializer];
    });
    return manager;
}

#pragma mark - Methods

- (void)loadOptionsOnSuccess:(void(^)()) success
                  onFailure:(void(^)(NSError *error)) failure {
    [self loadOptions].then(^(PRVOptionsServerModel *options) {
        [[PRVAppManager sharedInstance] setOptions:options];
        return [self checkUser];
    }).then(^(PRVUserServerModel *user){
        success();
    }).catch(^(NSError *error) {
        failure(error);
    });
}

- (PMKPromise *)loadOptions{
    return [PMKPromise new:^(PMKPromiseFulfiller fulfill, PMKPromiseRejecter reject){
        PRVCache *cache = [PRVCacheManager cacheForKey:@"options"];
        if (cache) {
            fulfill([[PRVOptionsServerModel alloc] initWithCache:cache]);
            return;
        }
        [[AFHTTPSessionOperation operationWithManager:self.requestOperationManager HTTPMethod:@"GET" URLString:@"http://cabinet.fithour.ru/api/" parameters:nil uploadProgress:nil downloadProgress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
            if (responseObject[@"options"]) {
                PRVOptionsServerModel *options = [EKMapper objectFromExternalRepresentation:responseObject withMapping:[PRVOptionsServerModel objectMapping]];
                if (responseObject[@"cache"]) {
                    [PRVCacheManager setCache:responseObject forKey:@"options"];
                }
                fulfill(options);
            } else {
                reject([[NSError alloc]init]);
            }
        } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
            reject(error);
        }] start];
    }];
}

- (PMKPromise *)checkUser{
    return [PMKPromise new:^(PMKPromiseFulfiller fulfill, PMKPromiseRejecter reject){
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSString *token = [userDefaults stringForKey:@"token"];
        if (token) {
            [self logInWithParams:@{@"token":token} onSuccess:^(PRVUserServerModel *user, NSString *error) {
                fulfill(user);
            } onFailure:^(NSError *error, NSInteger statusCode) {
                reject(error);
            }];
        } else {
            fulfill(nil);
        }
    }];
}

- (void)filterClubsWithParams:(NSDictionary *)params
                  onSuccess:(void(^)(PRVClubsServerModel *clubs, NSString *error)) success
                  onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure{
    __block NSString *error = nil;
    __block PRVClubsServerModel *clubs;
    PRVUrlServerModel *url = [[[PRVAppManager sharedInstance] getOptions] urlForName:@"url_clubs_search"];
    [[AFHTTPSessionOperation operationWithManager:self.requestOperationManager HTTPMethod:url.metod URLString:url.url parameters:params uploadProgress:nil downloadProgress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if (responseObject[@"error"]) {
            error = responseObject[@"error"];
        } else if (responseObject[@"clubs"]) {
            clubs = [EKMapper objectFromExternalRepresentation:responseObject withMapping:[PRVClubsServerModel objectMapping]];
        }
        
        if (success) {
            success(clubs, error);
        }
        
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
        if (failure)
        {
            failure(error,error.code);
        }
    }] start];
}

- (void)getClubWithId:(NSInteger)clubId
              userlat:(CGFloat)userLat
              userLng:(CGFloat)userLng
          currentDate:(NSString *)currentDate // format - d.MM.y
            onSuccess:(void(^)(PRVClubServerModel *club, NSString *error)) success
            onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure{
    NSMutableDictionary *params = [@{@"club_id":@(clubId),
                             @"lat":@(userLat),
                             @"lng":@(userLng),
                             @"date":currentDate} mutableCopy];
    if ([[PRVAppManager sharedInstance] getUser]) {
        params[@"token"] = [[PRVAppManager sharedInstance] getUser].info.accessToken;
    }
    __block NSString *error;
    __block PRVClubServerModel *club;

    PRVUrlServerModel *url = [[[PRVAppManager sharedInstance] getOptions] urlForName:@"url_club"];
    
    [[AFHTTPSessionOperation operationWithManager:self.requestOperationManager HTTPMethod:url.metod URLString:url.url parameters:params uploadProgress:nil downloadProgress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if (responseObject[@"error"]) {
            error = responseObject[@"error"];
        } else if (responseObject[@"club"]) {
            club = [EKMapper objectFromExternalRepresentation:responseObject[@"club"] withMapping:[PRVClubServerModel objectMapping]];
        }
        
        if (success) {
            success(club, error);
        }
        
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
        if (failure)
        {
            failure(error,error.code);
        }
    }] start];
}

- (void)logInWithParams:(NSDictionary *)params
                    onSuccess:(void(^)(PRVUserServerModel *user, NSString *error)) success
                    onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure{
    __block NSString *error = nil;
    __block PRVUserServerModel *user;
    PRVUrlServerModel *url = [[[PRVAppManager sharedInstance] getOptions] urlForName:@"url_login"];
    [[AFHTTPSessionOperation operationWithManager:self.requestOperationManager HTTPMethod:url.metod URLString:url.url parameters:params uploadProgress:nil downloadProgress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if (responseObject[@"error"]) {
            error = responseObject[@"error"][@"text"];
        } else if (responseObject[@"user"]) {
            user = [EKMapper objectFromExternalRepresentation:responseObject withMapping:[PRVUserServerModel objectMapping]];
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setValue:user.info.accessToken forKey:@"token"];
            [userDefaults synchronize];
            [[PRVAppManager sharedInstance] setUser:user];
        }
        
        if (success) {
            success(user, error);
        }
        
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
        if (failure)
        {
            failure(error,error.code);
        }
    }] start];
}

- (void)signUpWithLogin:(NSString *)login
                  phone:(NSString *)phone
                  email:(NSString *)email
                  password:(NSString *)password
               passport:(NSString *)passport
               image:(UIImage *)image
                  image2:(UIImage *)image2
              onSuccess:(void(^)(PRVUserServerModel *user, NSString *error)) success
              onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure{
    __block NSString *error = nil;
    __block PRVUserServerModel *user;
    
    NSDictionary *params = @{@"email":email,
                             @"phone":phone,
                             @"password": password,
                             @"passport": passport,
                             @"name":login};
    
    PRVUrlServerModel *url = [[[PRVAppManager sharedInstance] getOptions] urlForName:@"url_registration_short"];

    [[AFHTTPSessionOperation operationWithManager:self.requestOperationManager HTTPMethod:url.metod URLString:url.url parameters:params uploadProgress:nil downloadProgress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if (responseObject[@"error"]) {
            error = responseObject[@"error"][@"text"];
        } else if (responseObject[@"user"]) {
            user = [EKMapper objectFromExternalRepresentation:responseObject withMapping:[PRVUserServerModel objectMapping]];
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setObject:user.info.accessToken forKey:@"token"];
            [userDefaults synchronize];
            [[PRVAppManager sharedInstance] setUser:user];
        }
        if (success) {
            success(user, error);
        }
        
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
        if (failure)
        {
            failure(error,error.code);
        }
    }] start];
}

- (void)preSignUpWithParams:(NSDictionary *)params
                 onSuccess:(void(^)(void)) success
              onFailure:(void(^)(NSString *error)) failure{
    __block NSString *error = nil;
    PRVUrlServerModel *url = [[[PRVAppManager sharedInstance] getOptions] urlForName:@"url_registration"];
    [[AFHTTPSessionOperation operationWithManager:self.requestOperationManager HTTPMethod:url.metod URLString:url.url parameters:params uploadProgress:nil downloadProgress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if (responseObject[@"error"]) {
            error = responseObject[@"error"][@"text"];
            failure(error);
        } else if (responseObject[@"result"]) {
            if ([responseObject[@"result"][@"success"] boolValue]) {
                success();
            } else {
                failure(@"Произошла ошибка");
            }
        } else {
            failure(@"Произошла ошибка");
        }
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
        if (failure)
        {
            failure(error.localizedDescription);
        }
    }] start];
}

- (void)buyTicketWithParams:(NSDictionary *)params
               onSuccess:(void(^)(PRVTicketSeverModel *ticket, NSString *error)) success
               onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure{
    __block NSString *error = nil;
    __block PRVTicketSeverModel *ticket;
    NSMutableDictionary *temp = [params mutableCopy];
    if ([[PRVAppManager sharedInstance] getUser].info.accessToken) {
        temp[@"token"] = [[PRVAppManager sharedInstance] getUser].info.accessToken;
    }
    params = [temp copy];
    
    PRVUrlServerModel *url = [[[PRVAppManager sharedInstance] getOptions] urlForName:@"url_ticket_buy"];
    [[AFHTTPSessionOperation operationWithManager:self.requestOperationManager HTTPMethod:url.metod URLString:url.url parameters:params uploadProgress:nil downloadProgress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if (responseObject[@"error"]) {
            error = responseObject[@"error"][@"text"];
        } else if (responseObject[@"ticket"]) {
            ticket = [EKMapper objectFromExternalRepresentation:responseObject[@"ticket"] withMapping:[PRVTicketSeverModel objectMapping]];
        }
        if (success) {
            success(ticket, error);
        }
        
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
        if (failure)
        {
            failure(error,error.code);
        }
    }] start];
}

- (void)payTicketWithID:(NSInteger)ticketID
                  onSuccess:(void(^)(PRVTicketSeverModel *ticket, NSString *error)) success
                  onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure{
    __block NSString *error = nil;
    __block PRVTicketSeverModel *ticket;
    NSDictionary *params = @{@"ticket_id":@(ticketID),
                             @"token":[[PRVAppManager sharedInstance] getUser].info.accessToken};
    
    PRVUrlServerModel *url = [[[PRVAppManager sharedInstance] getOptions] urlForName:@"url_ticket_pay"];
    
    [[AFHTTPSessionOperation operationWithManager:self.requestOperationManager HTTPMethod:url.metod URLString:url.url parameters:params uploadProgress:nil downloadProgress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if (responseObject[@"error"]) {
            error = responseObject[@"error"][@"text"];
        } else if (responseObject[@"ticket"]) {
            ticket = [EKMapper objectFromExternalRepresentation:responseObject[@"ticket"] withMapping:[PRVTicketSeverModel objectMapping]];
        }
        if (success) {
            success(ticket, error);
        }
        
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
        if (failure)
        {
            failure(error,error.code);
        }
    }] start];
}

- (void)getTicketWithID:(NSInteger)ticketID
              onSuccess:(void(^)(PRVTicketSeverModel *ticket, NSString *error)) success
              onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure{
    __block NSString *error = nil;
    __block PRVTicketSeverModel *ticket;
    NSDictionary *params = @{@"ticket_id":@(ticketID),
                             @"token":[[PRVAppManager sharedInstance] getUser].info.accessToken};
    
    PRVUrlServerModel *url = [[[PRVAppManager sharedInstance] getOptions] urlForName:@"url_ticket"];
    [[AFHTTPSessionOperation operationWithManager:self.requestOperationManager HTTPMethod:url.metod URLString:url.url parameters:params uploadProgress:nil downloadProgress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if (responseObject[@"error"]) {
            error = responseObject[@"error"][@"text"];
        } else if (responseObject[@"ticket"]) {
            ticket = [EKMapper objectFromExternalRepresentation:responseObject[@"ticket"] withMapping:[PRVTicketSeverModel objectMapping]];
        }
        if (success) {
            success(ticket, error);
        }
        
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
        if (failure)
        {
            failure(error,error.code);
        }
    }] start];
}

- (void)cancelTicketWithID:(NSInteger)ticketID
                 onSuccess:(void(^)(PRVTicketSeverModel *ticket, NSString *error)) success
                 onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure{
    __block NSString *error = nil;
    __block PRVTicketSeverModel *ticket;
    NSDictionary *params = @{@"ticket_id":@(ticketID),
                             @"token":[[PRVAppManager sharedInstance] getUser].info.accessToken};
    
    PRVUrlServerModel *url = [[[PRVAppManager sharedInstance] getOptions] urlForName:@"url_ticket_cancel"];
    
    [[AFHTTPSessionOperation operationWithManager:self.requestOperationManager HTTPMethod:url.metod URLString:url.url parameters:params uploadProgress:nil downloadProgress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if (responseObject[@"error"]) {
            error = responseObject[@"error"][@"text"];
        } else if (responseObject[@"ticket"]) {
            ticket = [EKMapper objectFromExternalRepresentation:responseObject[@"ticket"] withMapping:[PRVTicketSeverModel objectMapping]];
        }
        if (success) {
            success(ticket, error);
        }
        
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
        if (failure)
        {
            failure(error,error.code);
        }
    }] start];
}

- (void)changePasswordWithNewPassword:(NSString *)newPassword
                            onSuccess:(void(^)(PRVUserServerModel *user, NSString *error)) success
                            onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure{
    __block NSString *error = nil;
    __block PRVUserServerModel *user;
    NSDictionary *params = @{@"password":newPassword,
                             @"token":[[PRVAppManager sharedInstance] getUser].info.accessToken};
    
    PRVUrlServerModel *url = [[[PRVAppManager sharedInstance] getOptions] urlForName:@"url_user_change_password"];
    
    [[AFHTTPSessionOperation operationWithManager:self.requestOperationManager HTTPMethod:url.metod URLString:url.url parameters:params uploadProgress:nil downloadProgress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if (responseObject[@"error"]) {
            error = responseObject[@"error"][@"text"];
        } else if (responseObject[@"user"]) {
            user = [EKMapper objectFromExternalRepresentation:responseObject[@"user"] withMapping:[PRVTicketSeverModel objectMapping]];
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setObject:params[@"password"] forKey:@"password"];
            [userDefaults synchronize];
        }
        if (success) {
            success(user, error);
        }
        
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
        if (failure)
        {
            failure(error,error.code);
        }
    }] start];
}

- (void)restorePasswordWithEmail:(NSString *)email
                            onSuccess:(void(^)(NSString *message, NSString *error)) success
                            onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure{
    __block NSString *error = nil;
    __block NSString *message = nil;

    NSDictionary *params = @{@"email":email};
    
    PRVUrlServerModel *url = [[[PRVAppManager sharedInstance] getOptions] urlForName:@"url_user_reset_password"];
    
    [[AFHTTPSessionOperation operationWithManager:self.requestOperationManager HTTPMethod:url.metod URLString:url.url parameters:params uploadProgress:nil downloadProgress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if (responseObject[@"error"]) {
            error = responseObject[@"error"][@"text"];
        } else if (responseObject[@"message"]) {
            message = responseObject[@"message"][@"text"];
        }
        if (success) {
            success(message, error);
        }
        
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
        if (failure)
        {
            failure(error,error.code);
        }
    }] start];
}

- (void)addReviewWithText:(NSString *)text
                   rating:(NSInteger)rating
                   clubId:(NSInteger)clubId
                onSuccess:(void(^)(PRVReviewServerModel *review, NSString *error)) success
                onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure{
    __block NSString *error = nil;
    __block PRVReviewServerModel *review;
    NSDictionary *params = @{@"text":text,
                             @"rating":@(rating),
                             @"club_id":@(clubId),
                             @"token":[[PRVAppManager sharedInstance] getUser].info.accessToken};
    
    PRVUrlServerModel *url = [[[PRVAppManager sharedInstance] getOptions] urlForName:@"url_add_review"];
    
    [[AFHTTPSessionOperation operationWithManager:self.requestOperationManager HTTPMethod:url.metod URLString:url.url parameters:params uploadProgress:nil downloadProgress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if (responseObject[@"error"]) {
            error = responseObject[@"error"][@"text"];
        } else if (responseObject[@"user"]) {
            review = [EKMapper objectFromExternalRepresentation:responseObject[@"review"] withMapping:[PRVReviewServerModel objectMapping]];
        }
        if (success) {
            success(review, error);
        }
        
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
        if (failure)
        {
            failure(error,error.code);
        }
    }] start];
}

- (void)scheduleForCoachWithId:(NSInteger)coachId
                     onSuccess:(void(^)(PRVCoachScheduleServerModel *schedule, NSString *error)) success
                     onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure{
    __block NSString *error = nil;
    __block PRVCoachScheduleServerModel *schedule;
    NSDictionary *params = @{@"coach_id":@(coachId)};
    
    PRVUrlServerModel *url = [[[PRVAppManager sharedInstance] getOptions] urlForName:@"url_coach_schedule"];
    
    [[AFHTTPSessionOperation operationWithManager:self.requestOperationManager HTTPMethod:url.metod URLString:url.url parameters:params uploadProgress:nil downloadProgress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if (responseObject[@"error"]) {
            error = responseObject[@"error"][@"text"];
        } else if (responseObject[@"schedule"]) {
            schedule = [EKMapper objectFromExternalRepresentation:responseObject withMapping:[PRVCoachScheduleServerModel objectMapping]];
        }
        if (success) {
            success(schedule, error);
        }
        
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
        if (failure)
        {
            failure(error,error.code);
        }
    }] start];
}

- (void)buyTicketForPersonalTrainingWithCoachId:(NSInteger)coachId
                                           date:(NSString *)date
                                           time:(NSString *)time
                                      onSuccess:(void(^)(PRVTicketSeverModel *ticket, NSString *error)) success
                                      onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure{
    __block NSString *error = nil;
    __block PRVTicketSeverModel *ticket;
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    params[@"coach_id"] = @(coachId);
    params[@"date"] = date;
    params[@"time"] = time;
    if ([[PRVAppManager sharedInstance] getUser].info.accessToken) {
        params[@"token"] = [[PRVAppManager sharedInstance] getUser].info.accessToken;
    }
    PRVUrlServerModel *url = [[[PRVAppManager sharedInstance] getOptions] urlForName:@"url_ticket_coach_buy"];
    
    [[AFHTTPSessionOperation operationWithManager:self.requestOperationManager HTTPMethod:url.metod URLString:url.url parameters:params uploadProgress:nil downloadProgress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if (responseObject[@"error"]) {
            error = responseObject[@"error"][@"text"];
        } else if (responseObject[@"ticket"]) {
            ticket = [EKMapper objectFromExternalRepresentation:responseObject[@"ticket"] withMapping:[PRVTicketSeverModel objectMapping]];
        }
        if (success) {
            success(ticket, error);
        }
        
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
        if (failure)
        {
            failure(error,error.code);
        }
    }] start];
}

- (void)loadTicketsOnSuccess:(void(^)(void)) success{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *token = [userDefaults stringForKey:@"token"];
    [self logInWithParams:@{@"token":token} onSuccess:^(PRVUserServerModel *user, NSString *error) {
        if (success) {
            success();
        }
    } onFailure:nil];
}

#pragma mark - Support and Rating

- (void)rateAppWith:(NSInteger)rating
               name:(NSString *)name
              email:(NSString *)email
               text:(NSString *)text
              onSuccess:(void(^)()) success
              onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure{
    NSDictionary *params = @{@"ball":@(rating),
                             @"name": name,
                             @"email": email,
                             @"text": text};
    PRVUrlServerModel *url = [[[PRVAppManager sharedInstance] getOptions] urlForName:@"url_app_rating"];
    [[AFHTTPSessionOperation operationWithManager:self.requestOperationManager HTTPMethod:url.metod URLString:url.url parameters:params uploadProgress:nil downloadProgress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if (success) {
            success();
        }
        
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
        if (failure)
        {
            failure(error,error.code);
        }
    }] start];
}

- (void)supportWith:(NSString *)name
              email:(NSString *)email
               text:(NSString *)text
          onSuccess:(void(^)()) success
          onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure{
    NSDictionary *params = @{@"name": name,
                             @"email": email,
                             @"text": text};
    PRVUrlServerModel *url = [[[PRVAppManager sharedInstance] getOptions] urlForName:@"url_tech"];
    [[AFHTTPSessionOperation operationWithManager:self.requestOperationManager HTTPMethod:url.metod URLString:url.url parameters:params uploadProgress:nil downloadProgress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if (success) {
            success();
        }
        
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
        if (failure)
        {
            failure(error,error.code);
        }
    }] start];
}

@end
