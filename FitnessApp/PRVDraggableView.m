//
//  PRVDraggableView.m
//  FitnessApp
//
//  Created by Ruslan on 8/30/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVDraggableView.h"

@interface PRVDraggableView ()

@property (nonatomic, strong) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *overlayView;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewTopSpacingConstraint;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIImageView *arrowImageView;
@property (weak, nonatomic) IBOutlet UIButton *leftButton;
@end

@implementation PRVDraggableView


- (void)awakeFromNib{
    [super awakeFromNib];
}

- (void)addContentView:(UIView *)view{
    view.frame = self.containerView.bounds;
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    view.translatesAutoresizingMaskIntoConstraints = YES;
    [self.containerView addSubview:view];
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        NSString *className = NSStringFromClass([self class]);
        [[NSBundle mainBundle]loadNibNamed:className owner:self options:nil];
        [self addSubview:_contentView];
        _contentView.frame = self.bounds;
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)setStatusTitle:(NSString *)title{
    _statusLabel.text = title;
}

- (void)setLeftButtonTitle:(NSString *)title{
    [self.leftButton setTitle:title forState:UIControlStateNormal];
}

- (void)setLeftButtonHidden:(BOOL)hidden{
    self.leftButton.hidden = hidden;
}

#pragma mark - Gestures

- (IBAction)handlePan:(UIPanGestureRecognizer *)recognizer {
    if (recognizer.state == UIGestureRecognizerStateChanged) {
        // move the view by gesture, if view intersects with top view (view with filters), move it back
        CGPoint translation = [recognizer translationInView:self.contentView];
        CGFloat minY = CGRectGetMinY(recognizer.view.frame);
        if (translation.y < 0 && fabs(translation.y) > minY) {
            translation.y = -minY;
        }
        recognizer.view.center = CGPointMake(recognizer.view.center.x, recognizer.view.center.y + translation.y);

        [recognizer setTranslation:CGPointMake(0, 0) inView:self.contentView];
    }
    
    self.overlayView.alpha = 1 - CGRectGetMinY(recognizer.view.frame) / CGRectGetHeight(recognizer.view.frame);
    // when user ended draging the view, show or hide view with clubs depending on direction of gesture
    if (recognizer.state == UIGestureRecognizerStateEnded) {

        CGPoint vel = [recognizer velocityInView:self.contentView];
        if (vel.y < 0) {
            // show
            if (CGRectGetMinY(recognizer.view.frame) / CGRectGetHeight(recognizer.view.frame) < 0.5) {
                [self showDraggableViewWithAnimation:YES];
            } else {
                [self showOnHalfDraggableViewWithAnimation:YES];
            }
        } else {
            // hide
            if (CGRectGetMinY(recognizer.view.frame) / CGRectGetHeight(recognizer.view.frame) < 0.5) {
                [self showOnHalfDraggableViewWithAnimation:YES];
            } else {
                [self hideDraggableViewWithAnimation:YES];
            }
        }
    }
}
- (IBAction)leftButtonPressed:(id)sender {
    if (self.leftButtonBlock) {
        self.leftButtonBlock();
    }
}

- (IBAction)tapOverlay:(id)sender {
    [self hideDraggableViewWithAnimation:YES];
}

- (void)animateArrorImageView{
    [UIView animateWithDuration:0.25 animations:^{
        if ([self stateOfDraggableView] == PRVDraggableViewStateShowing) {
            self.arrowImageView.transform = CGAffineTransformMakeRotation(M_PI);
        } else {
            self.arrowImageView.transform = CGAffineTransformIdentity;
        }
    }];
}

- (void)showDraggableViewWithAnimation:(BOOL)animation{
    self.hidden = NO;
    self.contentViewTopSpacingConstraint.constant = [self topSpacingForState:PRVDraggableViewStateShowing];
    [self.contentView setNeedsLayout];
    if (animation) {
        [UIView animateWithDuration:0.25 animations:^{
            [self.contentView layoutIfNeeded];
            self.overlayView.alpha = [self overlayAlphaForState:PRVDraggableViewStateShowing];
        } completion:^(BOOL finished) {
            [self animateArrorImageView];
        }];
    } else {
        self.overlayView.alpha = [self overlayAlphaForState:PRVDraggableViewStateShowing];
        [self animateArrorImageView];
        [self.contentView layoutIfNeeded];
    }
}

- (void)showOnHalfDraggableViewWithAnimation:(BOOL)animation{
    self.hidden = NO;
    self.contentViewTopSpacingConstraint.constant = [self topSpacingForState:PRVDraggableViewStateShowingOnHalf];
    [self.contentView setNeedsLayout];
    if (animation) {
        [UIView animateWithDuration:0.25 animations:^{
            [self.contentView layoutIfNeeded];
            self.overlayView.alpha = [self overlayAlphaForState:PRVDraggableViewStateShowingOnHalf];
        } completion:^(BOOL finished) {
            [self animateArrorImageView];
        }];
    } else {
        self.overlayView.alpha = [self overlayAlphaForState:PRVDraggableViewStateShowingOnHalf];
        [self animateArrorImageView];
        [self.contentView layoutIfNeeded];
    }
}

- (void)hideDraggableViewWithAnimation:(BOOL)animation{
    self.contentViewTopSpacingConstraint.constant = [self topSpacingForState:PRVDraggableViewStateHidden];
    [self.contentView setNeedsLayout];
    if (animation) {
        [UIView animateWithDuration:0.25 animations:^{
            [self.contentView layoutIfNeeded];
            self.overlayView.alpha = [self overlayAlphaForState:PRVDraggableViewStateHidden];
        } completion:^(BOOL finished) {
            [self animateArrorImageView];
            self.hidden = YES;
        }];
    } else {
        self.overlayView.alpha = [self overlayAlphaForState:PRVDraggableViewStateHidden];
        [self animateArrorImageView];
        [self.contentView layoutIfNeeded];
        self.hidden = YES;
    }
}

- (PRVDraggableViewState)stateOfDraggableView{
    if (self.contentViewTopSpacingConstraint.constant == 0) {
        return PRVDraggableViewStateShowing;
    }
    if (self.contentViewTopSpacingConstraint.constant == CGRectGetHeight(self.frame)) {
        return PRVDraggableViewStateHidden;
    }
    return PRVDraggableViewStateShowingOnHalf;
}

- (CGFloat)topSpacingForState:(PRVDraggableViewState)state{
    switch (state) {
        case PRVDraggableViewStateShowing:
            return 0.f;
        case PRVDraggableViewStateShowingOnHalf:
            return CGRectGetHeight(self.frame) / 2;
        case PRVDraggableViewStateHidden:
            return CGRectGetHeight(self.frame);
    }
}

- (CGFloat)overlayAlphaForState:(PRVDraggableViewState)state{
    switch (state) {
        case PRVDraggableViewStateShowing:
            return 1.f;
        case PRVDraggableViewStateShowingOnHalf:
            return .5f;
        case PRVDraggableViewStateHidden:
            return .0f;
    }
}

@end
