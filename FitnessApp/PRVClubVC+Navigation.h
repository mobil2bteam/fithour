//
//  PRVClubVC+Navigation.h
//  FitnessApp
//
//  Created by Ruslan on 8/27/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//
@class PRVGroupServerModel;
@class PRVPhoneServerModel;

#import "PRVClubVC.h"

@interface PRVClubVC (Navigation)

- (void)showGroupListViewController;

- (void)showProgramListViewControllerWithFreeSchedule;

- (void)showCoachListViewController;

- (void)showCoachListViewControllerForPersonalTraining;

- (void)showSafariViewControllerWithUrl:(NSString *)urlString;

- (void)showProgramListViewControllerForGroup:(PRVGroupServerModel *)group;

- (void)showPhoneListViewControllerForPhones:(NSArray <PRVPhoneServerModel *> *)phones phoneBlock:(void (^)(NSString *phoneNumber))phoneBlock;

- (void)showSelectDayViewController;

- (void)showAddReviewViewController;

- (void)showClubDetailViewController;

@end
