//
//  PRVStartVC+Navigation.m
//  FitnessApp
//
//  Created by Ruslan on 8/27/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVStartVC+Navigation.h"
#import "PRVGeoVC.h"
#import "PRVOnboardVC.h"
#import "PRVViewController.h"

@implementation PRVStartVC (Navigation)

- (void)showMainViewController{
    PRVViewController *vc = [[PRVViewController alloc] init];
    [self presentViewController:vc animated:NO completion:nil];
}

- (void)showOnboardingViewController{
    PRVOnboardVC *vc = [[PRVOnboardVC alloc] prv_initWithNib];
    [self presentViewController:vc animated:NO completion:nil];
}

- (void)showGpsViewController{
    PRVGeoVC *vc = [[PRVGeoVC alloc] prv_initWithNib];
    [self presentViewController:vc animated:NO completion:nil];
}

@end
