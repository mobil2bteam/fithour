//
//  PRVEnrollCoachVC.h
//  FitnessApp
//
//  Created by Ruslan on 9/18/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PRVCoachServerModel;
@class PRVScheduleServerModel;
@class PRVClubServerModel;

@interface PRVEnrollCoachVC : UIViewController

@property (nonatomic, strong) PRVCoachServerModel *coach;

@property (nonatomic, strong) PRVClubServerModel *club;
@property (weak, nonatomic) IBOutlet UICollectionView *daysCollectionView;
@property (nonatomic, strong) PRVScheduleServerModel *selectedDay;

@end
