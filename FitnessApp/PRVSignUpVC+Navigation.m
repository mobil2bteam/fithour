//
//  PRVSignUpVC+Navigation.m
//  FitnessApp
//
//  Created by Ruslan on 9/5/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVSignUpVC+Navigation.h"
#import "PRVLogInVC.h"

@implementation PRVSignUpVC (Navigation)

- (void)showLoginViewController{
    PRVLogInVC *vc = [[PRVLogInVC alloc] prv_initWithNib];
    vc.hideSignUpButton = YES;
    vc.loginCallback = self.loginCallback;
    [self.navigationController pushViewController:vc animated:YES];
}

@end
