//
//  PRVEnrollGroupVC.h
//  FitnessApp
//
//  Created by Ruslan on 7/25/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PRVClubServerModel;
@class PRVDataSource;

@interface PRVGroupListVC : UIViewController

@property (nonatomic, weak) IBOutlet UITableView *groupsTableView;
@property (nonatomic, strong) PRVClubServerModel *club;
@property (nonatomic, strong) PRVDataSource *groupTableViewDataSource;

@end
