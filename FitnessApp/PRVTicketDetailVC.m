//
//  PRVTicketDetailVC.m
//  FitnessApp
//
//  Created by Ruslan on 8/20/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVTicketDetailVC.h"
#import "PRVUserServerModel.h"
#import "PRVTicketAddressVC.h"
#import "PRVActivateTicketVC.h"
#import "PRVClubsServerModel.h"
#import "PRVOptionsServerModel.h"
#import "UIImageView+PRVExtension.h"
#import "PRVProgressView.h"
#import "PRVServerManager.h"
#import "PRVPaymentVC.h"
#import "PRVNotificationManager.h"
#import "PRVCancelTicketAlertVC.h"
#import "PRVAppManager.h"

@interface PRVTicketDetailVC ()
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *addressButton;
@property (weak, nonatomic) IBOutlet UILabel *programLabel;
@property (weak, nonatomic) IBOutlet UILabel *clubLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeStartLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeFinishLabel;
@property (weak, nonatomic) IBOutlet UIView *startTimeView;
@property (weak, nonatomic) IBOutlet UIView *finishTimeView;
@property (weak, nonatomic) IBOutlet UIView *additionalTimeView;
@property (weak, nonatomic) IBOutlet UILabel *failMinutesLabel;
@property (weak, nonatomic) IBOutlet UILabel *failPriceLabel;
@property (weak, nonatomic) IBOutlet UIButton *activateButton;
@property (weak, nonatomic) IBOutlet PRVProgressView *progressView;
@property (weak, nonatomic) IBOutlet UIImageView *programImageView;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *payButton;
@property (weak, nonatomic) IBOutlet UILabel *paymentStatusLabel;
@property (nonatomic, strong) UIRefreshControl *refreshControl;
@end

@implementation PRVTicketDetailVC

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Билет";
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self prepareView];
    // add pull to refresh to update ticket status
    self.refreshControl = [[UIRefreshControl alloc]init];
    [self.scrollView addSubview:self.refreshControl];
    [self.refreshControl addTarget:self action:@selector(refreshTicket) forControlEvents:UIControlEventValueChanged];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (self.ticket) {
        __weak typeof(self) weakSelf = self;
        [[PRVServerManager sharedManager] getTicketWithID:self.ticket._id onSuccess:^(PRVTicketSeverModel *ticket, NSString *error) {
            typeof(self) strongSelf = weakSelf;
            if (ticket) {
                strongSelf.ticket = ticket;
            }
        } onFailure:nil];
    }
}

#pragma mark - Methods

- (void)refreshTicket {
    __weak typeof(self) weakSelf = self;
    [[PRVServerManager sharedManager] getTicketWithID:self.ticket._id onSuccess:^(PRVTicketSeverModel *ticket, NSString *error) {
        typeof(self) strongSelf = weakSelf;
        [strongSelf.refreshControl endRefreshing];
        if (ticket) {
            strongSelf.ticket = ticket;
        } else {
            [strongSelf prv_showMessage:error withTitle:nil];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        typeof(self) strongSelf = weakSelf;
        [strongSelf.refreshControl endRefreshing];
        [strongSelf prv_showMessage:error.localizedDescription withTitle:nil];
    }];
}

- (void)prepareView{
    self.paymentStatusLabel.textColor = [UIColor primaryColor];
    self.priceLabel.textColor = [UIColor primaryColor];
    self.activateButton.backgroundColor = [UIColor primaryColor];
    self.cancelButton.hidden = [self.ticket isActive] || [self.ticket isCompleted];
    self.payButton.hidden = self.ticket.isPayment;
    self.paymentStatusLabel.text = self.ticket.isPayment ? @"Оплачено" : @"";
    self.activateButton.hidden = self.ticket.isPayment ? NO : YES;

    self.priceLabel.text = [NSString stringWithFormat:@"%ld руб.", (long)self.ticket.price];
    [self.progressView prepareViewForTicket:self.ticket];
    if (self.ticket.enrollmentType == PRVEnrollTicketTypeDefault) {
        self.programLabel.text = self.ticket.program.name;
        if (self.ticket.program.images.count) {
            [self.programImageView prv_setImageWithURL:self.ticket.program.images[0]];
        }
    }
    if (self.ticket.enrollmentType == PRVEnrollTicketTypePersonalCoach) {
        self.programLabel.text = @"Персональная тренировка";
        if (self.ticket.coach.image.length) {
            [self.programImageView prv_setImageWithURL:self.ticket.coach.image];
        } else {
            self.programImageView.image = [UIImage imageNamed:@"coach_placeholder.jpg"];
        }
    }
    if (self.ticket.enrollmentType == PRVEnrollTicketTypeFreeSchedule) {
        self.programLabel.text = @"Свободная зона";
        self.programImageView.image = [UIImage imageNamed:@"free_zone.jpg"];
        self.priceLabel.text = [NSString stringWithFormat:@"%ld руб.", (long)self.ticket.club.price_free];
    }
    
    self.clubLabel.text = self.ticket.club.name;
    self.timeLabel.text = [NSString stringWithFormat:@"%@ в %@ до %@", self.ticket.date, self.ticket.time_from, self.ticket.time_to];
    [self.addressButton setTitle:self.ticket.club.address forState:UIControlStateNormal];
    self.timeStartLabel.text = self.ticket.timeActive;
    self.timeFinishLabel.text = self.ticket.timeComplete;
    
    // If user used additional minutes
    if (self.ticket.failMinutes) {
        self.failMinutesLabel.text = [NSString stringWithFormat:@"%ld мин.", (long)self.ticket.failMinutes];
        self.failPriceLabel.text = [NSString stringWithFormat:@"%ld руб.", (long)self.ticket.failPrice];
        self.additionalTimeView.hidden = NO;
    } else {
        self.additionalTimeView.hidden = YES;
    }
    
    if ([self.ticket isCanceled]) {
        self.activateButton.hidden = YES;
        self.cancelButton.hidden = YES;
        self.payButton.hidden = YES;
        return;
    }
    
    if ([self.ticket isCompleted]) {
        self.startTimeView.hidden = NO;
        self.finishTimeView.hidden = NO;
        self.activateButton.hidden = YES;
        return;
    }
    
    if ([self.ticket isActive]) {
        self.startTimeView.hidden = NO;
        self.cancelButton.hidden = YES;
        [self.activateButton setTitle:@"Завершить тренировку" forState:UIControlStateNormal];
        self.activateButton.backgroundColor = [UIColor thirdColor];
        self.activateButton.hidden = [self.ticket isStarted] ? NO : YES;
        return;
    }
    
    if ([self.ticket isNew]) {
        self.startTimeView.hidden = YES;
        self.finishTimeView.hidden = YES;
        [self.activateButton setTitle:@"Активировать билет" forState:UIControlStateNormal];
        self.activateButton.backgroundColor = [UIColor primaryColor];
        if (self.ticket.isPayment) {
            self.activateButton.hidden = NO;
        }
        return;
    }
}

- (void)setTicket:(PRVTicketSeverModel *)ticket{
    _ticket = ticket;
    [self prepareView];
}

#pragma mark - Actions

- (IBAction)showAddressButtonPressed:(id)sender {
    PRVTicketAddressVC *vc = [[PRVTicketAddressVC alloc] prv_initWithNib];
    vc.ticket = self.ticket;
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)activateButtonPressed:(id)sender {
    PRVActivateTicketVC *vc = [[PRVActivateTicketVC alloc] prv_initWithNib];
    vc.ticket = self.ticket;
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)payButtonPressed:(id)sender {
    __weak typeof(self) weakSelf = self;
    PRVPaymentVC *vc = [[PRVPaymentVC alloc] prv_initWithNib];
    vc.ticket = self.ticket;
    vc.paymentCompletion = ^(PRVTicketSeverModel *ticket) {
        typeof(self) strongSelf = weakSelf;
        strongSelf.ticket = ticket;
        [[[PRVAppManager sharedInstance] getUser] updateTicket:ticket];
    };
    UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:navVC animated:YES completion:nil];
}

- (IBAction)cancelTicketButtonPressed:(id)sender {
    __weak typeof(self) weakSelf = self;
    PRVCancelTicketAlertVC *vc = [[PRVCancelTicketAlertVC alloc] prv_initWithNib];
    vc.completionCallback = ^{
        typeof(self) strongSelf = weakSelf;
        [strongSelf cancelTicket];
    };
    vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:vc animated:YES completion:nil];
}

- (void)cancelTicket{
    [self prv_addMBProgressHUD];
    __weak typeof(self) weakSelf = self;
    [[PRVServerManager sharedManager] cancelTicketWithID:self.ticket._id onSuccess:^(PRVTicketSeverModel *ticket, NSString *error) {
        typeof(self) strongSelf = weakSelf;
        [strongSelf prv_hideMBProgressHUD];
        if (error) {
            [strongSelf prv_showMessage:error withTitle:nil];
        } else {
            [strongSelf prv_showMessage:@"Билет отменен" withTitle:nil];
            [PRVNotificationManager removeNotificationForTicket:ticket];
            strongSelf.ticket.dateCancel = ticket.dateCancel;
            strongSelf.ticket = ticket;
            [[[PRVAppManager sharedInstance] getUser] removeTicketForID:ticket._id];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        typeof(self) strongSelf = weakSelf;
        [strongSelf prv_hideMBProgressHUD];
        [strongSelf prv_showMessage:error.localizedDescription withTitle:nil];
    }];
}

@end
