//
//  PRVProgramListVC.m
//  FitnessApp
//
//  Created by Ruslan on 7/28/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVProgramListVC.h"
#import "PRVProgramListVC+Setup.h"
#import "PRVProgramListVC+Navigation.h"
#import "PRVOptionsServerModel.h"
#import "PRVGroupHeaderView+Configuration.h"
#import "PRVClubsServerModel.h"
#import "PRVDataSource.h"

@interface PRVProgramListVC ()

@property (weak, nonatomic) IBOutlet UIView *freeScheduleView;

@end

@implementation PRVProgramListVC

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = self.isFreeSchedule ? @"Свободная зона" : self.group.name;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.freeScheduleView.hidden = !self.isFreeSchedule;
    [self setupTableView];
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.isFreeSchedule) {
        [self showProgramListViewControllerForGroup:[self.programsDataSource itemAtIndexPath:indexPath]];
    } else {
        [self showProgramDetailViewControllerForProgram:[self.programsDataSource itemAtIndexPath:indexPath]];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

#pragma mark - Actions

- (IBAction)freeScheduleButtonPressed:(id)sender {
    [self showSelectDayViewController];
}

- (BOOL)hidesBottomBarWhenPushed
{
    return YES;
}

@end
