//
//  PRVAboutViewController.m
//  FitnessApp
//
//  Created by Ruslan on 11/2/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVAboutViewController.h"

@interface PRVAboutViewController ()

@end

@implementation PRVAboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
}

- (BOOL)hidesBottomBarWhenPushed
{
    return YES;
}

@end
