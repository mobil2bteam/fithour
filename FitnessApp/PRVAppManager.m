//
//  PRVAppManager.m
//  FitnessApp
//
//  Created by Ruslan on 7/20/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVAppManager.h"
#import "PRVOptionsServerModel.h"

@interface PRVAppManager()

@property (nonatomic, strong) PRVOptionsServerModel *options;

@property (nonatomic, strong) PRVCityServerModel *userCity;

@property (nonatomic, strong) PRVUserServerModel *user;

@end


@implementation PRVAppManager

+ (instancetype)sharedInstance{
    static dispatch_once_t onceToken;
    __strong static PRVAppManager *sharedObject = nil;
    dispatch_once(&onceToken, ^{
        sharedObject = [[PRVAppManager alloc]init];
    });
    return sharedObject;
}

#pragma mark - Getters 

- (PRVOptionsServerModel *)getOptions{
    return _options;
}

- (PRVCityServerModel *)getUserCity{
    if (_userCity) {
        return _userCity;
    }
    // if city is not setted yet, check if it's exist in NSUserDefaults
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if ([userDefaults objectForKey:@"city"]) {
        NSData *storedEncodedObject = [userDefaults objectForKey:@"city"];
        PRVCityServerModel *city = [NSKeyedUnarchiver unarchiveObjectWithData:storedEncodedObject];
        _userCity = city;
    }
    return _userCity;
}

- (PRVUserServerModel *)getUser{
    return _user;
}

#pragma mark - Setters

- (void)setOptions:(PRVOptionsServerModel *)options{
    _options = options;
}

- (void)setUserCity:(PRVCityServerModel *)userCity{
    // save city to NSUserDefaults to use it in the furure
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:userCity];
    [userDefaults setObject:encodedObject forKey:@"city"];
    [userDefaults synchronize];
    _userCity = userCity;
}

- (void)setUser:(PRVUserServerModel *)user{
    _user = user;
}

- (void)removeUser{
    _user = nil;
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults removeObjectForKey:@"token"];
    [userDefaults synchronize];
}

@end
