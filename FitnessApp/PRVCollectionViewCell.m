//
//  PRVCollectionViewCell.m
//  FitnessApp
//
//  Created by Ruslan on 7/14/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVCollectionViewCell.h"

@implementation PRVCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.layer.cornerRadius = CGRectGetHeight(self.frame)/2;
    self.layer.borderWidth = 1.f;
    self.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

- (void)setHighlighted:(BOOL)highlighted{
    [super setHighlighted:highlighted];
    CGFloat alpha = 0;
    if (highlighted) {
        alpha = 1;
    }
    [UIView animateWithDuration:0.25 animations:^{
        self.gradientImageView.alpha = alpha;
    }];
}

- (void)prepareForReuse{
    self.gradientImageView.alpha = 0;
    self.nameLabel.text = @"";
}

@end
