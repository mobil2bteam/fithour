//
//  AppDelegate.m
//  FitnessApp
//
//  Created by Ruslan on 7/10/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

@import GoogleMaps;
#import "AppDelegate.h"
#import <IQKeyboardManager/IQKeyboardManager.h>
#import <MagicalRecord/MagicalRecord.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "PRVStartVC.h"
#import "UIViewController+PRVExtensions.h"
#import "UIColor+PRVHexColor.h"
#import "PRVCacheManager.h"
#import "PRVNotificationManager.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [PRVNotificationManager registerNotifications];
    [[UINavigationBar appearance] setBackgroundColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setTintColor:[UIColor primaryColor]];
    [[UINavigationBar appearance] setTranslucent:NO];
    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    // add Fabric to testing app
    [Fabric with:@[[Crashlytics class]]];
    
    // add IQKeyboardManager to automatically scroll view when keyboard is showing
    [IQKeyboardManager sharedManager].enable = YES;
    
    // set up MagicalRecord
    [MagicalRecord setupAutoMigratingCoreDataStack];
    
    [PRVCacheManager clearCache];
    
    // set up Google Maps
    [GMSServices provideAPIKey:@"AIzaSyAkN1ZAqXBNhtLR5p6T3hEeAaVvvUX9dO8"];
    
    // init window
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    
    // set up start view controller
    PRVStartVC *startVC = [[PRVStartVC alloc]prv_initWithNib];
    self.window.rootViewController = startVC;
    [self.window makeKeyAndVisible];
    return YES;
}

@end
