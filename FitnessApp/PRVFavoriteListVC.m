//
//  PRVFavoriteListVC.m
//  FitnessApp
//
//  Created by Ruslan on 7/20/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVFavoriteListVC.h"
#import "PRVDataSource.h"
#import "PRVClubDataModel+CoreDataClass.h"
#import "PRVClubCollectionViewCell+Configuration.h"
#import "PRVDataManager.h"
#import "UIScrollView+EmptyDataSet.h"
#import "PRVClubVC.h"
#import <UserNotifications/UserNotifications.h>
#import "PRVLocationServiceDelegate.h"
#import "PRVLocationServiceImplementation.h"


#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

static NSString * const kClubCollectionViewCellIdentifier = @"PRVClubCollectionViewCell";

@interface PRVFavoriteListVC () <DZNEmptyDataSetSource, DZNEmptyDataSetDelegate, PRVLocationServiceDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *clubsCollectionView;
@property (nonatomic, strong) PRVDataSource *clubListDataSource;
@property (nonatomic, strong) NSArray <PRVClubDataModel *> *favoriteClubs;
@property (nonatomic, strong) PRVDataManager *dataManager;
@property (nonatomic, strong) PRVLocationServiceImplementation *locationService;
@property (nonatomic, assign) CLLocationCoordinate2D position;
@end

@implementation PRVFavoriteListVC

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Избранные";
    self.dataManager = [[PRVDataManager alloc] init];
    self.locationService = [[PRVLocationServiceImplementation alloc] init];
    self.locationService.delegate = self;
    [self setupClubCollectionView];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
    self.favoriteClubs = [self.dataManager favoriteClubs];
    [self.clubListDataSource setItems:self.favoriteClubs];
    [self.clubsCollectionView reloadData];
}

#pragma mark - Methods

- (void)setupClubCollectionView{
    self.clubsCollectionView.emptyDataSetDelegate = self;
    self.clubsCollectionView.emptyDataSetSource = self;
    __weak typeof(self) weakSelf = self;
    self.clubListDataSource = [[PRVDataSource alloc] initWithItems:self.favoriteClubs   cellIdentifier:kClubCollectionViewCellIdentifier configureCellBlock:^(PRVClubCollectionViewCell *cell, PRVClubDataModel *club) {
        [cell configureCellForFavoriteClub:club deleteBlock:^{
            typeof(self) strongSelf = weakSelf;
            [strongSelf deleteClub:club];
        }];
    }];
    self.clubsCollectionView.dataSource = self.clubListDataSource;
    [self.clubsCollectionView registerNib:[PRVClubCollectionViewCell prv_nib] forCellWithReuseIdentifier:kClubCollectionViewCellIdentifier];
}

- (void)deleteClub:(PRVClubDataModel *)club{
    NSInteger clubIndex = [self.favoriteClubs indexOfObject:club];
    __weak typeof(self) weakSelf = self;
    [self.dataManager removeClub:club completionBlock:^(NSError *error) {
        typeof(self) strongSelf = weakSelf;
        if (error) {
            UIAlertController *alerVC = [UIAlertController alertControllerWithTitle:@"Ошибка" message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
            [alerVC prv_addActionWithTitle:@"Ок" style:UIAlertActionStyleDefault actionBlock:nil];
            [strongSelf presentViewController:alerVC animated:YES completion:nil];
        } else {
            strongSelf.favoriteClubs = [strongSelf.dataManager favoriteClubs];
            [strongSelf.clubsCollectionView performBatchUpdates:^{
                [strongSelf.clubsCollectionView deleteItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:clubIndex inSection:0]]];
            } completion:^(BOOL finished) {
                if (!strongSelf.favoriteClubs.count) {
                    [strongSelf.clubsCollectionView reloadData];
                }
            }];
        }
    }];
}

#pragma mark - Setters

- (void)setFavoriteClubs:(NSArray<PRVClubDataModel *> *)favoriteClubs{
    _favoriteClubs = favoriteClubs;
    [_clubListDataSource setItems:favoriteClubs];
}

#pragma mark - UICollectionViewDelegate

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat width = CGRectGetWidth(self.clubsCollectionView.frame) - 20.f;
    return CGSizeMake(width, 140.f);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    PRVClubVC *vc = [[PRVClubVC alloc] prv_initWithNib];
    [vc loadClubWithID:self.favoriteClubs[indexPath.row].clubID lat:self.position.latitude lng:self.position.longitude];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - DZNEmptyDataSetSource

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"Список избранных пуст";
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:17.0f],
                                 NSForegroundColorAttributeName: [UIColor darkTextColor]};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

#pragma mark - PRVLocationServiceDelegate

- (void)locationService:(id<PRVLocationService>)locationService didUpdateLocation:(CLLocation *)location{
    self.position = location.coordinate;
}

@end
