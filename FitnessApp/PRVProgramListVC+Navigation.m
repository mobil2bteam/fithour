//
//  PRVProgramListVC+Navigation.m
//  FitnessApp
//
//  Created by Ruslan on 8/28/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVProgramListVC+Navigation.h"
#import "PRVProgramDetailVC.h"
#import "PRVProgramFreeScheduleDetailVC.h"
#import "PRVOptionsServerModel.h"
#import "PRVProgramFreeScheduleDetailVC.h"
#import "PRVSelectDayVC.h"
#import "PRVProgramListWithFreeScheduleVC.h"

@implementation PRVProgramListVC (Navigation)

- (void)showProgramDetailViewControllerForProgram:(PRVProgramServerModel *)program{
    if (program.free) {
        PRVProgramFreeScheduleDetailVC *vc = [[PRVProgramFreeScheduleDetailVC alloc] prv_initWithNib];
        vc.program = program;
        vc.club = self.club;
        vc.group = self.group;
        [self.navigationController pushViewController:vc animated:YES];
    } else {
        PRVProgramDetailVC *vc = [[PRVProgramDetailVC alloc] prv_initWithNib];
        vc.club = self.club;
        vc.group = self.group;
        vc.program = program;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (void)showProgramListViewControllerForGroup:(PRVGroupServerModel *)group{
    PRVProgramListWithFreeScheduleVC *vc = [[PRVProgramListWithFreeScheduleVC alloc] init];
    vc.group = group;
    vc.club = self.club;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)showSelectDayViewController{
    PRVSelectDayVC *vc = [[PRVSelectDayVC alloc] prv_initWithNib];
    vc.club = self.club;
    [self.navigationController pushViewController:vc animated:YES];
}

@end
