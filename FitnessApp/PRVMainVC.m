//
//  PRVMainVC.m
//  FitnessApp
//
//  Created by Ruslan on 7/12/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVMainVC.h"
#import "PRVServerManager.h"
#import "PRVClubsServerModel.h"
#import "PRVAppManager.h"
#import "PRVFilterView.h"
#import "PRVOptionsServerModel.h"
#import "PRVClubListView.h"
#import "PRVFilterVC.h"
#import "PRVFilterManager.h"
#import "PRVClubVC.h"
#import "PRVClubCollectionViewCell+Configuration.h"
#import "PRVDraggableView.h"
#import "PRVLocationServiceImplementation.h"
#import "PRVLocationServiceDelegate.h"
#import "PRVUserServerModel.h"
#import "PRVTicketDetailVC.h"

@import GoogleMaps;

static NSString * const kClubCollectionViewCellIdentifier = @"PRVClubCollectionViewCell";

@interface PRVMainVC () <PRVFilterViewDelegate, PRVClubListViewDelegate, GMSMapViewDelegate, PRVLocationServiceDelegate>

@property (nonatomic, weak) IBOutlet PRVFilterView *filterView;
@property (nonatomic, weak) IBOutlet GMSMapView *mapView;
@property (nonatomic, weak) IBOutlet PRVDraggableView *draggableView;
@property (nonatomic, weak) IBOutlet UIView *activeTicketView;
@property (nonatomic, strong) PRVCityServerModel *selectedCity;
@property (nonatomic, strong) PRVClubsServerModel *clubs;
@property (nonatomic, strong) PRVFilterManager *filterManager;
@property (nonatomic, strong) PRVClubVC *clubVC;
@property (nonatomic, strong) PRVTicketDetailVC *ticketDetailVC;
@property (nonatomic, strong) GMSMarker *userMarker;
// view with filtered clubs
@property (nonatomic, strong) PRVClubListView *clubListView;
@property (nonatomic, strong) PRVLocationServiceImplementation *locationService;
@property (nonatomic, strong) PRVDataSource *clubListDataSource;
@property (nonatomic, strong) NSArray <GMSMarker *> *allMarkers;
@property (nonatomic, strong) NSDictionary *lastFilterParameters;

@end

@implementation PRVMainVC

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.filterManager = [PRVFilterManager sharedInstance];
    self.filterView.delegate = self;
    [self setupClubListView];
    [self animateMapViewToSelectedCity];
    [self addClubViewControllerAsChildViewController];
    [self addTicketDetailViewControllerAsChildViewController];
    [self filterClubsWithAdditionalParameters:nil];
    self.activeTicketView.backgroundColor = [UIColor primaryColor];
    self.locationService = [[PRVLocationServiceImplementation alloc] init];
    self.locationService.delegate = self;
    [self.locationService obtainUserLocation];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
    self.activeTicketView.hidden = YES;
    if ([[PRVAppManager sharedInstance] getUser]) {
        PRVTicketSeverModel *ticket = [[[PRVAppManager sharedInstance] getUser] ticketForToday];
        if (ticket) {
            self.activeTicketView.hidden = NO;
        }
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden = NO;
}

- (PRVTicketDetailVC *)ticketDetailVC{
    if (_ticketDetailVC) {
        return _ticketDetailVC;
    }
    PRVTicketDetailVC *vc = [[PRVTicketDetailVC alloc] prv_initWithNib];
    [self addChildViewController:vc];
    [self.draggableView addContentView:vc.view];
    [vc didMoveToParentViewController:self];
    _ticketDetailVC = vc;
    return _ticketDetailVC;
}

#pragma mark - Setup

- (void)setupClubListView{
    self.clubListView = [[PRVClubListView alloc] initWithFrame:CGRectZero];
    self.clubListView.backgroundColor = [UIColor backgroundColor];
    self.clubListDataSource = [[PRVDataSource alloc] initWithItems:self.clubs.list cellIdentifier:kClubCollectionViewCellIdentifier configureCellBlock:^(PRVClubCollectionViewCell *cell, PRVClubServerModel *club) {
        [cell configureCellForClub:club];
    }];
    self.clubListView.dataSource = self.clubListDataSource;
    self.clubListView.delegate = self;
    [self.draggableView addContentView:self.clubListView];
}

#pragma mark - Methods

- (void)addClubViewControllerAsChildViewController{
    PRVClubVC *vc = [[PRVClubVC alloc] prv_initWithNib];
    [self addChildViewController:vc];
    [self.draggableView addContentView:vc.view];
    [vc didMoveToParentViewController:self];
    self.clubVC = vc;
    __weak typeof(self) weakSelf = self;
    self.draggableView.leftButtonBlock = ^{
        typeof (self) strongSelf = weakSelf;
        [strongSelf.draggableView setLeftButtonHidden:YES];
        [strongSelf.draggableView setStatusTitle:[NSString stringWithFormat:@"Найдено %lu мест", (unsigned long)strongSelf.clubs.count]];
        strongSelf.clubListView.hidden = NO;
        strongSelf.clubVC.view.hidden = YES;
    };
}

- (void)addTicketDetailViewControllerAsChildViewController{
    PRVTicketDetailVC *vc = [[PRVTicketDetailVC alloc] prv_initWithNib];
    [self.draggableView addContentView:vc.view];
    [self addChildViewController:vc];
    vc.view.hidden = YES;
    [vc didMoveToParentViewController:self];
}

- (PRVCityServerModel *)selectedCity{
    return [[PRVAppManager sharedInstance] getUserCity];
}

#pragma mark - Markers

- (void)addMarkersForClubs:(NSArray <PRVClubServerModel *> *)clubs{
    // remove old markres
    for (GMSMarker *marker in self.allMarkers) {
        marker.map = nil;
    }
    // add new
    NSMutableArray <GMSMarker *> *array = [[NSMutableArray alloc] init];
    for (PRVClubServerModel *club in clubs) {
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(club.lat, club.lng);
        GMSMarker *marker = [GMSMarker markerWithPosition:position];
        marker.title = club.name;
        marker.icon = [UIImage imageNamed:@"map_pin_place"];
        marker.map = self.mapView;
        marker.userData = club;
        [array addObject:marker];
    }
    self.allMarkers = [array copy];
}

- (void)addUserMarkerWithPosition:(CLLocationCoordinate2D)position{
    GMSMarker *marker = [GMSMarker markerWithPosition:position];
    marker.icon = [UIImage imageNamed:@"map_pin_user"];
    marker.map = self.mapView;
    self.userMarker = marker;
}

- (void)animateMapViewToSelectedCity{
    __weak typeof(self) weakSelf = self;
    CLGeocoder *gc = [[CLGeocoder alloc] init];
    NSString *cityName = self.selectedCity.name;
    if ([self.selectedCity.name containsString:@"Москва"]) {
        cityName = @"Москва";
    }
    NSString *address = [NSString stringWithFormat:@"%@, Россия", cityName];
    [gc geocodeAddressString:address completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if ([placemarks count] > 0)
         {
             typeof (self) strongSelf = weakSelf;
             CLPlacemark *mark = (CLPlacemark *)[placemarks objectAtIndex:0];
             if (!strongSelf.userMarker) {
                 [strongSelf addUserMarkerWithPosition:mark.location.coordinate];
             }
             strongSelf.mapView.camera = [GMSCameraPosition cameraWithTarget:mark.location.coordinate zoom:10];
             [strongSelf.mapView animateToLocation:mark.location.coordinate];
         }
     }];
}

#pragma mark - Actions

- (IBAction)showClubsButtonPressed:(id)sender {
    [self.draggableView setStatusTitle:[NSString stringWithFormat:@"Найдено %lu мест", (unsigned long)self.clubs.count]];
    [self.draggableView setLeftButtonHidden:YES];
    self.ticketDetailVC.view.hidden = YES;
    self.clubVC.view.hidden = YES;
    self.clubListView.hidden = NO;
    [self.draggableView showOnHalfDraggableViewWithAnimation:YES];
}

- (void)filterClubsWithAdditionalParameters:(NSDictionary *)additionalParameters{
    [self.draggableView setStatusTitle:@"Идет поиск..."];
    // Configure parameters
    NSMutableDictionary *params = [[self defaultParameters] mutableCopy];
    if (additionalParameters) {
        [params addEntriesFromDictionary:additionalParameters];
    };
    self.lastFilterParameters = additionalParameters;
    
    __weak typeof(self) weakSelf = self;
    void (^successBlock)(PRVClubsServerModel *clubs, NSString *error) = ^(PRVClubsServerModel *clubs, NSString *error){
        
        typeof (self) strongSelf = weakSelf;
        // if current page is greater than old, add new clubs to list
        if (strongSelf.clubs.page && clubs.page > strongSelf.clubs.page) {
            strongSelf.clubs.page = clubs.page;
            [strongSelf.clubs addClubs:clubs.list];
        } else {
            // replace old clubs
            strongSelf.clubs = clubs;
        }
        [strongSelf.clubListDataSource setItems:strongSelf.clubs.list];
        [strongSelf.draggableView setStatusTitle:[NSString stringWithFormat:@"Найдено %lu мест", (unsigned long)strongSelf.clubs.count]];
        [strongSelf addMarkersForClubs:clubs.list_point];
        [strongSelf.clubListView reloadData];
        [strongSelf.clubListView setLoadMoreEnabled:clubs.page < clubs.pageCount];
    };

    void (^failureBlock)(NSError *error, NSInteger statusCode) = ^(NSError *error, NSInteger statusCode){
        typeof (self) strongSelf = weakSelf;
        [strongSelf.draggableView setStatusTitle:@"Произошла ошибка!"];
        [strongSelf.clubListView reloadData];
    };

    [[PRVServerManager sharedManager] filterClubsWithParams:[params copy] onSuccess:successBlock onFailure:failureBlock];
}

- (NSDictionary *)defaultParameters{
    return @{@"city_id":@(self.selectedCity._id),
             @"view":@"short",
             @"lat":@(self.userMarker.position.latitude),
             @"lng":@(self.userMarker.position.longitude)};
}

- (IBAction)showUserLocationButtonPressed:(id)sender {
    [self.locationService obtainUserLocation];
    [self.mapView animateToLocation:self.userMarker.position];
}

#pragma mark - PRVFilterViewDelegate

- (void)filterDidUpdate{
    [self animateMapViewToSelectedCity];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    params[@"lat"] = @(self.userMarker.position.latitude);
    params[@"lng"] = @(self.userMarker.position.longitude);
    params[@"sort"] = self.filterManager.getSortMode == PRVSortModeRating ? @"rating" : @"distance";
    if (self.filterManager.getSelectedDate) {
        params[@"date"] = self.filterManager.getSelectedDate.value;
    }
    if (self.filterManager.getSelectedTime) {
        params[@"from"] = self.filterManager.getSelectedTime;
    }
    if (self.filterManager.getSelectedPrice) {
        params[@"price"] = @(self.filterManager.getSelectedPrice.value);
    }
    // Parameter 'groups' should have format 'group_id,group_id,group_id...'
    if (self.filterManager.getSelectedGroups.count) {
        NSMutableArray *groupsId = [[NSMutableArray alloc] init];
        for (PRVGroupServerModel *group in self.filterManager.getSelectedGroups) {
            [groupsId addObject:@(group._id)];
        }
        NSString *groups = [groupsId componentsJoinedByString:@","];;
        params[@"groups"] = groups;
    }
    [self filterClubsWithAdditionalParameters:[params copy]];
}

- (void)detailFilterDidSelected{
    __weak typeof(self) weakSelf = self;
    PRVFilterVC *vc = [[PRVFilterVC alloc] prv_initWithNib];
    vc.filterCallback = ^(){
        typeof(self) strongSelf = weakSelf;
        [strongSelf.filterView updateView];
    };
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - PRVClubListViewDelegate

- (void)clubListView:(PRVClubListView *)clubListView didSelectClubAtIndex:(NSInteger)index{
    [self.draggableView setStatusTitle:@"Информация о клубе"];
    [self.draggableView setLeftButtonHidden:NO];
    [self.clubVC loadClubWithID:self.clubs.list[index]._id lat:self.userMarker.position.latitude lng:self.userMarker.position.longitude];
    self.clubVC.view.hidden = NO;
    self.ticketDetailVC.view.hidden = YES;
    self.clubListView.hidden = YES;
}

- (void)loadMore:(PRVClubListView *)clubListView{
    NSMutableDictionary *params;
    if (self.lastFilterParameters) {
        params = [self.lastFilterParameters mutableCopy];
    } else {
        params = [[NSMutableDictionary alloc] init];
    }
    params[@"page"] = @(self.clubs.page + 1);
    [self filterClubsWithAdditionalParameters:params];
}

#pragma mark - GMSMapViewDelegate

- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker{
    if (marker == self.userMarker) {
        return YES;
    }
    PRVClubServerModel *club = marker.userData;
    [self.draggableView setStatusTitle:@"Информация о клубе"];
    self.clubVC.view.hidden = NO;
    self.ticketDetailVC.view.hidden = YES;
    self.clubListView.hidden = YES;
    [self.draggableView setLeftButtonHidden:NO];
    [self.clubVC loadClubWithID:club._id lat:self.userMarker.position.latitude lng:self.userMarker.position.longitude];
    [self.draggableView showOnHalfDraggableViewWithAnimation:YES];
    [self.mapView animateToZoom:12];
    return NO;
}

#pragma mark - PRVLocationServiceDelegate

- (void)locationService:(id<PRVLocationService>)locationService didUpdateLocation:(CLLocation *)location{
    if (self.userMarker) {
        self.userMarker.position = location.coordinate;
    } else {
        [self addUserMarkerWithPosition:location.coordinate];
    }
}

- (IBAction)ticketButtonPressed:(id)sender {
    [self.draggableView setStatusTitle:@"Информация о билете"];
    [self.draggableView setLeftButtonHidden:YES];
    self.ticketDetailVC.view.hidden = NO;
    self.clubVC.view.hidden = YES;
    self.clubListView.hidden = YES;
    [self.ticketDetailVC setTicket:[[[PRVAppManager sharedInstance] getUser] ticketForToday]];
    [self.draggableView showOnHalfDraggableViewWithAnimation:YES];
}

@end
