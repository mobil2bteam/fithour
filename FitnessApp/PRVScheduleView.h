//
//  PRVSheduleView.h
//  FitnessApp
//
//  Created by Ruslan on 8/24/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//
@class PRVScheduleView;
@class PRVDayCollectionViewCell;
@class PRVScheduleServerModel;

@protocol PRVScheduleViewDelegate <NSObject>
@required
- (void)scheduleView:(PRVScheduleView *)scheduleView didSelectSchedule:(PRVScheduleServerModel *)schedule;
@end

typedef void (^PRVScheduleViewConfigurationBlock)(PRVDayCollectionViewCell *cell, PRVScheduleServerModel *schedule);

#import <UIKit/UIKit.h>

@interface PRVScheduleView : UIView
@property (nonatomic, weak) id <PRVScheduleViewDelegate> delegate;
@property (nonatomic, strong) PRVScheduleServerModel *selectedSchedule;

- (void)setSchedules:(NSArray <NSArray <PRVScheduleServerModel *> *> *)schedules; // Shedules should be in format: [weeks][days]

// This method allows to add additional configuration if it's needed
- (void)setConfigureBlock:(PRVScheduleViewConfigurationBlock)configureBlock;
- (void)setAllowsSelection:(BOOL)allow;
@end
