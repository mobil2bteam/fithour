//
//  PRVSectionedDataSource.m
//  FitnessApp
//
//  Created by Ruslan on 8/29/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVSectionedDataSource.h"

@interface PRVSectionedDataSource ()
@property (nonatomic, strong) NSArray <NSArray *> *items;
@property (nonatomic, copy) NSString *cellIdentifier;
@property (nonatomic, copy) PRVSectionedCellConfigureBlock configureCellBlock;
@end

@implementation PRVSectionedDataSource

- (id)init
{
    return nil;
}

- (id)initWithItems:(NSArray <NSArray *> *)items
     cellIdentifier:(NSString *)cellIdentifier
 configureCellBlock:(PRVSectionedCellConfigureBlock)configureCellBlock
{
    self = [super init];
    if (self) {
        self.items = items;
        self.cellIdentifier = cellIdentifier;
        self.configureCellBlock = [configureCellBlock copy];
    }
    return self;
}

- (id)itemAtIndexPath:(NSIndexPath *)indexPath
{
    return self.items[indexPath.section][indexPath.row];
}

- (void)setItems:(NSArray <NSArray *> *)items{
    _items = items;
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.items.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.items[section].count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:self.cellIdentifier
                                                            forIndexPath:indexPath];
    id item = [self itemAtIndexPath:indexPath];
    self.configureCellBlock(cell, item, indexPath);
    return cell;
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return self.items.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.items[section].count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:self.cellIdentifier forIndexPath:indexPath];
    id item = [self itemAtIndexPath:indexPath];
    self.configureCellBlock(cell, item, indexPath);
    return cell;
}

@end
