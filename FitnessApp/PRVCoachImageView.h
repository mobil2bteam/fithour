//
//  PRVCoachImageView.h
//  FitnessApp
//
//  Created by Ruslan on 9/19/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PRVCoachImageView : UIImageView

@property (nonatomic, assign, readonly) CGFloat gradientBorderWidth;

- (void)setGradientBorderWidth:(CGFloat)gradientBorderWidth;

- (void)showGradientBorder:(BOOL)show;

@end
