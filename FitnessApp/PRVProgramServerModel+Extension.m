//
//  PRVProgramServerModel+Extension.m
//  FitnessApp
//
//  Created by Ruslan on 8/24/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVProgramServerModel+Extension.h"

@implementation PRVProgramServerModel (Extension)


- (NSArray<PRVScheduleServerModel *> *)scheduleForWeek{
    NSMutableArray <PRVScheduleServerModel *> *temp = [[NSMutableArray alloc] init];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"d.MM.y";
    
    for (NSInteger i = 0; i < 7; i++) {
        for (PRVScheduleServerModel *shedule in self.schedule) {
            NSDate *date = [formatter dateFromString:shedule.date];
            NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
            [gregorian setFirstWeekday:2]; // Sunday == 1, Saturday == 7
            NSUInteger weekday = [gregorian ordinalityOfUnit:NSCalendarUnitWeekday inUnit:NSCalendarUnitWeekOfMonth forDate:date];
            if (weekday == i + 1) {
                [temp addObject:shedule];
                break;
            }
        }
    }
    return [temp copy];
}

- (NSArray <NSArray<PRVScheduleServerModel *> *> *)splitScheduleByWeeks{
    NSMutableArray *weeksArray = [[NSMutableArray alloc] init];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"d.MM.y";
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSMutableArray <PRVScheduleServerModel *> *daysArray = [[NSMutableArray alloc] init];
    
    for (PRVScheduleServerModel *schedule in self.schedule) {
        NSDate *date = [formatter dateFromString:schedule.date];
        NSInteger dayIndex = [calendar component:NSCalendarUnitWeekday fromDate:date];
        if (daysArray.count == 0) {
            [daysArray addObject:schedule];
        } else {
            NSDate *lastDate = [formatter dateFromString:daysArray.lastObject.date];
            NSInteger lastDayIndex = [calendar component:NSCalendarUnitWeekday fromDate:lastDate];
            // if current day index is bigger than last day index, then add the to current week, else crate new week and add it there
            if (dayIndex > lastDayIndex) {
                [daysArray addObject:schedule];
            } else {
                [weeksArray addObject:[daysArray copy]];
                daysArray = [[NSMutableArray alloc] init];
                [daysArray addObject:schedule];
            }
        }
    }
    [weeksArray addObject:daysArray];
    return [weeksArray copy];
}

@end
