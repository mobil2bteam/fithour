//
//  PRVFilterVC.h
//  FitnessApp
//
//  Created by Ruslan on 7/16/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

@class PRVFilterManager;
@class PRVGroupServerModel;
@class PRVPriceServerModel;
@class PRVDateModel;

#import <UIKit/UIKit.h>

@interface PRVFilterVC : UIViewController

@property (nonatomic, strong) PRVFilterManager *filterManager;
@property (weak, nonatomic) IBOutlet UICollectionView *groupsCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *datesCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *timesCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *pricesCollectionView;
@property (nonatomic, strong) NSMutableArray <PRVGroupServerModel *> *selectedGroupsArray;
@property (nonatomic, strong) NSString *selectedTime;
@property (nonatomic, strong) PRVDateModel *selectedDate;
@property (nonatomic, strong) PRVPriceServerModel *selectedPrice;
@property (nonatomic, copy) void (^filterCallback)(void);

@end
