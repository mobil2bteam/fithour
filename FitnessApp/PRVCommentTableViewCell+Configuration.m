//
//  PRVCommentTableViewCell+Configuration.m
//  FitnessApp
//
//  Created by Ruslan on 8/25/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVCommentTableViewCell+Configuration.h"
#import "PRVClubsServerModel.h"
#import "UILabel+PRVExtension.h"

@implementation PRVCommentTableViewCell (Configuration)

- (void)configureCellForReview:(PRVReviewServerModel *)review{
    self.dateLabel.text = review.date;
    self.userNameLabel.text = review.user;
    [self.commentLabel setHTML:review.comment];
    [self setNick:review.user];
    [self setRating:review.rating];
}

@end
