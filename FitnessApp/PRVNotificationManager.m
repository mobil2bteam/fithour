//
//  PRVNotificationManager.m
//  FitnessApp
//
//  Created by Ruslan on 9/25/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

#import "PRVNotificationManager.h"
#import <UserNotifications/UserNotifications.h>
#import "PRVUserServerModel.h"
#import "PRVClubsServerModel.h"

@implementation PRVNotificationManager

+ (void)registerNotifications{
    if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0")){
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            if(!error){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication] registerForRemoteNotifications];
                });
            }
        }];
    }
    else {
        // Code for old versions
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound categories:nil]];
    }
}

+ (void)addNotificationForTicket:(PRVTicketSeverModel *)ticket{
    NSString *messageStart = [NSString stringWithFormat:@"У вас запись на %@ в %@. Клуб %@, %@", ticket.date, ticket.time_from, ticket.club.name, ticket.club.address];
    NSString *messageEnd = @"Тренировка закончилась. У вас есть 15 минут для выхода";
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"d.MM.y HH:mm"];
    
    NSString *alarmStartDateString = [NSString stringWithFormat:@"%@ %@", ticket.date, ticket.time_from];
    NSString *alarmEndDateString = [NSString stringWithFormat:@"%@ %@", ticket.date, ticket.time_to];
    
    NSDate *alarmStartDate = [formatter dateFromString:alarmStartDateString];
    NSDate *alarmPreStartDate = [alarmStartDate dateByAddingTimeInterval:- 60 * 60];
    NSDate *alarmEndDate = [formatter dateFromString:alarmEndDateString];
    
    NSString *notificationIdPreStart = [NSString stringWithFormat:@"%ld-prestart", (long)ticket._id];
    NSString *notificationIdStart = [NSString stringWithFormat:@"%ld-start", (long)ticket._id];
    NSString *notificationIdEnd = [NSString stringWithFormat:@"%ld-end", (long)ticket._id];

    [self addNotificationWithAlert:@"Не забудьте!" message:messageStart fireDate:alarmStartDate notificationId:notificationIdStart];
    
    [self addNotificationWithAlert:@"Внимание!" message:messageStart fireDate:alarmPreStartDate notificationId:notificationIdPreStart];

    [self addNotificationWithAlert:@"Внимание!" message:messageEnd fireDate:alarmEndDate notificationId:notificationIdEnd];
}

+ (void)removeNotificationForTicket:(PRVTicketSeverModel *)ticket{
    NSString *notificationIdStart = [NSString stringWithFormat:@"%ld-start", (long)ticket._id];
    NSString *notificationIdEnd = [NSString stringWithFormat:@"%ld-end", (long)ticket._id];
    NSString *notificationIdPreStart = [NSString stringWithFormat:@"%ld-prestart", (long)ticket._id];

    if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0")){
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        [center removePendingNotificationRequestsWithIdentifiers:@[notificationIdStart, notificationIdEnd]];
        NSMutableArray <NSString *> *identifiers = [[NSMutableArray alloc] init];
        [center getPendingNotificationRequestsWithCompletionHandler:^(NSArray<UNNotificationRequest *> * _Nonnull requests) {
            for (UNNotificationRequest *request in requests) {
                if ([request.identifier isEqualToString:notificationIdStart] || [request.identifier isEqualToString:notificationIdEnd] ||  [request.identifier isEqualToString:notificationIdPreStart]) {
                    [identifiers addObject:request.identifier];
                }
            }
        }];
        [center removePendingNotificationRequestsWithIdentifiers:identifiers];
    }
    else {
        for (UILocalNotification *notification in [[UIApplication sharedApplication] scheduledLocalNotifications]) {
            NSDictionary *userInfoDict = notification.userInfo;
            NSString *notificationId = [NSString stringWithFormat:@"%@", [userInfoDict valueForKey:@"id"]];
            if ([notificationId isEqualToString:notificationIdStart] || [notificationId isEqualToString:notificationIdEnd] || [notificationId isEqualToString:notificationIdPreStart]){
                [[UIApplication sharedApplication] cancelLocalNotification:notification];
            }
        }
    }
}

+ (void)addNotificationWithAlert:(NSString *)alert message:(NSString *)message fireDate:(NSDate *)fireDate notificationId:(NSString *)notificationId {
    if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0")){
        UNMutableNotificationContent *localNotification = [UNMutableNotificationContent new];
        localNotification.title = [NSString localizedUserNotificationStringForKey:alert arguments:nil];
        localNotification.body = [NSString localizedUserNotificationStringForKey:message arguments:nil];
        localNotification.sound = [UNNotificationSound defaultSound];
        
        NSDateComponents *triggerDate = [[NSCalendar currentCalendar]
                                         components:NSCalendarUnitYear +
                                         NSCalendarUnitMonth + NSCalendarUnitDay +
                                         NSCalendarUnitHour + NSCalendarUnitMinute +
                                         NSCalendarUnitSecond fromDate:fireDate];
        
        UNCalendarNotificationTrigger *trigger = [UNCalendarNotificationTrigger triggerWithDateMatchingComponents:triggerDate repeats:NO];
        
        localNotification.badge = @([[UIApplication sharedApplication] applicationIconBadgeNumber] +1);
        
        UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:notificationId content:localNotification trigger:trigger];
        
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        [center addNotificationRequest:request withCompletionHandler:nil];
    }
    else {
        UILocalNotification *localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = fireDate;
        localNotification.timeZone = [NSTimeZone systemTimeZone];
        localNotification.repeatInterval = 0;
        localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
        localNotification.alertTitle = alert;
        localNotification.alertBody = message;
        localNotification.userInfo = @{@"id":notificationId};
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
}

@end
