//
//  PRVActiveTicketView.h
//  FitnessApp
//
//  Created by Ruslan on 9/21/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class PRVTicketSeverModel;

@interface PRVActiveTicketView : UIView
- (void)prepareViewForTicket:(PRVTicketSeverModel *)ticket;
@property (nonatomic, copy) void (^ticketBlock)(PRVTicketSeverModel *ticket);
@end
