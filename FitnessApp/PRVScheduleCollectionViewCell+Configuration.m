//
//  PRVSheduleCollectionViewCell+Configuration.m
//  FitnessApp
//
//  Created by Ruslan on 8/24/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVScheduleCollectionViewCell+Configuration.h"
#import "PRVOptionsServerModel.h"
#import "PRVClubsServerModel.h"
#import "UIImageView+PRVExtension.h"
#import "UIView+PRVExtensions.h"

@implementation PRVScheduleCollectionViewCell (Configuration)

- (void)configureCellForHour:(PRVHourServerModel *)hour{
    self.durationLabel.text = [NSString stringWithFormat:@"%ld мин.", (long)hour.duration];
    self.timeStartLabel.text = hour.from;
    if (hour.places) {
        self.placesLabel.text = [NSString stringWithFormat:@"%ld шт.", (long)hour.places];
    } else {
        self.placesLabel.text = @"Нет";
    }
}

- (void)configureCellForCoach:(PRVCoachServerModel *)coach{
    if (coach) {
        self.coachLabel.text = coach.name;
        [self.coachImageView prv_setImageWithURL:coach.image];
        self.coachView.hidden = NO;
    }
}

@end
