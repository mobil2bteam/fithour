//
//  PRVEnrollCoachVC.m
//  FitnessApp
//
//  Created by Ruslan on 9/18/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVEnrollCoachVC.h"
#import "PRVServerManager.h"
#import "UIViewController+PRVExtensions.h"
#import "PRVClubsServerModel.h"
#import "PRVCoachScheduleServerModel.h"
#import "PRVDayCollectionViewCell+Configuration.h"
#import "PRVSectionedDataSource.h"
#import "PRVOptionsServerModel.h"
#import "UIImageView+PRVExtension.h"
#import "PRVProgramConfirmVC.h"

static NSString * const kDayCollectionViewCellIdentifier = @"PRVDayCollectionViewCell";

@interface PRVEnrollCoachVC () <UITableViewDelegate>
@property (nonatomic, strong) PRVCoachScheduleServerModel *coachSchedule;
@property (nonatomic, weak) IBOutlet UIImageView *coachImageView;
@property (nonatomic, weak) IBOutlet UILabel *coachLabel;
@property (nonatomic, weak) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UITableView *timesTableView;
@property (nonatomic, strong) PRVSectionedDataSource *daysCollectionViewDataSource;
@property (nonatomic, strong) PRVDataSource *timesTableViewDataSource;
@end


@implementation PRVEnrollCoachVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadCoachSchedule];
    [self setupTimesTableView];
    self.title = self.coach.name;
    [self.coachImageView prv_setImageWithURL:self.coach.image];
    self.coachLabel.text = self.coach.name;
    self.priceLabel.text = [NSString stringWithFormat:@"%ld руб.", (long)self.coach.price_hour + self.club.price_free];
}

- (void)setupTimesTableView{
    PRVCellConfigureBlock configureCell = ^(UITableViewCell *cell, PRVTimeServerModel *time) {
        cell.textLabel.text = time.value;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        if ([time canEnrollForDate:self.selectedDay.date]) {
            cell.backgroundColor = [UIColor whiteColor];
            cell.contentView.backgroundColor = [UIColor whiteColor];
            cell.textLabel.textColor = [UIColor blackColor];
        } else {
            cell.textLabel.textColor = [UIColor darkGrayColor];
            cell.backgroundColor = [UIColor backgroundColor];
            cell.contentView.backgroundColor = [UIColor backgroundColor];
        }
    };
    self.timesTableViewDataSource = [[PRVDataSource alloc] initWithItems:self.selectedDay.times cellIdentifier:@"cell" configureCellBlock:configureCell];
    self.timesTableView.dataSource = self.timesTableViewDataSource;
    [self.timesTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
}

- (void)setupDaysCollectionView{
    __weak typeof(self) weakSelf = self;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"d.MM.y";
    
    NSDateFormatter *formatter2 = [[NSDateFormatter alloc] init];
    formatter2.dateFormat = @"d.MM";

    PRVSectionedCellConfigureBlock configureCell = ^(PRVDayCollectionViewCell *cell, PRVScheduleServerModel *schedule, NSIndexPath *indexPath) {
        typeof(self) strongSelf = weakSelf;
        NSString *date = [formatter2 stringFromDate:[formatter dateFromString:schedule.date]];
        cell.dayLabel.text = date;
        cell.timeLabel.text = [schedule formattedDay];
        cell.backgroundColor = indexPath.section % 2 == 0 ? [UIColor whiteColor] : [UIColor backgroundColor];
        if (schedule == strongSelf.selectedDay) {
            [strongSelf.daysCollectionView selectItemAtIndexPath:indexPath animated:NO scrollPosition:UICollectionViewScrollPositionNone];
            [cell setSelected:YES];
        }
    };

    ((UICollectionViewFlowLayout *)self.daysCollectionView.collectionViewLayout).estimatedItemSize = CGSizeMake(60, 60);
    self.daysCollectionViewDataSource = [[PRVSectionedDataSource alloc] initWithItems:[self.coachSchedule splitScheduleByWeeks] cellIdentifier:kDayCollectionViewCellIdentifier configureCellBlock:configureCell];
    self.daysCollectionView.dataSource = self.daysCollectionViewDataSource;
    [self.daysCollectionView registerNib:[PRVDayCollectionViewCell prv_nib] forCellWithReuseIdentifier:kDayCollectionViewCellIdentifier];
    [self.daysCollectionView reloadData];
}

#pragma mark - Methods

- (void)loadCoachSchedule{
    [self prv_addMBProgressHUD];
    [[PRVServerManager sharedManager] scheduleForCoachWithId:self.coach._id onSuccess:^(PRVCoachScheduleServerModel *schedule, NSString *error) {
        [self prv_hideMBProgressHUD];
        if (error) {
            [self prv_showMessage:error withTitle:nil];
        } else {
            self.coachSchedule = schedule;
            if ([self.coachSchedule splitScheduleByWeeks].count) {
                if ([self.coachSchedule splitScheduleByWeeks][0].count) {
                    self.selectedDay = [self.coachSchedule splitScheduleByWeeks][0][0]; // first day of first week
                }
            }
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [self prv_hideMBProgressHUD];
        [self prv_showMessage:error.localizedDescription withTitle:nil];
    }];
}

#pragma mark - Setters

- (void)setCoachSchedule:(PRVCoachScheduleServerModel *)coachSchedule{
    _coachSchedule = coachSchedule;
    [self setupDaysCollectionView];
}

- (void)setSelectedDay:(PRVScheduleServerModel *)selectedDay{
    _selectedDay = selectedDay;
    [self.timesTableViewDataSource setItems:self.selectedDay.times];
    [self.timesTableView reloadData];
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView == self.daysCollectionView) {
        [collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        PRVScheduleServerModel *schedule = [self.coachSchedule splitScheduleByWeeks][indexPath.section][indexPath.row];
        self.selectedDay = schedule;
        [self.timesTableViewDataSource setItems:self.selectedDay.times];
        [self.timesTableView reloadData];
    }
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    cell.backgroundColor = indexPath.section % 2 == 0 ? [UIColor whiteColor] : [UIColor backgroundColor];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    PRVProgramConfirmVC *vc = [[PRVProgramConfirmVC alloc] prv_initWithNib];
    vc.enrollType = PRVEnrollTicketTypePersonalCoach;
    vc.date = self.selectedDay.date;
    vc.price = self.coach.price_hour + self.club.price_free;
    vc.coach = self.coach;
    vc.club  = self.club;
    vc.timeModel = [self.timesTableViewDataSource itemAtIndexPath:indexPath];
    [self.navigationController pushViewController:vc animated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath{
    PRVTimeServerModel *time = [self.timesTableViewDataSource itemAtIndexPath:indexPath];
    return [time canEnrollForDate:self.selectedDay.date];
}

@end
