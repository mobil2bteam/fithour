//
//  PRVSignUpVC.m
//  FitnessApp
//
//  Created by Ruslan on 7/19/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVSignUpVC.h"
#import "PRVSignUpVC+Navigation.h"
#import "PRVServerManager.h"
#import "UIView+PRVExtensions.h"
#import "AFNetworking.h"
#import "AFHTTPSessionOperation.h"
#import "PRVAppManager.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "PRVOptionsServerModel.h"
#import "PRVUserServerModel.h"

@interface PRVSignUpVC () <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UITextField *phoneTextField;
@property (nonatomic, weak) IBOutlet UITextField *loginTextField;
@property (nonatomic, weak) IBOutlet UITextField *passportTextField;

@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UILabel *enrollTextLabel;
@property (weak, nonatomic) IBOutlet UIButton *signUpButton;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UIView *passportView;
@property (weak, nonatomic) IBOutlet UIView *faceVeiw;
@property (weak, nonatomic) IBOutlet UIImageView *faceImageView;
@property (weak, nonatomic) IBOutlet UIImageView *passportImageView;
@property (strong, nonatomic) UIImage *faceImage;
@property (strong, nonatomic) UIImage *passportImage;
@property (assign, nonatomic) BOOL isFaceImage;

@end

@implementation PRVSignUpVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Регистрация";
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc]initWithTitle:@"Закрыть" style:UIBarButtonItemStylePlain target:self action:@selector(dismissBarButtonPressed:)];
    self.navigationItem.rightBarButtonItem = rightButton;
    if (self.sighInForBuyTicket) {
        [self.signUpButton setTitle:@"Записаться" forState:UIControlStateNormal];
        self.enrollTextLabel.hidden = NO;
        self.loginButton.hidden = NO;
        self.logoImageView.hidden = YES;
    }
    self.passportView.layer.borderColor = [UIColor groupTableViewBackgroundColor].CGColor;
    self.faceVeiw.layer.borderColor = [UIColor groupTableViewBackgroundColor].CGColor;
    self.faceVeiw.layer.borderWidth = 1.0;
    self.passportView.layer.borderWidth = 1.0;
}

#pragma mark - Actions

- (IBAction)dismissBarButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)loginButtonPressed:(id)sender {
    [self showLoginViewController];
}

- (IBAction)signUpButtonPressed:(id)sender {
    if (!self.loginTextField.text.length) {
        [self.loginTextField prv_shake];
        return;
    }
    if (!self.phoneTextField.text.length) {
        [self.phoneTextField prv_shake];
        return;
    }
    if (!self.passportTextField.text.length) {
        [self.passportTextField prv_shake];
        return;
    }
    if (self.faceImage == nil) {
        [self.faceVeiw prv_shake];
        return;
    }
    if (self.passportImage == nil) {
        [self.passportView prv_shake];
        return;
    }
    [self signUp];
}

- (IBAction)faceButtonPressed:(id)sender {
    self.isFaceImage = YES;
    [self takePhoto];
}

- (IBAction)passportButtonPressed:(id)sender {
    self.isFaceImage = NO;
    [self takePhoto];
}


#pragma mark - Methods

- (void)takePhoto{
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera]) {
        UIImagePickerController * picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.allowsEditing = NO;
        [self presentViewController:picker animated:YES completion:nil];
    } else {
        [self prv_showMessage:@"Камера не доступна на вашем устройстве" withTitle:nil];
    }
}

- (void)signUp{
    NSData *faceData = UIImageJPEGRepresentation(self.faceImage, 1.0);
    NSData *passportData =  UIImageJPEGRepresentation(self.passportImage, 1.0);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];  // allocate AFHTTPManager
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    hud.mode = MBProgressHUDModeAnnularDeterminate;
    hud.label.text = @"Загрузка изображения";
    
    PRVUrlServerModel *url = [[[PRVAppManager sharedInstance] getOptions] urlForName:@"url_registration_short"];
    
    [manager POST:url.url parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:faceData name:@"image" fileName:@"image.jpg" mimeType:@"image/jpeg"];
        
        [formData appendPartWithFileData:passportData name:@"image2" fileName:@"image.jpg" mimeType:@"image/jpeg"];
        
        [formData appendPartWithFormData:[self.email dataUsingEncoding:NSUTF8StringEncoding]
                                    name:@"email"];
        
        [formData appendPartWithFormData:[self.code dataUsingEncoding:NSUTF8StringEncoding]
                                    name:@"verify-code"];

        [formData appendPartWithFormData:[self.phoneTextField.text dataUsingEncoding:NSUTF8StringEncoding]
                                    name:@"phone"];
        
        [formData appendPartWithFormData:[self.password dataUsingEncoding:NSUTF8StringEncoding]
                                    name:@"password"];
        
        [formData appendPartWithFormData:[self.passportTextField.text dataUsingEncoding:NSUTF8StringEncoding]
                                    name:@"passport"];
        
        [formData appendPartWithFormData:[self.loginTextField.text dataUsingEncoding:NSUTF8StringEncoding]
                                    name:@"login"];
        
        [formData appendPartWithFormData:[self.loginTextField.text dataUsingEncoding:NSUTF8StringEncoding]
                                    name:@"name"];

    } progress:^(NSProgress * _Nonnull uploadProgress) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [hud setProgress:uploadProgress.fractionCompleted];
        });
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@", responseObject);
        if (responseObject[@"error"]) {
            NSString *error = responseObject[@"error"][@"text"];
            [self prv_showMessage:error withTitle:@"Ошибка"];
        } else if (responseObject[@"user"]) {
            PRVUserServerModel *user = [EKMapper objectFromExternalRepresentation:responseObject withMapping:[PRVUserServerModel objectMapping]];
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setObject:user.info.accessToken forKey:@"token"];
            [userDefaults synchronize];
            [[PRVAppManager sharedInstance] setUser:user];
            [self dismissViewControllerAnimated:YES completion:^{
                if (self.loginCallback) {
                    self.loginCallback();
                }
            }];
        }
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
        });
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self prv_showMessage:error.localizedDescription withTitle:@"Ошибка"];
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [MBProgressHUD hideHUDForView:self.navigationController.view animated:YES];
        });
    }];
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    [picker dismissViewControllerAnimated:YES completion:NULL];
    UIImage *pickedImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    if (pickedImage) {
        if (self.isFaceImage) {
            self.faceImage = pickedImage;
            self.faceImageView.image = pickedImage;
        } else {
            self.passportImage = pickedImage;
            self.passportImageView.image = pickedImage;
        }
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.loginTextField) {
        [self.phoneTextField becomeFirstResponder];
    }
    if (textField == self.phoneTextField) {
        [self.passportTextField becomeFirstResponder];
    }
    [textField resignFirstResponder];
    return  YES;
}
@end
