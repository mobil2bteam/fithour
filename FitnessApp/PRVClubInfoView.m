//
//  PRVClubInfoView.m
//  FitnessApp
//
//  Created by Ruslan on 7/27/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVClubInfoView.h"
#import "PRVClubsServerModel.h"
#import "UIImageView+PRVExtension.h"

@interface PRVClubInfoView()
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *clubLabel;
@property (weak, nonatomic) IBOutlet UIImageView *clubImageView;
@property (nonatomic, strong) PRVClubServerModel *club;
@end

@implementation PRVClubInfoView

- (void)awakeFromNib{
    [super awakeFromNib];
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [[NSBundle mainBundle]loadNibNamed:@"PRVClubInfoView" owner:self options:nil];
        [self addSubview:_contentView];
        _contentView.frame = self.bounds;
    }
    return self;
}

- (void)setClub:(PRVClubServerModel *)club{
    _club = club;
    [self.clubImageView prv_setImageWithURL:club.logo];
    self.addressLabel.text = club.address;
    self.clubLabel.text = club.name;
}

- (void)setAddressLabelFontSize:(NSInteger)size{
    self.addressLabel.font = [self.addressLabel.font fontWithSize:size];
}

@end
