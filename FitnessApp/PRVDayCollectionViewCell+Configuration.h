//
//  PRVDayCollectionViewCell+Configuration.h
//  FitnessApp
//
//  Created by Ruslan on 8/24/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//
@class PRVScheduleServerModel;

#import "PRVDayCollectionViewCell.h"

@interface PRVDayCollectionViewCell (Configuration)

- (void)configureCellWithShedule:(PRVScheduleServerModel *)shedule;
- (void)configureCellWithSheduleForDate:(PRVScheduleServerModel *)schedule;
- (void)configureCellForDate:(NSDate *)date;

@end
