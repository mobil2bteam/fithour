//
//  PRVInfoVC.m
//  FitnessApp
//
//  Created by Ruslan on 7/20/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//
#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

#import "PRVInfoVC.h"
#import "PRVAboutViewController.h"
#import "PRVSupportViewController.h"
#import "PRVRatingViewController.h"

@interface PRVInfoVC () <UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *infoTableView;

@property (nonatomic, strong) NSArray <NSString *> *infoOptionsArray;

@end

@implementation PRVInfoVC

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Информация";
    self.infoOptionsArray = @[@"О проекте",
                              @"Техническая поддержка",
                              @"Оценить проект",
                              @"Пользовательское соглашение",
                              @"Политика конфиденциальности"];
    [self.infoTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    // hide emty cells in table view
    self.infoTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)openURL:(NSString *)urlString{
    NSURL *url = [NSURL URLWithString:urlString];
    if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0")){
        [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
    } else {
        [[UIApplication sharedApplication] openURL:url];
    }
}

- (void)showAboutViewController{
    PRVAboutViewController *vc = [[PRVAboutViewController alloc] prv_initWithNib];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)showSupportViewController{
    PRVSupportViewController *vc = [[PRVSupportViewController alloc] prv_initWithNib];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)showFeedbackViewController{
    PRVRatingViewController *vc = [[PRVRatingViewController alloc] prv_initWithNib];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return self.infoOptionsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.textLabel.text = self.infoOptionsArray[indexPath.row];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 0:
            [self showAboutViewController];
            break;
        case 1:
            [self showSupportViewController];
            break;
        case 2:
            [self showFeedbackViewController];
            break;
        case 3:
            [self openURL:@"http://fithour.ru/files/licenzionnoe_soglashenie.html"];
            break;
        case 4:
            [self openURL:@"http://fithour.ru/files/politika_confidicialnosty.html"];
            break;
        default:
            break;
    }
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

@end
