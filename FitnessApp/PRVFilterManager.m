//
//  PRVFilterManager.m
//  FitnessApp
//
//  Created by Ruslan on 8/13/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVFilterManager.h"
#import "PRVAppManager.h"
#import "PRVOptionsServerModel.h"

@implementation PRVDateModel
@end

@interface PRVFilterManager()

@property (nonatomic, strong) NSArray <PRVPriceServerModel *> *pricesOptions;
@property (nonatomic, strong) NSArray <PRVGroupServerModel *> *groupsOptions;
@property (nonatomic, strong) NSArray <PRVDateModel *> *datesOptions;
@property (nonatomic, strong) NSArray <NSString *> *timesOptions;
@property (nonatomic, strong) PRVPriceServerModel *selectedPrice;
@property (nonatomic, strong) NSArray <PRVGroupServerModel *> *selectedGroups;
@property (nonatomic, strong) NSString *selectedTime;
@property (nonatomic, strong) PRVDateModel *selectedDate;
@property (nonatomic) PRVSortMode sortMode;

@end

@implementation PRVFilterManager

#pragma mark - Lifecycle

+ (instancetype)sharedInstance{
    static PRVFilterManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[PRVFilterManager alloc] init];
        [manager setDefaultValues];
    });
    return manager;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setDefaultValues];
    }
    return self;
}

#pragma mark - Methods

- (void)setDefaultValues{
    _sortMode = PRVSortModeDistance;
    _pricesOptions = [[PRVAppManager sharedInstance] getOptions].price_list;
    _groupsOptions = [[PRVAppManager sharedInstance] getOptions].group_list;
    
    // configure date options
    NSMutableArray *temp = [[NSMutableArray alloc] init];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"d.MM.y";
    
    NSDateFormatter *dayNameFormatter = [[NSDateFormatter alloc] init];
    [dayNameFormatter setDateFormat:@"EE"];
    
    NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
    for (NSInteger i = 0; i < 30; i++) {
        dayComponent.day = i;
        NSDate *day = [[NSCalendar currentCalendar] dateByAddingComponents:dayComponent toDate:[NSDate date] options:0];
        PRVDateModel *date = [[PRVDateModel alloc] init];
        date.value = [formatter stringFromDate:day];
        date.text = [NSString stringWithFormat:@"%@ (%@)", [formatter stringFromDate:day], [dayNameFormatter stringFromDate:day]];
        if (i == 0) {
            date.text = @"Сегодня";
        }
        [temp addObject:date];
    }
    _datesOptions = [temp copy];
}

#pragma mark - Getters

- (PRVSortMode)getSortMode{
    return _sortMode;
}

- (NSArray <PRVDateModel *> *)getDatesOptions{
    return _datesOptions;
}

- (NSArray <NSString *> *)getTimesOptions{
    NSMutableArray *times = [[NSMutableArray alloc] init];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH"];
    
    NSDate *startHour = [formatter dateFromString:@"06"];
    NSDate *endHour = [formatter dateFromString:@"01"];
    
    if ([self isSelectedDateToday]) {
        NSDate *currentHour = [formatter dateFromString:[formatter stringFromDate:[NSDate date]]];
        [formatter setDateFormat:@"HH:mm"];
        if ([currentHour compare:startHour] == NSOrderedDescending) {
            // start hour is later than current hour
            startHour = [currentHour dateByAddingTimeInterval:60 * 60];
        }
    }
    
    [formatter setDateFormat:@"HH:mm"];
    for (NSInteger i = 0; ; i++) {
        [times addObject:[formatter stringFromDate:startHour]];
        startHour = [startHour dateByAddingTimeInterval:3600];
        startHour = [formatter dateFromString:[formatter stringFromDate:startHour]];
        if ([startHour compare:endHour] == NSOrderedSame) {
            break;
        }
    }

    return [times copy];
}

- (BOOL)isSelectedDateToday{
    if (!_selectedDate) {
        return NO;
    }
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"d.MM.y";
    
    NSString *todayString = [formatter stringFromDate:[NSDate date]];
    if ([todayString isEqualToString:_selectedDate.value]) {
        return YES;
    }
    
    return NO;
}

- (NSArray <PRVPriceServerModel *> *)getPricesOptions{
    return _pricesOptions;
}

- (NSArray <PRVGroupServerModel *> *)getGroupsOptions{
    return _groupsOptions;
}

- (PRVDateModel *)getSelectedDate{
    return _selectedDate;
}

- (NSString *)getSelectedTime{
    return _selectedTime;
}

- (PRVPriceServerModel *)getSelectedPrice{
    return _selectedPrice;
}

- (NSArray <PRVGroupServerModel *> *)getSelectedGroups{
    return _selectedGroups;
}

#pragma mark - Setters

- (void)setSortMode:(PRVSortMode)sortMode{
    _sortMode = sortMode;
}

- (void)setSelectedDate:(PRVDateModel *)date{
    _selectedDate = date;
}

- (void)setSelectedTime:(NSString *)time{
    _selectedTime = time;
}

- (void)setSelectedPrice:(PRVPriceServerModel *)price{
    _selectedPrice = price;
}

- (void)setSelectedGroups:(NSArray <PRVGroupServerModel *> *)groups{
    _selectedGroups = groups;
}

@end
