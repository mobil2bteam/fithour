//
//  PRVProgramConfirmVC.h
//  FitnessApp
//
//  Created by Ruslan on 7/31/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//
@class PRVClubServerModel;
@class PRVProgramServerModel;
@class PRVGroupServerModel;
@class PRVHourServerModel;
@class PRVCoachServerModel;
@class PRVTimeServerModel;

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, PRVEnrollTicketType) {
    PRVEnrollTicketTypeFreeSchedule,
    PRVEnrollTicketTypePersonalCoach,
    PRVEnrollTicketTypeDefault,
};

@interface PRVProgramConfirmVC : UIViewController
@property (nonatomic, strong) PRVClubServerModel *club;
@property (nonatomic, strong) PRVGroupServerModel *group;
@property (nonatomic, strong) PRVProgramServerModel *program;
@property (nonatomic, strong) PRVCoachServerModel *coach;
@property (nonatomic, strong) PRVHourServerModel *hour;
@property (nonatomic, strong) PRVTimeServerModel *timeModel;
@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSString *time;
@property (nonatomic, assign) NSInteger price;
@property (nonatomic, assign) PRVEnrollTicketType enrollType;

@end
