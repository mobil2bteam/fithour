//
//  PRVLocationServiceDelegate.h
//  FitnessApp
//
//  Created by Ruslan on 9/7/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 Delegate returns updated location info.
 */
@protocol PRVLocationService;

@class CLLocation;

@protocol PRVLocationServiceDelegate <NSObject>

/**
 Method returns location obtained from CLLocationManager.
 */
- (void)locationService:(id<PRVLocationService>)locationService didUpdateLocation:(CLLocation *)location;

@end
