//
//  PRVUserServerModel.m
//  FitnessApp
//
//  Created by Ruslan on 7/19/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVUserServerModel.h"
#import "PRVOptionsServerModel.h"
#import "PRVClubsServerModel.h"

static const NSInteger kMinutesToExit = 15;

@implementation PRVUserServerModel

+ (EKObjectMapping *)objectMapping{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasOne:[PRVInfoServerModel class] forKeyPath:@"user.info" forProperty:@"info"];
    }];
}

- (void)addTicketToUser:(PRVTicketSeverModel *)ticket{
    NSMutableArray <PRVTicketSeverModel *> *temp = [self.info.tickets mutableCopy];
    [temp addObject:ticket];
    self.info.tickets = [temp copy];
}

- (PRVTicketSeverModel *)ticketForClub:(NSInteger)clubId{
    PRVTicketSeverModel *tempTicket;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"d.MM.y";
    
    for (PRVTicketSeverModel *ticket in self.info.tickets) {
        if ([ticket isCompleted] || [ticket isCanceled]) {
            continue;
        }
        if (ticket.club._id == clubId) {
            NSDate *ticketDate = [formatter dateFromString:ticket.date];
            BOOL today = [[NSCalendar currentCalendar] isDateInToday:ticketDate];
            if (today || [ticketDate compare:[NSDate date]] == NSOrderedDescending) {
                
                if (tempTicket) {
                    if ([[formatter dateFromString:ticket.date] compare:ticketDate] == NSOrderedAscending) {
                        tempTicket = ticket;
                    }
                } else {
                    tempTicket = ticket;
                }
            }
        }
    }
    
    return tempTicket;
}

- (PRVTicketSeverModel *)ticketForToday{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"d.MM.y";
    NSString *todayString = [formatter stringFromDate:[NSDate date]];
    NSDate *today = [formatter dateFromString:todayString];
    
    for (PRVTicketSeverModel *ticket in self.info.tickets) {
        if ([ticket isCompleted] || [ticket isCanceled]) {
            continue;
        }
        NSDate *currentDate = [formatter dateFromString:ticket.date];
        if (!ticket.dateComplete.length && [currentDate compare:today] == NSOrderedSame) {
            return ticket;
        }
    }
    return nil;
}

- (void)removeTicketForID:(NSInteger)ticketId{
    NSMutableArray <PRVTicketSeverModel *> *temp = [[NSMutableArray alloc] init];
    for (PRVTicketSeverModel *ticket in self.info.tickets) {
        if (ticket._id != ticketId) {
            [temp addObject:ticket];
        }
    }
    self.info.tickets = [temp copy];
}

- (void)updateTicket:(PRVTicketSeverModel *)newTicket{
    NSMutableArray <PRVTicketSeverModel *> *temp = [[NSMutableArray alloc] init];
    for (PRVTicketSeverModel *ticket in self.info.tickets) {
        if (ticket._id == newTicket._id) {
            [temp addObject:newTicket];
        } else {
            [temp addObject:ticket];
        }
    }
    self.info.tickets = [temp copy];
}

@end


@implementation PRVInfoServerModel

+ (EKObjectMapping *)objectMapping{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasMany:[PRVTicketSeverModel class] forKeyPath:@"tickets" forProperty:@"tickets"];
        [mapping mapPropertiesFromArray:@[@"accessToken", @"email", @"balance", @"name", @"phone", @"number", @"verify"]];
        [mapping mapKeyPath:@"id" toProperty:@"_id"];
    }];
}

@end

@implementation PRVTicketSeverModel

+ (EKObjectMapping *)objectMapping{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"date", @"dateActive", @"dateComplete", @"dateCancel", @"datePayment", @"free", @"code", @"price", @"failMinutes", @"failPrice", @"isPayment", @"time_from", @"time_to", @"timeActive", @"timeCancel", @"timeComplete", @"timePayment", @"totalPrice", @"wallet", @"workMinutes"]];
        [mapping mapKeyPath:@"id" toProperty:@"_id"];
        [mapping hasOne:[PRVClubServerModel class] forKeyPath:@"club" forProperty:@"club"];
        [mapping hasOne:[PRVCoachServerModel class] forKeyPath:@"coach" forProperty:@"coach"];
        [mapping hasOne:[PRVProgramServerModel class] forKeyPath:@"program" forProperty:@"program"];
    }];
}

- (PRVEnrollTicketType)enrollmentType{
    if (self.free && !self.coach) {
        return PRVEnrollTicketTypeFreeSchedule;
    }
    if (self.free && self.coach) {
        return PRVEnrollTicketTypePersonalCoach;
    }
    return PRVEnrollTicketTypeDefault;
}

- (NSInteger)trainingDurationInSeconds{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"d.MM.y HH:mm"];
    NSString *dateAndTimeString = [NSString stringWithFormat:@"%@ %@",self.date, self.time_to];
    NSString *dateAndTimeStartString = [NSString stringWithFormat:@"%@ %@",self.date, self.time_from];
    NSDate *finishTrainingDate = [formatter dateFromString:dateAndTimeString];
    NSDate *startTrainingDate = [formatter dateFromString:dateAndTimeStartString];
    NSTimeInterval substraction = [finishTrainingDate timeIntervalSinceDate:startTrainingDate];
    NSInteger hours = substraction / 3600;
    NSInteger minutes = ((NSInteger)substraction / 60) % 60;
    NSInteger seconds = (NSInteger)substraction % 60;
    return hours * 3600 + minutes * 60 + seconds;
}

- (NSInteger)secondsToFinishTraining{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"d.MM.y HH:mm"];
    NSString *dateAndTimeString = [NSString stringWithFormat:@"%@ %@", self.date, self.time_to];
    NSDate *finishTrainingDate = [formatter dateFromString:dateAndTimeString];
    NSTimeInterval substraction = [finishTrainingDate timeIntervalSinceDate:[NSDate date]];
    NSInteger hours = substraction / 3600;
    NSInteger minutes = ((NSInteger)substraction / 60) % 60;
    NSInteger seconds = (NSInteger)substraction % 60;
    return hours * 3600 + minutes * 60 + seconds;
}

- (NSInteger)minutesToFinishTraining{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"d.MM.y HH:mm"];
    NSString *dateAndTimeString = [NSString stringWithFormat:@"%@ %@", self.date, self.time_to];
    NSDate *finishTrainingDate = [formatter dateFromString:dateAndTimeString];
    NSTimeInterval substraction = [finishTrainingDate timeIntervalSinceDate:[NSDate date]];
    NSInteger hours = substraction / 3600;
    NSInteger minutes = ((NSInteger)substraction / 60) % 60;
    return minutes + hours * 60;
}

- (NSInteger)minutesToStartTraining{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"d.MM.y HH:mm"];
    NSString *dateAndTimeString = [NSString stringWithFormat:@"%@ %@", self.date, self.time_from];
    NSDate *startTrainingDate = [formatter dateFromString:dateAndTimeString];
    NSTimeInterval substraction = [startTrainingDate timeIntervalSinceDate:[NSDate date]];
    NSInteger hours = substraction / 3600;
    NSInteger minutes = ((NSInteger)substraction / 60) % 60;
    return minutes + hours * 60;
}

- (NSString *)dateToFinishTraining{
    NSInteger secondsToFinish = [self secondsToFinishTraining];
    NSInteger hours = secondsToFinish / 3600;
    NSInteger minutes = (secondsToFinish / 60) % 60;
    NSInteger seconds = secondsToFinish % 60;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    if (hours) {
        [formatter setDateFormat:@"HH:mm:ss"];
        return [NSString stringWithFormat:@"%ld:%ld:%ld", (long)hours, (long)minutes, (long)seconds];
    } else {
        [formatter setDateFormat:@"mm:ss"];
        return [NSString stringWithFormat:@"%ld:%ld", (long)minutes, (long)seconds];
    }
}

- (BOOL)isActive{
    return self.dateActive.length;
}

- (BOOL)isNew{
    return !self.dateActive.length;
}

- (BOOL)isCompleted{
    return self.dateComplete.length;
}

- (BOOL)isCanceled{
    return self.dateCancel.length;
}

- (BOOL)isStarted{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"d.MM.y HH:mm"];
    NSString *dateAndTimeString = [NSString stringWithFormat:@"%@ %@", self.date, self.time_from];
    NSDate *startTrainingDate = [formatter dateFromString:dateAndTimeString];
    return [[NSDate date] compare:startTrainingDate] == NSOrderedDescending;
}

- (BOOL)isEnded{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"d.MM.y HH:mm"];
    NSString *dateAndTimeString = [NSString stringWithFormat:@"%@ %@", self.date, self.time_to];
    NSDate *finishTrainingDate = [formatter dateFromString:dateAndTimeString];
    return [[NSDate date] compare:finishTrainingDate] == NSOrderedDescending;
}

- (BOOL)isFailTimeStarted{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"d.MM.y HH:mm"];
    NSString *dateAndTimeString = [NSString stringWithFormat:@"%@ %@", self.date, self.time_to];
    NSDate *finishTrainingDate = [formatter dateFromString:dateAndTimeString];
    finishTrainingDate = [finishTrainingDate dateByAddingTimeInterval:kMinutesToExit * 60];
    return [[NSDate date] compare:finishTrainingDate] == NSOrderedDescending;
}

- (NSInteger)currentFailMinutes{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"d.MM.y HH:mm"];
    NSString *dateAndTimeString = [NSString stringWithFormat:@"%@ %@", self.date, self.time_to];
    NSDate *finishTrainingDate = [formatter dateFromString:dateAndTimeString];
    finishTrainingDate = [finishTrainingDate dateByAddingTimeInterval:kMinutesToExit * 60];
    NSTimeInterval substraction = [[NSDate date] timeIntervalSinceDate:finishTrainingDate];
    NSInteger hours = substraction / 3600;
    NSInteger minutes = ((NSInteger)substraction / 60) % 60;
    return minutes + hours * 60 + 1;
}

- (NSInteger)currentMinutesToExit{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"d.MM.y HH:mm"];
    NSString *dateAndTimeString = [NSString stringWithFormat:@"%@ %@", self.date, self.time_to];
    NSDate *finishTrainingDate = [formatter dateFromString:dateAndTimeString];
    NSTimeInterval substraction = [finishTrainingDate timeIntervalSinceDate:[NSDate date]];
    NSInteger hours = substraction / 3600;
    NSInteger minutes = ((NSInteger)substraction / 60) % 60;
    return (minutes + hours * 60) + kMinutesToExit;
}

- (NSInteger)totalSecondsToExit{
    return kMinutesToExit * 60;
}

- (NSInteger)currentSecondsToExit{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"d.MM.y HH:mm"];
    NSString *dateAndTimeString = [NSString stringWithFormat:@"%@ %@", self.date, self.time_to];
    NSDate *finishTrainingDate = [formatter dateFromString:dateAndTimeString];
    finishTrainingDate = [finishTrainingDate dateByAddingTimeInterval:kMinutesToExit * 60];
    NSTimeInterval substraction = [finishTrainingDate timeIntervalSinceDate:[NSDate date]];
    return substraction;
}

@end

