//
//  PRVClubDataModel+CoreDataClass.h
//  
//
//  Created by Ruslan on 9/11/17.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface PRVClubDataModel : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "PRVClubDataModel+CoreDataProperties.h"
