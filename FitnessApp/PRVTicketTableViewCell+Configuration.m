//
//  PRVTicketTableViewCell+Configuration.m
//  FitnessApp
//
//  Created by Ruslan on 9/13/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVTicketTableViewCell+Configuration.h"
#import "PRVUserServerModel.h"
#import "PRVClubsServerModel.h"
#import "PRVOptionsServerModel.h"
#import "UIImageView+PRVExtension.h"

@implementation PRVTicketTableViewCell (Configuration)

- (void)configureCellForTicket:(PRVTicketSeverModel *)ticket{
    self.clubLabel.text = ticket.club.name;
    self.addressLabel.text = ticket.club.address;
    self.programLabel.text = ticket.program.name;

    if (ticket.enrollmentType == PRVEnrollTicketTypeDefault) {
        self.programLabel.text = ticket.program.name;
        [self.logoImageView prv_setImageWithURL:ticket.club.logo];
        self.logoImageView.hidden = NO;
    }
    if (ticket.enrollmentType == PRVEnrollTicketTypePersonalCoach) {
        self.programLabel.text = @"ПЕРСОНАЛЬНАЯ ТРЕНИРОВКА";
        self.coachImageView.hidden = NO;
        if (ticket.coach.image.length) {
            [self.coachImageView prv_setImageWithURL:ticket.coach.image];
        }
    }
    if (ticket.enrollmentType == PRVEnrollTicketTypeFreeSchedule) {
        self.programLabel.text = @"СВОБОДНАЯ ЗОНА";
        [self.logoImageView prv_setImageWithURL:ticket.club.logo];
        self.logoImageView.hidden = NO;
    }

    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"d.MM.y";
    BOOL today = [[NSCalendar currentCalendar] isDateInToday:[formatter dateFromString:ticket.date]];
    if (today) {
        self.timeLabel.text = [NSString stringWithFormat:@"Сегодня в %@ до %@", ticket.time_from, ticket.time_to];
    } else {
        self.timeLabel.text = [NSString stringWithFormat:@"%@ в %@ до %@", ticket.date, ticket.time_from, ticket.time_to];
    }
}

@end
