//
//  PRVOnboardVC.m
//  FitnessApp
//
//  Created by Ruslan on 7/15/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVOnboardVC.h"
#import "PRVGeoVC.h"

@interface PRVOnboardVC () <UIScrollViewDelegate>

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttonsArray;

@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@end

@implementation PRVOnboardVC

#pragma mark - Lifecycle

- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    for (UIButton *button in self.buttonsArray) {
        button.layer.borderColor = [UIColor blackColor].CGColor;
        button.layer.borderWidth = 1.f;
    }
}

#pragma mark - Actions

- (IBAction)nextButtonPressed:(id)sender {
    NSInteger page = self.pageControl.currentPage;
    if (page < self.pageControl.numberOfPages - 1) {
        self.pageControl.currentPage = self.pageControl.currentPage + 1;
        CGRect frame = self.scrollView.frame;
        frame.origin.x = frame.size.width * self.pageControl.currentPage;
        frame.origin.y = 0;
        [self.scrollView scrollRectToVisible:frame animated:YES];
    } else {
        PRVGeoVC *vc = [[PRVGeoVC alloc] init];
        [self presentViewController:vc animated:YES completion:nil];
    }
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat pageWidth = self.scrollView.frame.size.width;
    float fractionalPage = self.scrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    self.pageControl.currentPage = page;
}

@end
