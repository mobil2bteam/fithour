//
//  PRVDayCollectionViewCell.m
//  FitnessApp
//
//  Created by Ruslan on 8/24/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVDayCollectionViewCell.h"
#import "UIColor+PRVHexColor.h"

@implementation PRVDayCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.dayLabel.textColor = [UIColor primaryColor];
}

- (void)setSelected:(BOOL)selected{
    [super setSelected:selected];
    if (selected) {
        self.dayLabel.textColor = [UIColor whiteColor];
        self.timeLabel.textColor = [UIColor whiteColor];
    } else {
        self.dayLabel.textColor = [UIColor primaryColor];
        self.timeLabel.textColor = [UIColor secondTextColor];
    }
    [UIView animateWithDuration:0.25 animations:^{
        self.backgroundColor = selected ? [UIColor secondColor] : [UIColor whiteColor];
    }];
}


@end
