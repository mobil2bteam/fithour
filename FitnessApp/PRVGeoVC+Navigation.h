//
//  PRVGeoVC+Navigation.h
//  FitnessApp
//
//  Created by Ruslan on 8/27/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVGeoVC.h"

@interface PRVGeoVC (Navigation)

- (void)showMainViewController;

@end
