//
//  PRVPhoneListAlertVC.h
//  FitnessApp
//
//  Created by Ruslan on 8/23/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//
@class PRVPhoneServerModel;

#import <UIKit/UIKit.h>

@interface PRVPhoneListAlertVC : UIViewController

@property (nonatomic, strong) NSArray <PRVPhoneServerModel *> *phones;
@property (nonatomic, copy) void (^phoneBlock)(NSString *phoneNumber);

@end
