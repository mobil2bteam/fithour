//
//  PRVActiveTicketView.m
//  FitnessApp
//
//  Created by Ruslan on 9/21/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVActiveTicketView.h"
#import "PRVUserServerModel.h"

@interface PRVActiveTicketView()
@property (nonatomic, strong) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (nonatomic, strong) PRVTicketSeverModel *ticket;
@end

@implementation PRVActiveTicketView

#pragma mark - Lifecycle

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        NSString *className = NSStringFromClass([self class]);
        [[NSBundle mainBundle]loadNibNamed:className owner:self options:nil];
        [self addSubview:_contentView];
        _contentView.frame = self.bounds;
    }
    return self;
}

#pragma mark - Methods

- (void)prepareViewForTicket:(PRVTicketSeverModel *)ticket{
    self.ticket = ticket;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"d.MM.y HH:mm"];
    
    // User should be on training, but he is late
    if (![ticket isActive] && [ticket isStarted]) {
        self.contentView.backgroundColor = [UIColor thirdColorDark];
        self.statusLabel.text = [NSString stringWithFormat:@"У вас запись в %@ на %@", ticket.time_from, ticket.date];
        return;
    }

    // User has ticket
    if (![ticket isActive]) {
        self.contentView.backgroundColor = [UIColor secondColorLight];
        self.statusLabel.text = [NSString stringWithFormat:@"У вас запись в %@ на %@", ticket.time_from, ticket.date];
        return;
    }
    
    // User is training now
    if ([ticket isActive]) {
        if ([ticket isFailTimeStarted]) {
            self.contentView.backgroundColor = [UIColor thirdColorDark];
            NSInteger failMinutes = [ticket currentFailMinutes];
            self.statusLabel.text = [NSString stringWithFormat:@"Идут штрафные минуты - %ld мин.", (long)failMinutes];
        } else {
            self.contentView.backgroundColor = [UIColor primaryColor];
            NSInteger minutesToFinish = [self.ticket minutesToFinishTraining];
            if (minutesToFinish > 0) {
                self.statusLabel.text = [NSString stringWithFormat:@"Активная тренировка, еще %ld мин.", (long)minutesToFinish];
            } else {
                self.statusLabel.text = [NSString stringWithFormat:@"У вас 15 мин. на сборы"];
            }
        }
        return;
    }
    
}

#pragma mark - Action

- (IBAction)ticketButtonPressed:(id)sender {
    if (self.ticketBlock) {
        self.ticketBlock(self.ticket);
    }
}

@end
