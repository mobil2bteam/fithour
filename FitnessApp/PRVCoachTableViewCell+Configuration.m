//
//  PRVCoachTableViewCell+Configuration.m
//  FitnessApp
//
//  Created by Ruslan on 9/20/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVCoachTableViewCell+Configuration.h"
#import "UIImageView+PRVExtension.h"
#import "PRVClubsServerModel.h"
#import "PRVCoachImageView.h"

@implementation PRVCoachTableViewCell (Configuration)

- (void)configureCellForCoach:(PRVCoachServerModel *)coach{
    if (coach.image.length) {
        [self.coachImageView prv_setImageWithURL:coach.image];
    }
    self.coachNameLabel.text = coach.name;
    self.rankLabel.text = coach.rank;
    self.groups = coach.groups;
}

@end
