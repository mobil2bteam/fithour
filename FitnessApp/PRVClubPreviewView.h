//
//  PRVClubPreviewView.h
//  FitnessApp
//
//  Created by Ruslan on 8/11/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PRVClubServerModel;
@class PRVClubDataModel;

@interface PRVClubPreviewView : UIView

@property (weak, nonatomic) IBOutlet UILabel *clubNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *clubAddressLabel;
@property (weak, nonatomic) IBOutlet UIImageView *clubLogoImageView;
@property (weak, nonatomic) IBOutlet UILabel *reviewsCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *ratingLabel;
@property (weak, nonatomic) IBOutlet UILabel *workingTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (nonatomic, copy) void(^deleteBlock)();

- (void)setNumberOfReviews:(NSInteger)number;
- (void)configureForClub:(PRVClubServerModel *)club;
- (void)configureForFavoriteClub:(PRVClubDataModel *)favoriteClub;

@end
