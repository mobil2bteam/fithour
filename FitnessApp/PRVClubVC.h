//
//  PRVClubVC.h
//  FitnessApp
//
//  Created by Ruslan on 7/16/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//


#import <UIKit/UIKit.h>

@class PRVClubServerModel;
@class PRVDataSource;
@class PRVSliderView;

@interface PRVClubVC : UIViewController

@property (nonatomic, strong) PRVClubServerModel *club;

@property (nonatomic, strong) PRVDataSource *sliderDataSource;
@property (nonatomic, strong) PRVDataSource *commentsTableViewDataSource;

@property (nonatomic, weak) IBOutlet UITableView *commentsTableView;

- (void)loadClubWithID:(NSInteger)clubId lat:(double)lat lng:(double)lng;

@end
