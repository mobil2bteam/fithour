//
//  PRVFilterVC.m
//  FitnessApp
//
//  Created by Ruslan on 7/16/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

static NSString * const kGroupsCollectionViewKeyPath = @"groupsCollectionView.contentSize";

#import "PRVFilterVC.h"
#import "PRVFilterCollectionViewCell.h"
#import "PRVAppManager.h"
#import "PRVOptionsServerModel.h"
#import "UIImageView+PRVExtension.h"
#import "PRVFilterManager.h"
#import "PRVCityListVC.h"

@interface PRVFilterVC ()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *groupsCollectionViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UISwitch *distanceSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *ratingSwitch;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;

@end

@implementation PRVFilterVC

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self customizeViews];
    [self prepareViews];
    [self setupValues];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    // scroll selected date cell to the center
    if (self.selectedDate) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem: [self.filterManager.getDatesOptions indexOfObject:self.selectedDate] inSection:0];
        [self.datesCollectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
    }
    // scroll selected time cell to the center
    if (self.selectedTime) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem: [self.filterManager.getTimesOptions indexOfObject:self.selectedTime] inSection:0];
        [self.timesCollectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
    }
    // scroll selected price cell to the center
    if (self.selectedPrice) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem: [self.filterManager.getPricesOptions indexOfObject:self.selectedPrice] inSection:0];
        [self.pricesCollectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
    }
}

- (void)dealloc{
    [self removeObserver:self forKeyPath:kGroupsCollectionViewKeyPath];
}

#pragma mark - Observers

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
    if ([keyPath isEqualToString:kGroupsCollectionViewKeyPath]) {
        CGFloat newHeight = self.groupsCollectionView.collectionViewLayout.collectionViewContentSize.height;
        self.groupsCollectionViewHeightConstraint.constant = newHeight;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view layoutIfNeeded];
        });
    }
}

#pragma mark - Methods

- (void)customizeViews{
    [self.distanceSwitch setOnTintColor:[UIColor secondPrimaryColor]];
    [self.ratingSwitch setOnTintColor:[UIColor secondPrimaryColor]];
}

- (void)prepareViews{
    self.cityLabel.text = [PRVAppManager sharedInstance].getUserCity.name;
    [self.distanceSwitch setOn:[self.filterManager getSortMode] != PRVSortModeRating];
    [self.ratingSwitch setOn:[self.filterManager getSortMode] == PRVSortModeRating];
    self.groupsCollectionView.allowsMultipleSelection = YES;
    
    // add back button to rightBarButtonItem to return back without accepting current filters
    UIBarButtonItem *backRightButton = [[UIBarButtonItem alloc] initWithTitle:@"Применить" style:UIBarButtonItemStylePlain target:self action:@selector(backWithoutUpdateButtonPressed:)];
    self.navigationItem.rightBarButtonItem = backRightButton;
    
    // register cells
    NSString *identifier = @"PRVGroupCollectionViewCell";
    [self.groupsCollectionView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellWithReuseIdentifier:identifier];
    for (UICollectionView *collectionView in @[self.datesCollectionView, self.timesCollectionView, self.pricesCollectionView]) {
        [collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([PRVFilterCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([PRVFilterCollectionViewCell class])];
        collectionView.allowsMultipleSelection = NO;
    }

    // add observer to update groupsCollectionView's height
    [self addObserver:self forKeyPath:kGroupsCollectionViewKeyPath options:NSKeyValueObservingOptionNew context:nil];
}

- (void)setupValues{
    self.title = @"Фильтры";
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.filterManager = [PRVFilterManager sharedInstance];
    if (self.filterManager.getSelectedGroups.count) {
        self.selectedGroupsArray = [self.filterManager.getSelectedGroups mutableCopy];
    } else {
        self.selectedGroupsArray = [[NSMutableArray alloc] init];
    }
    self.selectedDate = self.filterManager.getSelectedDate;
    self.selectedTime = self.filterManager.getSelectedTime;
    self.selectedPrice = self.filterManager.getSelectedPrice;
}

#pragma mark - Actions

- (IBAction)changeCityButtonPressed:(id)sender {
    PRVCityListVC *vc = [[PRVCityListVC alloc] prv_initWithNib];
    vc.callback = ^(PRVCityServerModel *selectedCity) {
        [[PRVAppManager sharedInstance] setUserCity:selectedCity];
        _cityLabel.text = selectedCity.name;
    };
    UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:navVC animated:YES completion:nil];
}

- (IBAction)backWithoutUpdateButtonPressed:(id)sender {
    if (self.filterCallback) {
        [self.filterManager setSelectedDate:self.selectedDate];
        [self.filterManager setSelectedTime:self.selectedTime];
        [self.filterManager setSelectedPrice:self.selectedPrice];
        [self.filterManager setSelectedGroups:self.selectedGroupsArray];
        self.filterCallback();
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)distanceSwitchValueChanged:(id)sender {
    [self.ratingSwitch setOn:!self.ratingSwitch.isOn];
    [self.filterManager setSortMode:self.ratingSwitch.isOn ? PRVSortModeRating : PRVSortModeDistance];
}

- (IBAction)ratingSwitchValueChanged:(id)sender {
    [self.distanceSwitch setOn:!self.distanceSwitch.isOn];
    [self.filterManager setSortMode:self.distanceSwitch.isOn ? PRVSortModeDistance : PRVSortModeRating];
}

- (BOOL)hidesBottomBarWhenPushed
{
    return YES;
}

@end
