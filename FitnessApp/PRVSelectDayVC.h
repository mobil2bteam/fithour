//
//  PRVSelectDayVC.h
//  FitnessApp
//
//  Created by Ruslan on 9/12/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PRVClubServerModel;

@interface PRVSelectDayVC : UIViewController

@property (nonatomic, strong) PRVClubServerModel *club;

@end
