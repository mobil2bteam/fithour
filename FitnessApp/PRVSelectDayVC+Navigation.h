//
//  PRVSelectDayVC+Navigation.h
//  FitnessApp
//
//  Created by Ruslan on 9/20/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVSelectDayVC.h"

@interface PRVSelectDayVC (Navigation)

- (void)showSelectTimeViewControllerForDate:(NSDate *)date;

@end
