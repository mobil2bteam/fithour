//
//  PRVCityListVC.m
//  FitnessApp
//
//  Created by Ruslan on 7/28/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVCityListVC.h"
#import "PRVOptionsServerModel.h"
#import "PRVAppManager.h"

@interface PRVCityListVC ()
@property (weak, nonatomic) IBOutlet UITableView *citiesTableView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (nonatomic, strong) NSArray<PRVCityServerModel *> *city_list;
@end

@implementation PRVCityListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationItem.title = @"Города";
    [self.citiesTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    UIBarButtonItem *closeBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Закрыть" style:UIBarButtonItemStylePlain target:self action:@selector(dismissBarButtonPressed:)];
    self.navigationItem.rightBarButtonItem = closeBarButton;
}

- (IBAction)dismissBarButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return self.city_list.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.textLabel.text = self.city_list[indexPath.row].name;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    __weak typeof(self) weakSelf = self;
    [self dismissViewControllerAnimated:YES completion:^{
        if (weakSelf.callback) {
            weakSelf.callback(weakSelf.city_list[indexPath.row]);
        }
    }];
}

#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self.searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [self.searchBar setShowsCancelButton:NO];
    [self.searchBar resignFirstResponder];
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    [self.searchBar setShowsCancelButton:YES];
    return YES;
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar{
    return YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    self.city_list = nil;
    [self.citiesTableView reloadData];
}

#pragma mark - Methods

- (NSArray <PRVCityServerModel *> *)city_list{
    if (_city_list) {
        return _city_list;
    }
    NSArray <PRVCityServerModel *> *temp = [[PRVAppManager sharedInstance] getOptions].city_list;
    if (self.searchBar.text.length) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:
                                  @"(name BEGINSWITH[c] %@)", _searchBar.text];
        temp = [temp filteredArrayUsingPredicate:predicate];
    }
    _city_list = temp;
    return temp;
}

@end
