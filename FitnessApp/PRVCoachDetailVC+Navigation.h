//
//  PRVCoachDetailVC+Navigation.h
//  FitnessApp
//
//  Created by Ruslan on 22.11.2017.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVCoachDetailVC.h"

@class PRVPhoneServerModel;

@interface PRVCoachDetailVC (Navigation)

- (void)showPhoneListViewControllerForPhones:(NSArray <PRVPhoneServerModel *> *)phones phoneBlock:(void (^)(NSString *phoneNumber))phoneBlock;

@end
