//
//  PRVViewController.m
//  FitnessApp
//
//  Created by Ruslan on 7/12/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVViewController.h"
#import "PRVMainVC.h"
#import "PRVAppManager.h"
#import "PRVFavoriteListVC.h"
#import "PRVInfoVC.h"
#import "PRVProfileVC.h"
#import "PRVLogInVC.h"

@interface PRVViewController () <UITabBarControllerDelegate>

@end

@implementation PRVViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.delegate = self;
    [self setupTabBarController];
}

#pragma mark - Methods

- (void)setupTabBarController{
    self.tabBar.tintColor = [UIColor secondPrimaryColor];
    self.tabBar.barTintColor = [UIColor whiteColor];
    self.tabBar.unselectedItemTintColor = [UIColor primaryColor];
    
    PRVMainVC *mainVC = [[PRVMainVC alloc] prv_initWithNib];
    mainVC.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Поиск" image:[UIImage imageNamed:@"bottom_icon_pin"] tag:0];
    
    UINavigationController *navVC = [[UINavigationController alloc]initWithRootViewController:mainVC];
    
    PRVProfileVC *profileVC = [[PRVProfileVC alloc]prv_initWithNib];
    profileVC.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Мой FitHout" image:[UIImage imageNamed:@"bottom_icon_user"] tag:1];

    UINavigationController *navVC2 = [[UINavigationController alloc]initWithRootViewController:profileVC];
    
    PRVFavoriteListVC *favoriteVC = [[PRVFavoriteListVC alloc]prv_initWithNib];
    favoriteVC.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Избранные" image:[UIImage imageNamed:@"bottom_icon_bookmarks"] tag:2];
    UINavigationController *navVC3 = [[UINavigationController alloc]initWithRootViewController:favoriteVC];
    
    PRVInfoVC *infoVC = [[PRVInfoVC alloc]prv_initWithNib];
    infoVC.tabBarItem = [[UITabBarItem alloc]initWithTabBarSystemItem:UITabBarSystemItemRecents tag:3];
    infoVC.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Инфо" image:[UIImage imageNamed:@"bottom_icon_help"] tag:3];

    UINavigationController *navVC4 = [[UINavigationController alloc]initWithRootViewController:infoVC];
    
    self.viewControllers = @[navVC, navVC3, navVC2, navVC4];
}

#pragma mark - UITabBarControllerDelegate

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController{
    // if user choose profile view controller (tag == 1) check if he is logged in, if no show login view controleller
    if (viewController.tabBarItem.tag == 1) {
        if ([[PRVAppManager sharedInstance] getUser] != nil) {
            return YES;
        }
        PRVLogInVC *vc = [[PRVLogInVC alloc] prv_initWithNib];
        UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:vc];
        [self presentViewController:navVC animated:YES completion:nil];
        return NO;
    }
    return YES;
}

@end
