//
//  PRVGeoVC.m
//  FitnessApp
//
//  Created by Ruslan on 7/28/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//
@class PRVCityServerModel;

#import "PRVGeoVC.h"
#import "PRVCityListVC.h"
#import "PRVAppManager.h"
#import "PRVOptionsServerModel.h"
#import "PRVGeoVC+Navigation.h"

@import CoreLocation;

@interface PRVGeoVC ()<CLLocationManagerDelegate>
@property (weak, nonatomic) IBOutlet UIView *cityView;
@property (weak, nonatomic) IBOutlet UIView *permissionView;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *locationIconZeroHeightConstraint;
@property (nonatomic, assign) BOOL shouldStop;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) PRVCityServerModel *selectedCity;
@end

@implementation PRVGeoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self prepareView];
}

- (void)prepareView{
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    switch (status) {
        case kCLAuthorizationStatusNotDetermined:
        {
            self.permissionView.hidden = NO;
        }
            break;
        case kCLAuthorizationStatusDenied:
        {
            self.cityView.hidden = NO;
        }
            break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:
        {
            self.locationManager = [[CLLocationManager alloc] init];
            self.locationManager.delegate = self;
            [self.locationManager startUpdatingLocation];
            [self startAnimation];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations {
    CLLocation *location = [locations lastObject];
    [self getAddressFromLocation:location];
    [self.locationManager stopUpdatingLocation];
    self.locationManager.delegate = nil;
    self.shouldStop = YES;
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    if (status == kCLAuthorizationStatusDenied) {
        self.shouldStop = YES;
        self.cityView.hidden = NO;
    }
}

- (void)getAddressFromLocation:(CLLocation *)location{
    CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
    [geocoder reverseGeocodeLocation:location
                   completionHandler:^(NSArray *array, NSError *error){
                       if (error){
                           self.cityView.hidden = NO;
                           return;
                       }
                       CLPlacemark *placemark = [array objectAtIndex:0];
                       if ([self cityIsAvaliable:placemark.locality]){
                           self.cityLabel.text = self.selectedCity.name;
                       }
                       self.cityView.hidden = NO;
                   }];
}

- (BOOL)cityIsAvaliable:(NSString *)cityName{
    for (PRVCityServerModel *city in [[PRVAppManager sharedInstance] getOptions].city_list) {
        if ([city.name isEqualToString:cityName]){
            self.selectedCity = city;
            return YES;
        }
    }
    return NO;
}


- (PRVCityServerModel *)selectedCity{
    if (_selectedCity) {
        return _selectedCity;
    }
    for (PRVCityServerModel *city in [[PRVAppManager sharedInstance] getOptions].city_list) {
        if ([city.name.lowercaseString containsString:@"москва"]) {
            return city;
        }
    }
    return nil;
}

- (void)startAnimation{
    self.locationIconZeroHeightConstraint.priority = 900;
    [self.view layoutIfNeeded];
    self.locationIconZeroHeightConstraint.priority = 250;
    [UIView animateWithDuration:0.6 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        if (finished && !self.shouldStop) {
            [self startAnimation];
        }
    }];
}

#pragma mark - Actions

- (IBAction)yesButtonPressed:(id)sender {
    [[PRVAppManager sharedInstance] setUserCity:self.selectedCity];

    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:self.selectedCity];
    [userDefaults setObject:encodedObject forKey:@"city"];
    [userDefaults synchronize];
    [self showMainViewController];
}

- (IBAction)anotherCityButtonPressed:(id)sender {
    PRVCityListVC *vc = [[PRVCityListVC alloc] prv_initWithNib];
    vc.callback = ^(PRVCityServerModel *selectedCity) {
        _selectedCity = selectedCity;
        _cityLabel.text = [NSString stringWithFormat:@"%@?", selectedCity.name];
    };
    UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:navVC animated:YES completion:nil];
}

- (IBAction)enableGpsButtonPressed:(id)sender {
    self.permissionView.hidden = YES;
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    [self.locationManager requestWhenInUseAuthorization];
    [self.locationManager startUpdatingLocation];
    [self startAnimation];
}

@end
