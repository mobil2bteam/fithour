//
//  PRVAppManager.h
//  FitnessApp
//
//  Created by Ruslan on 7/20/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

@class PRVOptionsServerModel;
@class PRVCityServerModel;
@class PRVUserServerModel;

#import <Foundation/Foundation.h>

@interface PRVAppManager : NSObject

+ (instancetype)sharedInstance;

#pragma mark - Getters 

- (PRVOptionsServerModel *)getOptions;

- (PRVCityServerModel *)getUserCity;

- (PRVUserServerModel *)getUser;

#pragma mark - Setters

- (void)setOptions:(PRVOptionsServerModel *)options;

- (void)setUserCity:(PRVCityServerModel *)userCity;

- (void)setUser:(PRVUserServerModel *)user;

- (void)removeUser;

@end
