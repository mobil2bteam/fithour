//
//  PRVProgramServerModel+Extension.h
//  FitnessApp
//
//  Created by Ruslan on 8/24/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVOptionsServerModel.h"

@interface PRVProgramServerModel (Extension)

- (NSArray<PRVScheduleServerModel *> *)scheduleForWeek;

- (NSArray <NSArray<PRVScheduleServerModel *> *> *)splitScheduleByWeeks;

@end
