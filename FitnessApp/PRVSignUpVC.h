//
//  PRVSignUpVC.h
//  FitnessApp
//
//  Created by Ruslan on 7/19/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PRVSignUpVC : UIViewController
@property (nonatomic, assign) BOOL sighInForBuyTicket;
@property (nonatomic, copy) void (^loginCallback)(void);
@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *password;
@property (nonatomic, copy) NSString *code;
@end
