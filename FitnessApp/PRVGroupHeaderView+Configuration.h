//
//  PRVGroupHeaderView+Configuration.h
//  FitnessApp
//
//  Created by Ruslan on 8/29/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//
@class PRVGroupServerModel;

#import "PRVGroupHeaderView.h"

@interface PRVGroupHeaderView (Configuration)

- (void)configureHeaderForGroup:(PRVGroupServerModel *)group;

@end
