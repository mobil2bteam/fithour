//
//  PRVFilterView.h
//  FitnessApp
//
//  Created by Ruslan on 7/20/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//
@class PRVPriceServerModel;
@class PRVGroupServerModel;
@class PRVDateModel;

#import <UIKit/UIKit.h>

@protocol PRVFilterViewDelegate <NSObject>
- (void)filterDidUpdate;
- (void)detailFilterDidSelected;
@end


@interface PRVFilterView : UIView

@property (nonatomic, weak) id <PRVFilterViewDelegate> delegate;
@property (nonatomic, assign) BOOL sort;

- (void)updateView;

@end

