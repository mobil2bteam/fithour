//
//  PRVFilterCollectionViewCell.h
//  FitnessApp
//
//  Created by Ruslan on 8/9/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PRVFilterCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@end
