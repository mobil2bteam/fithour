//
//  PRVLogInVC.m
//  FitnessApp
//
//  Created by Ruslan on 7/19/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVLogInVC.h"
#import "PRVSignUpVC.h"
#import "PRVServerManager.h"
#import "UIView+PRVExtensions.h"
#import "PRVRestorePasswordAlertVC.h"
#import "PRVPreSignUpStep1VC.h"

@interface PRVLogInVC ()

@property (weak, nonatomic) IBOutlet UITextField *loginTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *signUpButton;

@end

@implementation PRVLogInVC

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.hideSignUpButton) {
        self.signUpButton.hidden = YES;
    }
    self.navigationItem.title = @"Вход";
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc]initWithTitle:@"Закрыть" style:UIBarButtonItemStylePlain target:self action:@selector(dismissBarButtonPressed:)];
    self.navigationItem.rightBarButtonItem = rightButton;
    
    UIImage *image = [UIImage imageNamed:@"mail"];
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(100, 0, 30, 20)];
    imageView.image = image;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    self.loginTextField.leftViewMode = UITextFieldViewModeAlways;
    self.loginTextField.leftView = imageView;
    
    UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(100, 0, 30, 20)];
    imageView2.contentMode = UIViewContentModeScaleAspectFit;

    imageView2.image = [UIImage imageNamed:@"lock"];
    self.passwordTextField.leftViewMode = UITextFieldViewModeAlways;
    self.passwordTextField.leftView = imageView2;
}

#pragma mark - Actions

- (IBAction)dismissBarButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)restorePasswordButtonPressed:(id)sender {
    PRVRestorePasswordAlertVC *vc = [[PRVRestorePasswordAlertVC alloc] prv_initWithNib];
    vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:vc animated:YES completion:nil];
}

- (IBAction)logInButtonPressed:(id)sender {
    if (!self.loginTextField.text.length) {
        [self.loginTextField prv_shake];
        return;
    }
    if (!self.passwordTextField.text.length) {
        [self.passwordTextField prv_shake];
        return;
    }
    [self prv_addMBProgressHUD];
    NSDictionary *params = @{@"login":self.loginTextField.text,
                             @"password":self.passwordTextField.text};
    __weak typeof(self) weakSelf = self;
    [[PRVServerManager sharedManager] logInWithParams:params onSuccess:^(PRVUserServerModel *user, NSString *error) {
        [self prv_hideMBProgressHUD];
        if (error) {
            [self prv_showMessage:error withTitle:@"Ошибка"];
        } else {
            [self dismissViewControllerAnimated:YES completion:^{
                if (weakSelf.loginCallback) {
                    weakSelf.loginCallback();
                }
            }];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [self prv_showMessage:error.localizedDescription withTitle:@"Ошибка"];
        [self prv_hideMBProgressHUD];
    }];
}

- (IBAction)signUpButtonPressed:(id)sender {
    PRVPreSignUpStep1VC *vc = [[PRVPreSignUpStep1VC alloc] prv_initWithNib];
//    PRVSignUpVC *vc = [[PRVSignUpVC alloc] prv_initWithNib];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == self.loginTextField) {
        [self.passwordTextField becomeFirstResponder];
    }
    [textField resignFirstResponder];
    return YES;
}

@end
