//
//  PRVCancelTicketAlertVC.m
//  FitnessApp
//
//  Created by Ruslan on 11/1/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVCancelTicketAlertVC.h"

@interface PRVCancelTicketAlertVC ()

@end

@implementation PRVCancelTicketAlertVC


- (IBAction)dismissButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)cancelButtonPressed:(id)sender {
    __weak typeof(self) weakSelf = self;
    [self dismissViewControllerAnimated:YES completion:^{
        typeof(self) strongSelf = weakSelf;
        if (strongSelf.completionCallback) {
            strongSelf.completionCallback();
        }
    }];
}

@end
