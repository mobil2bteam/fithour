//
//  PRVCoachesListVC.h
//  FitnessApp
//
//  Created by Ruslan on 7/27/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//
@class PRVCoachServerModel;
@class PRVClubServerModel;

#import <UIKit/UIKit.h>

@interface PRVCoachesListVC : UIViewController
@property (nonatomic, assign) BOOL isPersonalTraining;
@property (nonatomic, strong) PRVClubServerModel *club;
@end
