//
//  PRVHoursCollectionViewCell.h
//  FitnessApp
//
//  Created by Ruslan on 10/27/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PRVHoursCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *hoursLabel;
@end
