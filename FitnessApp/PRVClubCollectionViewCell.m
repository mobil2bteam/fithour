//
//  PRVClubCollectionViewCell.m
//  FitnessApp
//
//  Created by Ruslan on 7/16/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVClubCollectionViewCell.h"
#import "PRVClubPreviewView.h"

@implementation PRVClubCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.layer.cornerRadius = 4;
    self.layer.borderWidth = 1.f;
    self.layer.borderColor = [UIColor colorWithWhite:0.914 alpha:1.0].CGColor;
}

- (void)prepareForReuse{
    self.clubPreviewView.clubNameLabel.text = @"";
    self.clubPreviewView.clubAddressLabel.text = @"";
    self.clubPreviewView.ratingLabel.text = @"";
    self.clubPreviewView.reviewsCountLabel.text = @"Нет отзывов";
    self.clubPreviewView.distanceLabel.text = @"";
    self.clubPreviewView.clubLogoImageView.image = [UIImage imageNamed:@"club_logo_stock"];
}

@end
