//
//  PRVSelectDayVC.m
//  FitnessApp
//
//  Created by Ruslan on 9/12/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVSelectDayVC.h"
#import "PRVSelectDayVC+Navigation.h"
#import <FSCalendar.h>
#import <FSCalendar.h>
#import "PRVClubsServerModel.h"
#import "PRVOptionsServerModel.h"

typedef NS_ENUM(NSUInteger, PRVEnrollButtonState) {
    PRVEnrollButtonStateNormal,
    PRVEnrollButtonStateNotWorking,
    PRVEnrollButtonStateWeekend,
};

@interface PRVSelectDayVC () <FSCalendarDelegate, FSCalendarDataSource, FSCalendarDelegateAppearance>
@property (nonatomic, weak) IBOutlet UIButton *enrollButton;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *calendarViewHeightConstraint;
@property (nonatomic, weak) IBOutlet FSCalendar *calendar;
@property (nonatomic, strong) NSDate *selectedDate;
@end

@implementation PRVSelectDayVC

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Дата";
    // Set today as selected date by default
    self.selectedDate = [NSDate date];
    [self prepareCalendarView];
}

- (BOOL)hidesBottomBarWhenPushed
{
    return YES;
}

#pragma mark - Actions

- (IBAction)enrollButtonPressed:(id)sender {
    [self showSelectTimeViewControllerForDate:self.selectedDate];
}

#pragma mark - Setters

- (void)setSelectedDate:(NSDate *)selectedDate{
    _selectedDate = selectedDate;
    [self configureEnrollButtonForDate:selectedDate];
}

#pragma mark - FSCalendar

- (void)calendar:(FSCalendar *)calendar boundingRectWillChange:(CGRect)bounds animated:(BOOL)animated
{
    self.calendarViewHeightConstraint.constant = CGRectGetHeight(bounds);
    [self.view layoutIfNeeded];
}

- (NSDate *)minimumDateForCalendar:(FSCalendar *)calendar{
    return [NSDate date];
}

- (NSDate *)maximumDateForCalendar:(FSCalendar *)calendar{
    return [[NSDate date] dateByAddingTimeInterval: 30 * 24 * 60 *60];
}

- (UIColor *)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance fillSelectionColorForDate:(NSDate *)date{
    return [UIColor secondPrimaryColor];
}

- (UIColor *)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance titleDefaultColorForDate:(NSDate *)date{
    // If current date is out of range (By default, a range is 1 month)
    if ([date compare:calendar.minimumDate] == NSOrderedAscending || [date compare:calendar.maximumDate] == NSOrderedDescending) {
        return [UIColor lightGrayColor];
    }
    // If club is not working at current date
    PRVWorkingHourServerModel *workingHours = [self.club workingHoursForDate:date];
    if (workingHours) {
        if (workingHours.free_day) {
            return [UIColor redColor];
        }
    }
    return [UIColor blackColor];
}

- (void)calendar:(FSCalendar *)calendar didSelectDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition{
    self.selectedDate = date;
}

#pragma mark - Methods

- (void)prepareCalendarView{
    // Set Monday as first day of a week
    self.calendar.firstWeekday = 2;
    self.calendar.today = nil;
    self.calendar.appearance.weekdayTextColor = [UIColor blackColor];
    self.calendar.appearance.headerTitleColor = [UIColor blackColor];
    [self.calendar selectDate:[NSDate date]];
}

- (void)configureEnrollButtonForDate:(NSDate *)date{
    PRVWorkingHourServerModel *workingHours = [self.club workingHoursForDate:date];
    if (workingHours) {
        if (workingHours.free_day) {
            [self configureEnrollButtonForState:PRVEnrollButtonStateWeekend];
        } else {
            BOOL today = [[NSCalendar currentCalendar] isDateInToday:date];
            if (today) {
                // Check working hours for today
                NSDateFormatter *hourFormatter = [[NSDateFormatter alloc] init];
                [hourFormatter setDateFormat:@"HH:mm"];
                NSDate *currentHour = [hourFormatter dateFromString:[hourFormatter stringFromDate:[NSDate date]]];
                NSDate *workingHour = [hourFormatter dateFromString:workingHours.hours_finish];
                if ([currentHour compare:workingHour] == NSOrderedAscending) {
                    [self configureEnrollButtonForState:PRVEnrollButtonStateNormal];
                } else {
                    [self configureEnrollButtonForState:PRVEnrollButtonStateNotWorking];
                }
            } else {
                [self configureEnrollButtonForState:PRVEnrollButtonStateNormal];
            }
        }
    } else {
        [self configureEnrollButtonForState:PRVEnrollButtonStateNormal];
    }
}

- (void)configureEnrollButtonForState:(PRVEnrollButtonState)state{
    switch (state) {
        case PRVEnrollButtonStateWeekend:
        {
            self.enrollButton.userInteractionEnabled = NO;
            self.enrollButton.backgroundColor = [UIColor secondTextColor];
            [self.enrollButton setTitle:@"ВЫХОДНОЙ" forState:UIControlStateNormal];
        }
            break;
        case PRVEnrollButtonStateNotWorking:
        {
            self.enrollButton.userInteractionEnabled = NO;
            self.enrollButton.backgroundColor = [UIColor secondTextColor];
            [self.enrollButton setTitle:@"НЕ РАБОТАЕТ" forState:UIControlStateNormal];
        }
            break;

        default:
        {
            [self.enrollButton setTitle:@"ДАЛЕЕ" forState:UIControlStateNormal];
            self.enrollButton.backgroundColor = [UIColor secondColor];
            self.enrollButton.userInteractionEnabled = YES;
        }
            break;
    }
}

@end
