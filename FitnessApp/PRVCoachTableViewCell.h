//
//  PRVCoachTableViewCell.h
//  FitnessApp
//
//  Created by Ruslan on 7/27/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//
@class PRVGroupServerModel;
@class PRVCoachImageView;

#import <UIKit/UIKit.h>

@interface PRVCoachTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet PRVCoachImageView *coachImageView;
@property (weak, nonatomic) IBOutlet UILabel *coachNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *rankLabel;
@property (nonatomic, strong) NSArray<PRVGroupServerModel *> *groups;
@property (weak, nonatomic) IBOutlet UICollectionView *groupsCollectionView;

@end
