//
//  PRVClubCollectionViewCell.h
//  FitnessApp
//
//  Created by Ruslan on 7/16/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//
@class PRVClubPreviewView;

#import <UIKit/UIKit.h>

@interface PRVClubCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet PRVClubPreviewView *clubPreviewView;

@end
