//
//  PRVProgramDetailVC.m
//  FitnessApp
//
//  Created by Ruslan on 7/29/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVProgramDetailVC.h"
#import "PRVProgramDetailVC+Setup.h"
#import "PRVProgramDetailVC+Navigation.h"
#import "UIImageView+PRVExtension.h"
#import "PRVOptionsServerModel.h"
#import "PRVProgramServerModel+Extension.h"
#import "PRVDataSource.h"
#import "PRVClubsServerModel.h"
#import <NSAttributedString-DDHTML/NSAttributedString+DDHTML.h>

@interface PRVProgramDetailVC () <UICollectionViewDelegate>
@property (nonatomic, weak) IBOutlet UIImageView *programImageView;
@property (nonatomic, weak) IBOutlet UILabel *programNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *priceLabel;
@end

@implementation PRVProgramDetailVC

@synthesize selectedDay = _selectedDay;

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupScheduleCollectionView];
    [self setupDaysCollectionView];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationItem.title = self.program.name;
    self.programNameLabel.text = self.program.name;
    self.priceLabel.text = [NSString stringWithFormat:@"%ld.0 руб.", (long)[self.program price]];
    if (self.program.images.count) {
        [self.programImageView prv_setImageWithURL:self.program.images[0]];
    }
}

- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    CGFloat width = CGRectGetWidth(self.scheduleCollectionView.frame);
    CGSize itemSize = CGSizeMake(width, 80);
    ((UICollectionViewFlowLayout *)self.scheduleCollectionView.collectionViewLayout).itemSize = itemSize;
}

- (IBAction)infoButtonPressed:(id)sender {
    if (self.program._description.length) {
        NSString *text = [self convertHTML:self.program._description];
        [self prv_showMessage:text withTitle:@"Описание"];
    }
}

- (NSString *)convertHTML:(NSString *)html {
    
    NSScanner *myScanner;
    NSString *text = nil;
    myScanner = [NSScanner scannerWithString:html];
    
    while ([myScanner isAtEnd] == NO) {
        
        [myScanner scanUpToString:@"<" intoString:NULL] ;
        
        [myScanner scanUpToString:@">" intoString:&text] ;
        
        html = [html stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>", text] withString:@""];
    }
    html = [html stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    return html;
}

#pragma mark - Getters/Setters

- (PRVScheduleServerModel *)selectedDay{
    if (_selectedDay) {
        return _selectedDay;
    }
    if ([self.program splitScheduleByWeeks].count) {
        if ([self.program splitScheduleByWeeks][0].count) {
            return [self.program splitScheduleByWeeks][0][0]; // first day of first week
        }
    }
    return nil;
}

- (void)setSelectedDay:(PRVScheduleServerModel *)selectedDay{
    _selectedDay = selectedDay;
    [self.scheduleCollectionViewDataSource setItems:self.selectedDay.hours];
    [self.scheduleCollectionView reloadData];
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView == self.daysCollectionView) {
        [collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        PRVScheduleServerModel *schedule = [self.program splitScheduleByWeeks][indexPath.section][indexPath.row];
        self.selectedDay = schedule;;
    }
    if (collectionView == self.scheduleCollectionView) {
        [self showProgramConfirmViewControllerWithHour:[self.scheduleCollectionViewDataSource itemAtIndexPath:indexPath]];
    }
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    cell.backgroundColor = indexPath.section % 2 == 0 ? [UIColor whiteColor] : [UIColor backgroundColor];
}

@end
