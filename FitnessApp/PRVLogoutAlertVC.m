//
//  PRVLogoutAlertVC.m
//  FitnessApp
//
//  Created by Ruslan on 9/15/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVLogoutAlertVC.h"
#import "PRVAppManager.h"

@interface PRVLogoutAlertVC ()

@end

@implementation PRVLogoutAlertVC

#pragma mark - Actions

- (IBAction)cancelButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)logoutButtonPressed:(id)sender {
    [[PRVAppManager sharedInstance] removeUser];
    __weak typeof(self) weakSelf = self;
    [self dismissViewControllerAnimated:YES completion:^{
        typeof(self) strongSelf = weakSelf;
        if (strongSelf.logoutCallback) {
            strongSelf.logoutCallback();
        }
    }];
}

@end
