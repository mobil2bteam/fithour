//
//  PRVPhoneTableViewCell.m
//  FitnessApp
//
//  Created by Ruslan on 8/23/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVPhoneTableViewCell.h"

@implementation PRVPhoneTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)prepareForReuse{
    [super prepareForReuse];
    self.nameLabel.text = @"";
    self.phoneLabel.text = @"";
}

@end
