//
//  PRVCoachDetailVC.h
//  FitnessApp
//
//  Created by Ruslan on 7/27/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class PRVCoachServerModel;
@class PRVClubServerModel;

@interface PRVCoachDetailVC : UIViewController
@property (nonatomic, strong) PRVCoachServerModel *coach;
@property (nonatomic, strong) PRVClubServerModel *club;
@end
