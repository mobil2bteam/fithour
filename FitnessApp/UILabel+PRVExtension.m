//
//  UILabel+PRVExtension.m
//  FitnessApp
//
//  Created by Ruslan on 7/29/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "UILabel+PRVExtension.h"
#import "UIColor+PRVHexColor.h"

@implementation UILabel (PRVExtension)

- (void)setHTML:(NSString *)htmlString{
    
    NSString *htmlString2 = [htmlString stringByAppendingString:[NSString stringWithFormat:@"<style>body{font-family: '%@'; color:%@; font-size:%fpx;}</style>", self.font.fontName, [self.textColor prv_hex], self.font.pointSize]];
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc]
                                                   initWithData: [htmlString2 dataUsingEncoding:NSUnicodeStringEncoding]
                                                   options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                   documentAttributes: nil
                                                   error: nil
                                                   ];
    self.attributedText = attributedString;
}

@end
