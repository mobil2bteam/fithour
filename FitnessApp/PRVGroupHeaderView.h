//
//  PRVGroupHeaderView.h
//  FitnessApp
//
//  Created by Ruslan on 8/29/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PRVGroupHeaderView : UITableViewHeaderFooterView
@property (weak, nonatomic) IBOutlet UIImageView *groupImageView;
@property (weak, nonatomic) IBOutlet UILabel *groupLabel;
@end
