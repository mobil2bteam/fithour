//
//  PRVClubVC+Setup.h
//  FitnessApp
//
//  Created by Ruslan on 8/28/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVClubVC.h"

@interface PRVClubVC (Setup)

- (void)setupCommentsTableView;

@end
