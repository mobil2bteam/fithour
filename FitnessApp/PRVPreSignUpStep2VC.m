#import "PRVPreSignUpStep2VC.h"
#import "PRVServerManager.h"
#import "UIView+PRVExtensions.h"
#import "AFNetworking.h"
#import "AFHTTPSessionOperation.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "PRVSignUpVC.h"

@interface PRVPreSignUpStep2VC ()
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *codeTextField;
@property (weak, nonatomic) IBOutlet UIButton *retryButton;
@end

@implementation PRVPreSignUpStep2VC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.emailTextField.text = self.email;
    self.passwordTextField.text = self.password;
    self.retryButton.layer.borderColor = [UIColor primaryColor].CGColor;
    self.retryButton.layer.borderWidth = 2.0f;
}

- (IBAction)nextButtonPressed:(id)sender {
    if (!self.emailTextField.text.length) {
        [self.emailTextField prv_shake];
        return;
    }
    if (!self.passwordTextField.text.length) {
        [self.passwordTextField prv_shake];
        return;
    }
    if (!self.codeTextField.text.length) {
        [self.codeTextField prv_shake];
        return;
    }
    [self signUp];
}

- (IBAction)retryButtonPressed:(id)sender {
    if (!self.emailTextField.text.length) {
        [self.emailTextField prv_shake];
        return;
    }
    if (!self.passwordTextField.text.length) {
        [self.passwordTextField prv_shake];
        return;
    }
    [self prv_addMBProgressHUD];
    __weak typeof(self) weakSelf = self;
    NSDictionary *params = @{@"email":self.emailTextField.text,
                             @"password": self.passwordTextField.text,
                             @"login":self.emailTextField.text};
    [[PRVServerManager sharedManager] preSignUpWithParams:params onSuccess:^{
        typeof(self) strongSelf = weakSelf;
        [strongSelf prv_hideMBProgressHUD];
        [strongSelf prv_showMessage:@"Код выслан вам повторно" withTitle:@"Сообщение"];
    } onFailure:^(NSString *error) {
        typeof(self) strongSelf = weakSelf;
        [strongSelf prv_showMessage:error withTitle:@"Ошибка"];
        [strongSelf prv_hideMBProgressHUD];
    }];
}

- (void)signUp {
    [self prv_addMBProgressHUD];
    __weak typeof(self) weakSelf = self;
    NSDictionary *params = @{@"email":self.emailTextField.text,
                             @"password": self.passwordTextField.text,
                             @"verify-code": self.codeTextField.text,
                             @"login":self.emailTextField.text};
    [[PRVServerManager sharedManager] preSignUpWithParams:params onSuccess:^{
        typeof(self) strongSelf = weakSelf;
        [strongSelf prv_hideMBProgressHUD];
        [strongSelf showSignUp];
    } onFailure:^(NSString *error) {
        typeof(self) strongSelf = weakSelf;
        [strongSelf prv_showMessage:error withTitle:@"Ошибка"];
        [strongSelf prv_hideMBProgressHUD];
    }];
}

- (void)showSignUp {
    PRVSignUpVC *vc = [[PRVSignUpVC alloc] prv_initWithNib];
    vc.loginCallback = self.loginCallback;
    vc.email = self.emailTextField.text;
    vc.password = self.passwordTextField.text;
    vc.code = self.codeTextField.text;
    vc.sighInForBuyTicket = self.sighInForBuyTicket;
    [self.navigationController pushViewController:vc animated:true];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.emailTextField) {
        [self.passwordTextField becomeFirstResponder];
    }
    if (textField == self.passwordTextField) {
        [self.codeTextField becomeFirstResponder];
    }
    [textField resignFirstResponder];
    return  YES;
}

@end
