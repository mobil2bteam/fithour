//
//  PRVActivateTicketVC.m
//  FitnessApp
//
//  Created by Ruslan on 8/20/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVActivateTicketVC.h"
#import "PRVUserServerModel.h"
#import "PRVOptionsServerModel.h"
#import "PRVClubsServerModel.h"

@interface PRVActivateTicketVC ()

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *programLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UIImageView *qrImageView;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *numberLabel;

@end

@implementation PRVActivateTicketVC

#pragma mark - Actions

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Активировать билет";
    if ([self.ticket isActive]) {
        self.statusLabel.text = @"Выход";
    }
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.numberLabel.text = [NSString stringWithFormat:@"Билет №%ld", (long)self.ticket.code];
    self.addressLabel.text = self.ticket.club.address;
    self.timeLabel.text = [NSString stringWithFormat:@"%@ в %@ до %@", self.ticket.date, self.ticket.time_from, self.ticket.time_to];
    if (![self.ticket isPayment]) {
        self.priceLabel.text = [NSString stringWithFormat:@"%ld руб.", (long)self.ticket.price];
    }
    self.programLabel.text = self.ticket.program.name;
    [self generateQRCode];
}

#pragma mark - Methods

- (void)generateQRCode{
    NSString *qrString = self.ticket.code;
    NSData *stringData = [qrString dataUsingEncoding: NSUTF8StringEncoding];
    
    CIFilter *qrFilter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    [qrFilter setValue:stringData forKey:@"inputMessage"];
    [qrFilter setValue:@"H" forKey:@"inputCorrectionLevel"];
    
    CIImage *qrImage = qrFilter.outputImage;
    float scaleX = self.qrImageView.frame.size.width / qrImage.extent.size.width;
    float scaleY = self.qrImageView.frame.size.height / qrImage.extent.size.height;
    
    qrImage = [qrImage imageByApplyingTransform:CGAffineTransformMakeScale(scaleX, scaleY)];
    
    self.qrImageView.image = [UIImage imageWithCIImage:qrImage
                                                 scale:[UIScreen mainScreen].scale
                                           orientation:UIImageOrientationUp];
}

@end
