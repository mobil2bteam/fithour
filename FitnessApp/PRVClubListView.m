//
//  PRVClubListView.m
//  FitnessApp
//
//  Created by Ruslan on 7/25/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVClubListView.h"
#import "SVPullToRefresh.h"
#import "PRVDataSource.h"

@interface PRVClubListView()

@property (nonatomic, strong) IBOutlet UIView *contentView;
@property (nonatomic, weak) IBOutlet UICollectionView *clubsCollectionView;

@end

@implementation PRVClubListView

#pragma mark - Lifecycle

- (void)awakeFromNib{
    [super awakeFromNib];
    [self setup];
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        NSString *className = NSStringFromClass([self class]);
        [[NSBundle mainBundle] loadNibNamed:className owner:self options:nil];
        [self addSubview:_contentView];
        _contentView.frame = self.bounds;
        [self setup];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        NSString *className = NSStringFromClass([self class]);
        [[NSBundle mainBundle] loadNibNamed:className owner:self options:nil];
        [self addSubview:_contentView];
        _contentView.frame = self.bounds;
        [self setup];
    }
    return self;
}

- (void)setDataSource:(PRVDataSource *)dataSource{
    _clubsCollectionView.dataSource = dataSource;
    [_clubsCollectionView reloadData];
}

- (void)setup{
    // register cell
    NSString *identifier = NSStringFromClass([PRVClubCollectionViewCell class]);
    [_clubsCollectionView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellWithReuseIdentifier:identifier];
    
    // add refresh control to bottom to load more
    __weak typeof(self) weakSelf = self;
    [_clubsCollectionView addPullToRefreshWithActionHandler:^{
        typeof(self) strongSelf = weakSelf;
        if ([_delegate respondsToSelector:@selector(loadMore:)]) {
            [_delegate loadMore:strongSelf];
        }
    } position:SVPullToRefreshPositionBottom];
    
    // customize refresh control
    [_clubsCollectionView.pullToRefreshView setTitle:@"Отпустите чтобы обновить " forState:SVPullToRefreshStateTriggered];
    [_clubsCollectionView.pullToRefreshView setTitle:@"Потяните чтобы обновить " forState:SVPullToRefreshStateStopped];
    [_clubsCollectionView.pullToRefreshView setTitle:@"Идет загрузка" forState:SVPullToRefreshStateLoading];
    [_clubsCollectionView.pullToRefreshView setArrowColor:[UIColor secondPrimaryColor]];
}

#pragma mark - Methods

- (void)clear{
    [_clubsCollectionView reloadData];
}

- (void)reloadData{
    [_clubsCollectionView.pullToRefreshView stopAnimating];
    [_clubsCollectionView reloadData];
}

- (void)setLoadMoreEnabled:(BOOL)enabled{
    if (_clubsCollectionView.showsPullToRefresh == enabled) {
        return;
    }
    _clubsCollectionView.showsPullToRefresh = enabled;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (_delegate) {
        [_delegate clubListView:self didSelectClubAtIndex:indexPath.item];
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(CGRectGetWidth(self.bounds) - 20, 150);
}

@end
