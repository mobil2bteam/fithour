//
//  PRVClubVC+Setup.m
//  FitnessApp
//
//  Created by Ruslan on 8/28/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVClubVC+Setup.h"
#import "PRVClubVC.h"
#import "PRVDataSource.h"
#import "PRVCommentTableViewCell+Configuration.h"
#import "PRVDirectionCollectionViewCell.h"
#import "UIImageView+PRVExtension.h"
#import "PRVClubsServerModel.h"

static NSString * const kImageCollectionViewCellIdentifier = @"PRVImageCollectionViewCell";
static NSString * const kCommentTableViewCellIdentifier = @"PRVCommentTableViewCell";

@implementation PRVClubVC (Setup)

- (void)setupCommentsTableView{
    PRVCellConfigureBlock configureCell = ^(PRVCommentTableViewCell *cell, PRVReviewServerModel *review) {
        [cell configureCellForReview:review];
    };
    self.commentsTableView.rowHeight = UITableViewAutomaticDimension;
    self.commentsTableView.estimatedRowHeight = 89;
    self.commentsTableViewDataSource = [[PRVDataSource alloc] initWithItems:[self reviews] cellIdentifier:kCommentTableViewCellIdentifier configureCellBlock:configureCell];
    self.commentsTableView.dataSource = self.commentsTableViewDataSource;
    [self.commentsTableView registerNib:[PRVCommentTableViewCell prv_nib] forCellReuseIdentifier:kCommentTableViewCellIdentifier];
    [self.commentsTableView addObserver:self forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew context:NULL];
}

- (NSArray<PRVReviewServerModel *> *)reviews{
    if (self.club.user_review) {
        NSMutableArray *temp = [self.club.reviews mutableCopy];
        [temp insertObject:self.club.user_review atIndex:0];
        return [temp copy];
    } else {
        return self.club.reviews;
    }
}

@end
