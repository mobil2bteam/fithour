//
//  PRVEnrollFinishVC.h
//  FitnessApp
//
//  Created by Ruslan on 8/18/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//
@class PRVTicketSeverModel;

#import <UIKit/UIKit.h>

@interface PRVEnrollFinishVC : UIViewController

@property (nonatomic, strong) PRVTicketSeverModel *ticket;
@property (nonatomic, assign) BOOL isPersonal;

@end
