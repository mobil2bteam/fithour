//
//  PRVProgramDetailVC+Navigation.h
//  FitnessApp
//
//  Created by Ruslan on 8/28/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//
@class PRVHourServerModel;

#import "PRVProgramDetailVC.h"

@interface PRVProgramDetailVC (Navigation)

- (void)showProgramConfirmViewControllerWithHour:(PRVHourServerModel *)hour;

@end
