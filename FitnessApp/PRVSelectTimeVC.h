//
//  PRVSelectTimeVC.h
//  FitnessApp
//
//  Created by Ruslan on 9/13/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PRVClubServerModel;

@interface PRVSelectTimeVC : UIViewController

@property (nonatomic, strong) NSDate *selectedDate;
@property (nonatomic, strong) PRVClubServerModel *club;

@end
