//
//  PRVCoachesListVC.m
//  FitnessApp
//
//  Created by Ruslan on 7/27/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVCoachesListVC.h"
#import "PRVCoachTableViewCell+Configuration.h"
#import "PRVClubsServerModel.h"
#import "PRVCoachDetailVC.h"
#import "PRVEnrollCoachVC.h"
#import "PRVDataSource.h"

static NSString * const kCoachTableViewCellIdentifier = @"PRVCoachTableViewCell";

@interface PRVCoachesListVC ()
@property (nonatomic, weak) IBOutlet UITableView *coachesTableView;
@property (nonatomic, strong) PRVDataSource *coachesTableViewDataSource;
@end

@implementation PRVCoachesListVC

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Тренеры";
    [self setupCoachesTableView];
}

#pragma mark - Methods

- (BOOL)hidesBottomBarWhenPushed
{
    return YES;
}

- (void)setupCoachesTableView{
    PRVCellConfigureBlock configureCell = ^(PRVCoachTableViewCell *cell, PRVCoachServerModel *coach) {
        [cell configureCellForCoach:coach];
    };
    self.coachesTableView.rowHeight = UITableViewAutomaticDimension;
    self.coachesTableView.estimatedRowHeight = 150.f;
    self.coachesTableViewDataSource = [[PRVDataSource alloc] initWithItems:self.club.coaches  cellIdentifier:kCoachTableViewCellIdentifier  configureCellBlock:configureCell];
    self.coachesTableView.dataSource = self.coachesTableViewDataSource;
    [self.coachesTableView registerNib:[PRVCoachTableViewCell prv_nib] forCellReuseIdentifier:kCoachTableViewCellIdentifier];
    self.coachesTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    PRVCoachServerModel *coach = [self.coachesTableViewDataSource itemAtIndexPath:indexPath];
    if (self.isPersonalTraining) {
        PRVEnrollCoachVC *vc = [[PRVEnrollCoachVC alloc] prv_initWithNib];
        vc.coach = coach;
        vc.club = self.club;
        [self.navigationController pushViewController:vc animated:YES];
    } else {
        PRVCoachDetailVC *vc = [[PRVCoachDetailVC alloc] prv_initWithNib];
        vc.coach = coach;
        vc.club = self.club;
        [self.navigationController pushViewController:vc animated:YES];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

@end
