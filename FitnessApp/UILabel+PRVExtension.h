//
//  UILabel+PRVExtension.h
//  FitnessApp
//
//  Created by Ruslan on 7/29/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (PRVExtension)

- (void)setHTML:(NSString *)htmlString;

@end
