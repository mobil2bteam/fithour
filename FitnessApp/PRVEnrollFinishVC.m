//
//  PRVEnrollFinishVC.m
//  FitnessApp
//
//  Created by Ruslan on 8/18/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVEnrollFinishVC.h"
#import "PRVUserServerModel.h"
#import "PRVTicketAddressVC.h"
#import "PRVActivateTicketVC.h"
#import "PRVOptionsServerModel.h"
#import "PRVClubsServerModel.h"
#import "PRVNotificationManager.h"
#import "PRVAppManager.h"

@interface PRVEnrollFinishVC ()
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *programLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@end

@implementation PRVEnrollFinishVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [[[PRVAppManager sharedInstance] getUser] addTicketToUser:self.ticket];
    [PRVNotificationManager addNotificationForTicket:self.ticket];
    if (self.isPersonal) {
        self.programLabel.text = [NSString stringWithFormat:@"ПЕРСОНАЛЬНАЯ ТРЕНИРОВКА"];
    } else {
        if (self.ticket.free) {
            self.programLabel.text = [NSString stringWithFormat:@"Свободная \n зона"];
        } else {
            self.programLabel.text = self.ticket.program.name;
        }
    }
    self.addressLabel.text = self.ticket.club.address;
    self.dateLabel.text = [NSString stringWithFormat:@"%@ %@", self.ticket.date, self.ticket.time_from];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden = NO;
}

#pragma mark - Actions

- (IBAction)okButtonPressed:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
