//
//  PRVSignUpVC+Navigation.h
//  FitnessApp
//
//  Created by Ruslan on 9/5/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVSignUpVC.h"

@interface PRVSignUpVC (Navigation)

- (void)showLoginViewController;

@end
