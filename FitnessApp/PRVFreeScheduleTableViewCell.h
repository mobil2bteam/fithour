//
//  PRVFreeScheduleTableViewCell.h
//  FitnessApp
//
//  Created by Ruslan on 10/27/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PRVScheduleServerModel;
@class PRVHourServerModel;

@interface PRVFreeScheduleTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *dayLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *hoursCollectionView;
@property (nonatomic, copy) void (^completionCallback)(PRVScheduleServerModel *, PRVHourServerModel *);

- (void)configureCellForSchedule:(PRVScheduleServerModel *)schedule;

@end
