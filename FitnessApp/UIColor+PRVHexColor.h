//
//  UIColor+PRVHexColor.h
//  FitnessApp
//
//  Created by Ruslan on 7/24/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (PRVHexColor)

+ (UIColor *)prv_colorWithHex:(NSString *)hex;

- (NSString *)prv_hex;

@end
