//
//  PRVFilterCollectionViewCell.m
//  FitnessApp
//
//  Created by Ruslan on 8/9/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVFilterCollectionViewCell.h"

@implementation PRVFilterCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.nameLabel.textColor = [UIColor secondColorDark];
    self.contentView.backgroundColor = [UIColor colorWithRed:0.973 green:0.957 blue:1.0 alpha:1.0];
    self.layer.borderColor = [UIColor secondColorLight].CGColor;
    self.layer.borderWidth = 0.5f;
    self.layer.cornerRadius = 25.f;
}

- (void)setSelected:(BOOL)selected{
    [super setSelected:selected];
    if (selected) {
        self.contentView.backgroundColor = [UIColor secondColor];
        self.nameLabel.textColor = [UIColor whiteColor];
    } else {
        self.contentView.backgroundColor = [UIColor colorWithRed:0.973 green:0.957 blue:1.0 alpha:1.0];
        self.nameLabel.textColor = [UIColor secondColorDark];
    }
}

- (void)prepareForReuse{
    self.nameLabel.text = @"";
}

@end
