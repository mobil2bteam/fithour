//
//  UICollectionViewCell+PRVNib.m
//  FitnessApp
//
//  Created by Ruslan on 8/25/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "UICollectionViewCell+PRVNib.h"

@implementation UICollectionViewCell (PRVNib)

+ (UINib *)prv_nib
{
    return [UINib nibWithNibName:NSStringFromClass([self class]) bundle:nil];
}

@end
