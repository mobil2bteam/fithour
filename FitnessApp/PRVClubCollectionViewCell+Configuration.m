//
//  PRVClubCollectionViewCell+Configuration.m
//  FitnessApp
//
//  Created by Ruslan on 8/24/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVClubCollectionViewCell+Configuration.h"
#import "PRVClubsServerModel.h"
#import "PRVClubPreviewView.h"

@implementation PRVClubCollectionViewCell (Configuration)

- (void)configureCellForClub:(PRVClubServerModel *)club{
    [self.clubPreviewView configureForClub:club];
    [self.clubPreviewView setNumberOfReviews:club.count_reviews];
}

- (void)configureCellForFavoriteClub:(PRVClubDataModel *)favoriteClub deleteBlock:(void(^)())deleteBlock{
    [self.clubPreviewView configureForFavoriteClub:favoriteClub];
    self.clubPreviewView.deleteBlock = deleteBlock;
}

@end
