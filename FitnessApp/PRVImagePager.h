//
//  PRVImagePager.h
//  FitnessApp
//
//  Created by Ruslan on 22.11.2017.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^PRVImagePagerImageRequestBlock)(UIImage*image, NSError * error);

@class PRVImagePager;

#pragma mark  - Data source
@protocol PRVImagePagerDataSource <NSObject>

@required
- (NSArray *) arrayWithImages:(PRVImagePager*)pager;
- (UIViewContentMode) contentModeForImage:(NSUInteger)image inPager:(PRVImagePager*)pager;

@optional
- (UIImage *) placeHolderImageForImagePager:(PRVImagePager*)pager;
- (NSString *) captionForImageAtIndex:(NSInteger)index  inPager:(PRVImagePager*)pager;
- (UIViewContentMode) contentModeForPlaceHolder:(PRVImagePager*)pager;

@end

#pragma mark  - Delegate
@protocol PRVImagePagerDelegate <NSObject>

@optional
- (void) imagePager:(PRVImagePager *)imagePager didScrollToIndex:(NSUInteger)index;
- (void) imagePager:(PRVImagePager *)imagePager didSelectImageAtIndex:(NSUInteger)index;

@end

#pragma mark  - Image source

@protocol PRVImagePagerImageSource <NSObject>

-(void) imageWithUrl:(NSURL*)url completion:(PRVImagePagerImageRequestBlock)completion;

@end


@interface PRVImagePager : UIView

// Delegate and Datasource
@property (weak) IBOutlet id <PRVImagePagerDataSource> dataSource;
@property (weak) IBOutlet id <PRVImagePagerDelegate> delegate;
@property (weak) IBOutlet id <PRVImagePagerImageSource> imageSource;


// General
@property (nonatomic) UIViewContentMode contentMode;
@property (nonatomic) UIScrollView *scrollView;
@property (nonatomic) UIPageControl *pageControl;
@property (nonatomic) NSUInteger currentPage;
@property (nonatomic) BOOL indicatorDisabled;
@property (nonatomic) BOOL bounces;
@property (nonatomic) BOOL imageCounterDisabled;
@property (nonatomic) BOOL hidePageControlForSinglePages; // Defaults YES

// Slideshow
@property (nonatomic) NSUInteger slideshowTimeInterval; // Defaults 0.0f (off)
@property (nonatomic) BOOL slideshowShouldCallScrollToDelegate; // Defaults YES

// Caption Label
@property (nonatomic, strong) UIColor *captionTextColor; // Defaults Black
@property (nonatomic, strong) UIColor *captionBackgroundColor; // Defaults White (with an alpha of .7f)
@property (nonatomic, strong) UIFont *captionFont; // Defaults to Helvetica 12.0f points

- (void) reloadData;
- (void) setCurrentPage:(NSUInteger)currentPage animated:(BOOL)animated;


@end
