//
//  PRVProgramListVC+Setup.h
//  FitnessApp
//
//  Created by Ruslan on 8/28/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVProgramListVC.h"

@interface PRVProgramListVC (Setup)

- (void)setupTableView;

@end
