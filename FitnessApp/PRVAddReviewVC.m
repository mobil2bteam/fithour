//
//  PRVGiveReviewVC.m
//  FitnessApp
//
//  Created by Ruslan on 7/24/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVAddReviewVC.h"
#import "StarRatingView.h"
#import "UIView+PRVExtensions.h"
#import "UIViewController+PRVExtensions.h"
#import "PRVServerManager.h"

@interface PRVAddReviewVC ()

@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIView *ratingView;
@property (strong, nonatomic) StarRatingView *starview;
@property (weak, nonatomic) IBOutlet UIButton *reviewButton;

@end

@implementation PRVAddReviewVC

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationItem.title = @"Оставить отзыв";
    self.reviewButton.backgroundColor = [UIColor primaryColor];
    self.starview = [[StarRatingView alloc]initWithFrame:self.ratingView.bounds andRating:100 withLabel:NO animated:NO];
    self.starview.animated = NO;
    [self.starview ratingDidChange];
    [self.ratingView addSubview:self.starview];
}

- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    self.textView.layer.masksToBounds = YES;
    self.textView.layer.borderWidth = 1.0f;
    self.textView.layer.borderColor = [UIColor groupTableViewBackgroundColor].CGColor;
    self.textView.layer.cornerRadius = 10.f;
}

#pragma mark - Actions

- (IBAction)addReviewButtonPressed:(id)sender {
    [self.view endEditing:YES];
    if (!self.textView.text.length) {
        [self.textView prv_shake];
        return;
    }
    NSInteger rating = self.starview.rating / 20;
    [self prv_addMBProgressHUD];
    
    [[PRVServerManager sharedManager] addReviewWithText:self.textView.text rating:rating clubId:self.clubId onSuccess:^(PRVReviewServerModel *review, NSString *error) {
        [self prv_hideMBProgressHUD];
        if (error) {
            [self prv_showMessage:error withTitle:nil];
        } else {
            if (self.reviewBlock) {
                self.reviewBlock(review);
            }
            [self.navigationController popViewControllerAnimated:YES];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [self prv_hideMBProgressHUD];
        [self prv_showMessage:error.localizedDescription withTitle:nil];
    }];
    
}

@end
