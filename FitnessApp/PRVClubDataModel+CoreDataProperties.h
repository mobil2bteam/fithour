//
//  PRVClubDataModel+CoreDataProperties.h
//  
//
//  Created by Ruslan on 9/11/17.
//
//

#import "PRVClubDataModel+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface PRVClubDataModel (CoreDataProperties)

+ (NSFetchRequest<PRVClubDataModel *> *)fetchRequest;

@property (nonatomic) int64_t clubID;
@property (nullable, nonatomic, copy) NSString *name;
@property (nonatomic) float rating;
@property (nullable, nonatomic, copy) NSString *image;
@property (nullable, nonatomic, copy) NSString *address;

@end

NS_ASSUME_NONNULL_END
