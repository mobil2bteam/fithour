//
//  PRVSectionedDataSource.h
//  FitnessApp
//
//  Created by Ruslan on 8/29/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVDataSource.h"

typedef void (^PRVSectionedCellConfigureBlock)(id cell, id item, id indexPath);

@interface PRVSectionedDataSource : NSObject <UITableViewDataSource, UICollectionViewDataSource>

- (id)initWithItems:(NSArray <NSArray *> *)items
     cellIdentifier:(NSString *)cellIdentifier
 configureCellBlock:(PRVSectionedCellConfigureBlock)configureCellBlock;

- (id)itemAtIndexPath:(NSIndexPath *)indexPath;

- (void)setItems:(NSArray <NSArray *> *)items;

@end
