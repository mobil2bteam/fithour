//
//  PRVFilterView.m
//  FitnessApp
//
//  Created by Ruslan on 7/20/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

static const NSTimeInterval kAnimationDuration = 0.2;

typedef NS_ENUM (NSInteger, PRVFilterType) {
    PRVFilterTypeGroup,
    PRVFilterTypeDate,
    PRVFilterTypeTime,
    PRVFilterTypePrice,
    PRVFilterTypeDone
};

#import "PRVFilterView.h"
#import "UIImageView+PRVExtension.h"
#import "PRVAppManager.h"
#import "PRVDirectionCollectionViewCell.h"
#import "PRVOptionsServerModel.h"
#import "PRVFilterManager.h"

@interface PRVFilterView() <UICollectionViewDelegate, UICollectionViewDataSource>

@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *groupView;
@property (weak, nonatomic) IBOutlet UIView *dateView;
@property (weak, nonatomic) IBOutlet UIView *priceView;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (weak, nonatomic) IBOutlet UILabel *groupLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIImageView *selectedGroupImageView;
@property (weak, nonatomic) IBOutlet UIImageView *selectedPriceImageView;
@property (weak, nonatomic) IBOutlet UIImageView *selectedTimeImageView;
@property (weak, nonatomic) IBOutlet UICollectionView *filterCollectionView;
@property (nonatomic, assign) PRVFilterType filterType;
@property (nonatomic, strong) PRVFilterManager *filterManager;

@end


@implementation PRVFilterView

#pragma mark - Lifecycle

- (void)awakeFromNib{
    [super awakeFromNib];
    self.sort = YES;
    self.filterType = PRVFilterTypeGroup;
    self.filterManager = [PRVFilterManager sharedInstance];
    [self.filterCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([PRVDirectionCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([PRVDirectionCollectionViewCell class])];
    self.filterCollectionView.delegate = self;
    self.filterCollectionView.dataSource = self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [[NSBundle mainBundle]loadNibNamed:@"PRVFilterView" owner:self options:nil];
        [self addSubview:_contentView];
        _contentView.frame = self.bounds;
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [[NSBundle mainBundle]loadNibNamed:@"PRVFilterView" owner:self options:nil];
        [self addSubview:_contentView];
        _contentView.frame = self.bounds;
        self.sort = YES;
        self.filterType = PRVFilterTypeGroup;
        self.filterManager = [PRVFilterManager sharedInstance];
        [self.filterCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([PRVDirectionCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([PRVDirectionCollectionViewCell class])];
        self.filterCollectionView.delegate = self;
        self.filterCollectionView.dataSource = self;
    }
    return self;
}

#pragma mark - Actions

- (IBAction)detailFilterButtonPressed:(id)sender {
    if (self.delegate) {
        [self.delegate detailFilterDidSelected];
    }
}

- (IBAction)backButtonPressed:(id)sender {
    [self showVisibleCells:NO animation:YES compilation:^{
        self.mainScrollView.hidden = NO;
        [self showMenuView:YES animation:YES compilation:nil];
    }];
}

- (IBAction)changeGroupButtonPressed:(id)sender {
    [self.filterManager setSelectedGroups:nil];
    self.filterType = PRVFilterTypeGroup;
    [self reloadFilterCollectionView];
    [self showMenuView:NO animation:YES compilation:^{
        [self showVisibleCells:NO animation:NO compilation:^{
            self.mainScrollView.hidden = YES;
            [self prepareGroupView];
            [self showVisibleCells:YES animation:YES compilation:nil];
        }];
    }];
}

- (IBAction)changeDateButtonPressed:(id)sender {
    [self.filterManager setSelectedDate:nil];
    [self.filterManager setSelectedTime:nil];
    self.filterType = PRVFilterTypeDate;
    [self reloadFilterCollectionView];
    [self showMenuView:NO animation:YES compilation:^{
        [self showVisibleCells:NO animation:NO compilation:^{
            self.mainScrollView.hidden = YES;
            [self prepareDateTimeView];
            [self showVisibleCells:YES animation:YES compilation:nil];
        }];
    }];
}

- (IBAction)changePriceButtonPressed:(id)sender {
    [self.filterManager setSelectedPrice:nil];
    self.filterType = PRVFilterTypePrice;
    [self reloadFilterCollectionView];
    [self showMenuView:NO animation:YES compilation:^{
        [self showVisibleCells:NO animation:NO compilation:^{
            self.mainScrollView.hidden = YES;
            [self preparePriceView];
            [self showVisibleCells:YES animation:YES compilation:nil];
        }];
    }];
}

#pragma mark - Methods

- (void)reloadFilterCollectionView{
    [self.filterCollectionView setContentOffset:CGPointZero];
    [self.filterCollectionView reloadData];
    if (self.delegate) {
        [self.delegate filterDidUpdate];
    }
}

- (void)selectGroup:(PRVGroupServerModel *)group{
    [self.filterManager setSelectedGroups:@[group]];
    self.filterType = PRVFilterTypeDate;
    [self prepareGroupView];
    [self showVisibleCells:NO animation:YES compilation:^{
        self.mainScrollView.hidden = NO;
        [self reloadFilterCollectionView];
        [self showMenuView:YES animation:YES compilation:nil];
    }];
}

- (void)selectDate:(PRVDateModel *)date{
    [self.filterManager setSelectedDate:date];
    self.filterType = PRVFilterTypeTime;
    [self showVisibleCells:NO animation:YES compilation:^{
        self.filterCollectionView.hidden = YES;
        [self.filterCollectionView reloadData];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self showVisibleCells:NO animation:NO compilation:nil];
            self.filterCollectionView.hidden = NO;
            [self showVisibleCells:YES animation:YES compilation:nil];
        });
    }];
}

- (void)selectTime:(NSString *)time{
    [self.filterManager setSelectedTime:time];
    self.filterType = PRVFilterTypePrice;
    [self prepareDateTimeView];
    [self showVisibleCells:NO animation:YES compilation:^{
        self.mainScrollView.hidden = NO;
        [self reloadFilterCollectionView];
        [self showMenuView:YES animation:YES compilation:nil];
    }];
}

- (void)selectPrice:(PRVPriceServerModel *)price{
    [self.filterManager setSelectedPrice:price];
    self.filterType = PRVFilterTypeDone;
    [self preparePriceView];
    [self showVisibleCells:NO animation:YES compilation:^{
        self.mainScrollView.hidden = NO;
        [self reloadFilterCollectionView];
        [self showMenuView:YES animation:YES compilation:nil];
    }];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    switch (self.filterType) {
        case PRVFilterTypeGroup:
            return [self.filterManager getGroupsOptions].count;
        case PRVFilterTypeDate:
            return [self.filterManager getDatesOptions].count;
        case PRVFilterTypeTime:
            return [self.filterManager getTimesOptions].count;
        case PRVFilterTypePrice:
            return [self.filterManager getPricesOptions].count;
        case PRVFilterTypeDone:
            return 0;
    }
}

- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.filterType == PRVFilterTypeDate) {
        return CGSizeMake(120, 30);
    }
    if (self.filterType == PRVFilterTypeGroup) {
        return CGSizeMake(50, 30);
    } else if (self.filterType == PRVFilterTypePrice){
        return CGSizeMake(100, 30);
    } else {
        return CGSizeMake(60, 30);
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    PRVDirectionCollectionViewCell *cell = (PRVDirectionCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([PRVDirectionCollectionViewCell class]) forIndexPath:indexPath];
    switch (self.filterType) {
        case PRVFilterTypeGroup:
            [cell.iconImageView prv_setImageWithURL:self.filterManager.getGroupsOptions[indexPath.item].icon];
            break;
        case PRVFilterTypeDate:
            cell.nameLabel.text = self.filterManager.getDatesOptions[indexPath.item].text;
            break;
        case PRVFilterTypeTime:
            cell.nameLabel.text = self.filterManager.getTimesOptions[indexPath.item];
            break;
        case PRVFilterTypePrice:
            cell.nameLabel.text = self.filterManager.getPricesOptions[indexPath.item].text;
            break;
        default:
            break;
    }
    [cell setSelected:[collectionView.indexPathsForSelectedItems containsObject:indexPath]];
    cell.layer.cornerRadius = 15.f;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    switch (self.filterType) {
        case PRVFilterTypeGroup:
            [self selectGroup:[self.filterManager getGroupsOptions][indexPath.item]];
            break;
        case PRVFilterTypeDate:
            [self selectDate:[self.filterManager getDatesOptions][indexPath.item]];
            break;
        case PRVFilterTypeTime:
            [self selectTime:[self.filterManager getTimesOptions][indexPath.item]];
            break;
        case PRVFilterTypePrice:
            [self selectPrice:[self.filterManager getPricesOptions][indexPath.item]];
            break;
        default:
            break;
    }
}

#pragma mark - Preparation views

- (void)prepareGroupView{
    if ([self.filterManager getSelectedGroups].count == 0) {
        self.groupView.backgroundColor = [UIColor borderColor];
        self.groupLabel.textColor = [UIColor secondTextColor];
        self.groupLabel.text = @"Активность";
        self.selectedGroupImageView.image = [UIImage imageNamed:@"gym_icon_dark"];
    } else {
        [self.selectedGroupImageView prv_setImageWithURL:[self.filterManager getSelectedGroups][0].icon_select];
        self.groupView.backgroundColor = [UIColor secondColor];
        self.groupLabel.textColor = [UIColor whiteColor];
        if ([self.filterManager getSelectedGroups].count == 1) {
            self.groupLabel.text = [self.filterManager getSelectedGroups][0].name;
        } else {
            NSMutableString *string = [NSMutableString stringWithFormat:@"%@, %@", [self.filterManager getSelectedGroups][0].name, [self.filterManager getSelectedGroups][1].name];
            if ([self.filterManager getSelectedGroups].count > 2) {
                [string appendString:[NSString stringWithFormat:@", %@", [self.filterManager getSelectedGroups][2].name]];
            }
            if ([self.filterManager getSelectedGroups].count > 3) {
                [string appendString:@"..."];
            }
            self.groupLabel.text = [string copy];
        }
    }
}

- (void)prepareDateTimeView{
    if ([self.filterManager getSelectedTime]) {
        self.dateView.backgroundColor = [UIColor secondColor];
        self.dateLabel.textColor = [UIColor whiteColor];
        NSString *date = [self.filterManager getSelectedDate].text;
        self.dateLabel.text = [NSString stringWithFormat:@"%@ на %@", date, [self.filterManager getSelectedTime]];
        self.selectedTimeImageView.image = [UIImage imageNamed:@"clock_work_white"];
    } else {
        self.dateView.backgroundColor = [UIColor colorWithWhite:0.914 alpha:1.0];
        self.dateLabel.textColor = [UIColor secondTextColor];
        self.dateLabel.text = @"Дата и время";
        self.selectedTimeImageView.image = [UIImage imageNamed:@"clock_work_dark"];
    }
}

- (void)preparePriceView{
    if ([self.filterManager getSelectedPrice]) {
        self.priceView.backgroundColor = [UIColor secondColor];
        self.priceLabel.textColor = [UIColor whiteColor];
        self.priceLabel.text = [self.filterManager getSelectedPrice].text;
        self.selectedPriceImageView.image = [UIImage imageNamed:@"dollar_white"];
    } else {
        self.priceView.backgroundColor = [UIColor borderColor];
        self.priceLabel.textColor = [UIColor secondTextColor];
        self.priceLabel.text = @"Стоимость";
        self.selectedPriceImageView.image = [UIImage imageNamed:@"dollar_dark"];
    }
}

#pragma mark - Animations

- (void)showVisibleCells:(BOOL)show animation:(BOOL)animation compilation:(void (^)(void))compilation{
    // get visible cells
    NSArray <UICollectionViewCell *> *cells = [self.filterCollectionView visibleCells];
    void (^animationBlock)() = ^void() {
        for (UICollectionViewCell *cell in cells) {
            cell.transform = show ? CGAffineTransformIdentity : CGAffineTransformMakeScale(0.01, 0.01);
        }
    };
    if (animation) {
        [UIView animateWithDuration:kAnimationDuration animations:^{
            animationBlock();
        } completion:^(BOOL finished) {
            if (finished && compilation) {
                compilation();
            }
        } ];
    } else {
        animationBlock();
        if (compilation) {
            compilation();
        }
    }
}

- (void)showMenuView:(BOOL)show animation:(BOOL)animation compilation:(void (^)(void))compilation{
    [self.mainScrollView setContentOffset:CGPointZero];
    NSArray <UIView *> *views = @[_groupView, _dateView, _priceView];
    void (^animationBlock)() = ^void() {
        for (UIView *view in views) {
            view.transform = show ? CGAffineTransformIdentity : CGAffineTransformMakeScale(0.01, 0.01);
        }
    };
    if (animation) {
        [UIView animateWithDuration:kAnimationDuration animations:^{
            animationBlock();
        } completion:^(BOOL finished) {
            if (finished && compilation) {
                compilation();
            }
        } ];
    } else {
        animationBlock();
        if (compilation) {
            compilation();
        }
    }
}

- (void)updateView{
    [self prepareGroupView];
    [self preparePriceView];
    [self prepareDateTimeView];
    [self reloadFilterCollectionView];
}

@end
