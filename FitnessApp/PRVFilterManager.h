//
//  PRVFilterManager.h
//  FitnessApp
//
//  Created by Ruslan on 8/13/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//
@class PRVPriceServerModel;
@class PRVGroupServerModel;

typedef NS_ENUM(NSInteger, PRVSortMode) {
    PRVSortModeDistance,
    PRVSortModeRating
};

#import <Foundation/Foundation.h>

@interface PRVDateModel : NSObject

@property (nonatomic, copy) NSString *text;
@property (nonatomic, copy) NSString *value;

@end

@interface PRVFilterManager : NSObject

+ (instancetype)sharedInstance;

- (NSArray <NSString *> *)getTimesOptions;
- (NSArray <PRVDateModel *> *)getDatesOptions;
- (NSArray <PRVPriceServerModel *> *)getPricesOptions;
- (NSArray <PRVGroupServerModel *> *)getGroupsOptions;

- (PRVSortMode)getSortMode;
- (NSString *)getSelectedTime;
- (PRVDateModel *)getSelectedDate;
- (PRVPriceServerModel *)getSelectedPrice;
- (NSArray <PRVGroupServerModel *> *)getSelectedGroups;

- (void)setSortMode:(PRVSortMode)sortMode;
- (void)setSelectedTime:(NSString *)time;
- (void)setSelectedDate:(PRVDateModel *)date;
- (void)setSelectedPrice:(PRVPriceServerModel *)price;
- (void)setSelectedGroups:(NSArray <PRVGroupServerModel *> *)groups;

@end
