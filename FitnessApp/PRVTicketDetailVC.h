//
//  PRVTicketDetailVC.h
//  FitnessApp
//
//  Created by Ruslan on 8/20/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//
@class PRVTicketSeverModel;

#import <UIKit/UIKit.h>

@interface PRVTicketDetailVC : UIViewController

@property (nonatomic, strong) PRVTicketSeverModel *ticket;

@end
