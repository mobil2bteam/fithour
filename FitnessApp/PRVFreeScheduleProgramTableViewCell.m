//
//  PRVFreeScheduleProgramTableViewCell.m
//  FitnessApp
//
//  Created by Ruslan on 27.11.2017.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVFreeScheduleProgramTableViewCell.h"
#import "PRVOptionsServerModel.h"

@implementation PRVFreeScheduleProgramTableViewCell

- (void)configureCellWithProgram:(PRVProgramServerModel *)program forDate:(NSDate *)date{
}

- (void)prepareForReuse{
    [super prepareForReuse];
    self.programLabel.text = @"";
    self.hoursLabel.text = @"";
}
@end
