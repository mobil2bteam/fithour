//
//  PRVCoachScheduleServerModel.m
//  FitnessApp
//
//  Created by Ruslan on 9/18/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVCoachScheduleServerModel.h"
#import "PRVOptionsServerModel.h"

@implementation PRVCoachScheduleServerModel

+ (EKObjectMapping *)objectMapping{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasMany:[PRVScheduleServerModel class] forKeyPath:@"schedule.items" forProperty:@"items"];
        [mapping mapKeyPath:@"schedule.coachId" toProperty:@"coachId"];
    }];
}


- (NSArray <NSArray<PRVScheduleServerModel *> *> *)splitScheduleByWeeks{
    NSMutableArray *weeksArray = [[NSMutableArray alloc] init];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"d.MM.y";
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSMutableArray <PRVScheduleServerModel *> *daysArray = [[NSMutableArray alloc] init];
    
    for (PRVScheduleServerModel *schedule in self.items) {
        NSDate *date = [formatter dateFromString:schedule.date];
        NSInteger dayIndex = [calendar component:NSCalendarUnitWeekday fromDate:date];
        if (daysArray.count == 0) {
            [daysArray addObject:schedule];
        } else {
            NSDate *lastDate = [formatter dateFromString:daysArray.lastObject.date];
            NSInteger lastDayIndex = [calendar component:NSCalendarUnitWeekday fromDate:lastDate];
            // if current day index is bigger than last day index, then add the to current week, else create new week and add it there
            if (dayIndex > lastDayIndex) {
                [daysArray addObject:schedule];
            } else {
                [weeksArray addObject:[daysArray copy]];
                daysArray = [[NSMutableArray alloc] init];
                [daysArray addObject:schedule];
            }
        }
    }
    [weeksArray addObject:daysArray];
    return [weeksArray copy];
}

@end
