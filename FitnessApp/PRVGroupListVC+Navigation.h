//
//  PRVGroupListVC+Navigation.h
//  FitnessApp
//
//  Created by Ruslan on 8/28/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//
@class PRVGroupServerModel;

#import "PRVGroupListVC.h"

@interface PRVGroupListVC (Navigation)

- (void)showProgramListViewControllerForGroup:(PRVGroupServerModel *)group;

- (void)showSelectDayViewController;

- (void)showCoachListViewController;

@end
