//
//  PRVGiveReviewVC.h
//  FitnessApp
//
//  Created by Ruslan on 7/24/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PRVReviewServerModel;

@interface PRVAddReviewVC : UIViewController
@property (nonatomic, assign) NSInteger clubId;
@property (nonatomic, copy) void (^reviewBlock)(PRVReviewServerModel *review);
@end
