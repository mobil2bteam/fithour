//
//  PRVSelectDayVC+Navigation.m
//  FitnessApp
//
//  Created by Ruslan on 9/20/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVSelectDayVC+Navigation.h"
#import "PRVSelectTimeVC.h"

@implementation PRVSelectDayVC (Navigation)

- (void)showSelectTimeViewControllerForDate:(NSDate *)date{
    PRVSelectTimeVC *vc = [[PRVSelectTimeVC alloc] prv_initWithNib];
    vc.club = self.club;
    vc.selectedDate = date;
    [self.navigationController pushViewController:vc animated:YES];
}

@end
