//
//  PRVCancelTicketAlertVC.h
//  FitnessApp
//
//  Created by Ruslan on 11/1/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PRVCancelTicketAlertVC : UIViewController

@property (nonatomic, copy) void (^completionCallback)(void);

@end
