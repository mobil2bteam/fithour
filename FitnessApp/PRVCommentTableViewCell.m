//
//  PRVCommentTableViewCell.m
//  FitnessApp
//
//  Created by Ruslan on 8/23/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVCommentTableViewCell.h"
#import "StarRatingView.h"
#import "UIColor+PRVHexColor.h"

@interface PRVCommentTableViewCell()

@property (weak, nonatomic) IBOutlet UIView *ratingView;
@property (strong, nonatomic) StarRatingView *starview;

@end

@implementation PRVCommentTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.starview = [[StarRatingView alloc]initWithFrame:self.ratingView.bounds andRating:100 withLabel:NO animated:NO];
    self.starview.animated = NO;
    [self.starview ratingDidChange];
    [self.ratingView addSubview:self.starview];
}

- (void)setRating:(NSInteger)rating{
    [self.starview setRating:rating * 20];
    [self.starview ratingDidChange];
}

- (void)setNick:(NSString *)nick{
    NSString *firstLetter = [[nick substringToIndex:1] uppercaseString];
    self.userNickLabel.text = firstLetter;
    NSString *hexColor = [self colorForLetter:[firstLetter characterAtIndex:0]];
    self.userNickLabel.backgroundColor =  [UIColor prv_colorWithHex:hexColor];
}

- (void)prepareForReuse{
    [super prepareForReuse];
    self.userNickLabel.text = @"";
    self.userNameLabel.text = @"";
    self.commentLabel.text = @"";
    [self setRating:5 * 20];
    [self.starview ratingDidChange];
}

- (NSString *)colorForLetter:(unichar)letter{
    if (letter == L'А') {
        return @"#9C27B0";
    }
    if (letter == L'Б') {
        return @"#E91E63";
    }
    if (letter == L'В') {
        return @"#F44336";
    }
    if (letter == L'Г') {
        return @"#673AB7";
    }
    if (letter == L'Д') {
        return @"#3F51B5";
    }
    if (letter == L'Е') {
        return @"#2196F3";
    }
    if (letter == L'Ж') {
        return @"#03A9F4";
    }
    if (letter == L'З') {
        return @"#00BCD4";
    }
    if (letter == L'И') {
        return @"#009688";
    }
    if (letter == L'К') {
        return @"#4CAF50";
    }
    if (letter == L'Л') {
        return @"#8BC34A";
    }
    if (letter == L'М') {
        return @"#CDDC39";
    }
    if (letter == L'Н') {
        return @"#FFEB3B";
    }
    if (letter == L'О') {
        return @"#FFC107";
    }
    if (letter == L'П') {
        return @"#FF9800";
    }
    if (letter == L'Р') {
        return @"#FF5722";
    }
    if (letter == L'С') {
        return @"#795548";
    }
    if (letter == L'Т') {
        return @"#424242";
    }
    if (letter == L'Ф') {
        return @"#607D8B";
    }
    if (letter == L'Х') {
        return @"#263238";
    }
    if (letter == L'Ю') {
        return @"#607D8B";
    }
    if (letter == L'Ч') {
        return @"#F4511E";
    }
    if (letter == L'Ц') {
        return @"#795548";
    }
    if (letter == L'У') {
        return @"#00E676";
    }
    if (letter == L'Ш') {
        return @"#64DD17";
    }
    if (letter == L'Щ') {
        return @"#827717";
    }
    if (letter == L'Я') {
        return @"#388E3C";
    }
    if (letter == 'Q') {
        return @"#D32F2F";
    }
    if (letter == 'W') {
        return @"#C2185B";
    }
    if (letter == 'E') {
        return @"#7B1FA2";
    }
    if (letter == 'R') {
        return @"#512DA8";
    }
    if (letter == 'T') {
        return @"#303F9F";
    }
    if (letter == 'Y') {
        return @"#1976D2";
    }
    if (letter == 'U') {
        return @"#0288D1";
    }
    if (letter == 'I') {
        return @"#0097A7";
    }
    if (letter == 'O') {
        return @"#00796B";
    }
    if (letter == 'P') {
        return @"#388E3C";
    }
    if (letter == 'A') {
        return @"#689F38";
    }
    if (letter == 'S') {
        return @"#AFB42B";
    }
    if (letter == 'D') {
        return @"#FBC02D";
    }
    if (letter == 'F') {
        return @"#FFA000";
    }
    if (letter == 'G') {
        return @"#EF6C00";
    }
    if (letter == 'H') {
        return @"#E64A19";
    }
    if (letter == 'J') {
        return @"#5D4037";
    }
    if (letter == 'K') {
        return @"#616161";
    }
    if (letter == 'L') {
        return @"#263238";
    }
    if (letter == 'Z') {
        return @"#0D47A1";
    }
    if (letter == 'X') {
        return @"#1A237E";
    }
    if (letter == 'C') {
        return @"#651FFF";
    }
    if (letter == 'V') {
        return @"#3D5AFE";
    }
    if (letter == 'B') {
        return @"#00E5FF";
    }
    if (letter == 'N') {
        return @"#1DE9B6";
    }
    if (letter == 'M') {
        return @"#558B2F";
    }
    return @"#FB7979";
}

@end
