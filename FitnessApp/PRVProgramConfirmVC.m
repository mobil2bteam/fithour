//
//  PRVProgramConfirmVC.m
//  FitnessApp
//
//  Created by Ruslan on 7/31/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVProgramConfirmVC.h"
#import "PRVClubInfoView.h"
#import "PRVOptionsServerModel.h"
#import "PRVClubsServerModel.h"
#import "PRVServerManager.h"
#import "PRVEnrollFinishVC.h"
#import "UIImageView+PRVExtension.h"
#import "PRVDataSource.h"
#import "PRVImagePager.h"
#import "PRVAppManager.h"
#import "PRVPreSignUpStep1VC.h"
#import "PRVNotificationManager.h"
#import "PRVUserServerModel.h"
#import "PRVTicketCancelViewController.h"
#import "PRVPaymentVC.h"

@interface PRVProgramConfirmVC () <PRVImagePagerDataSource>
@property (weak, nonatomic) IBOutlet PRVClubInfoView *clubInfoView;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *additionalPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *placesLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *groupLabel;
@property (weak, nonatomic) IBOutlet UILabel *programLabel;
@property (weak, nonatomic) IBOutlet UIView *freeScheduleView;
@property (weak, nonatomic) IBOutlet PRVImagePager *sliderView;
@property (weak, nonatomic) IBOutlet UIImageView *groupIconImageView;
@property (weak, nonatomic) IBOutlet UIButton *enrollButton;
@property (weak, nonatomic) IBOutlet UIView *coachView;
@property (weak, nonatomic) IBOutlet UIImageView *coachImageView;
@property (weak, nonatomic) IBOutlet UILabel *coachLabel;
@property (weak, nonatomic) IBOutlet UILabel *coachRankLabel;
@property (nonatomic, strong) PRVDataSource *sliderDataSource;
@property (weak, nonatomic) IBOutlet UIView *freePlacesView;
@property (weak, nonatomic) IBOutlet UIView *groupInfoView;
@property (weak, nonatomic) IBOutlet UIView *personalCoachView;
@property (nonatomic, strong) PRVTicketSeverModel *ticket;
@property (weak, nonatomic) IBOutlet UIView *firstFreeView;
@end

@implementation PRVProgramConfirmVC

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationItem.title = @"Запись";
    if (!self.program.images.count) {
        self.sliderView.hidden = YES;
    } else {
        self.sliderView.dataSource = self;
        self.sliderView.imageCounterDisabled = YES;
    }
    [self prepareView];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (self.ticket != nil) {
        [self loadTicket];
    }
}

- (void)loadTicket {
    __weak typeof(self) weakSelf = self;
    [self prv_addMBProgressHUD];
    [[PRVServerManager sharedManager] getTicketWithID:self.ticket._id onSuccess:^(PRVTicketSeverModel *ticket, NSString *error) {
        typeof(self) strongSelf = weakSelf;
        if (ticket != nil) {
            if (ticket.isPayment) {
                [strongSelf prv_hideMBProgressHUD];
                [strongSelf showFinishViewController:ticket];
            } else {
                [strongSelf cancelTicket];
            }
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        typeof(self) strongSelf = weakSelf;
        [strongSelf prv_hideMBProgressHUD];
        [strongSelf prv_showMessage:error.localizedDescription withRepeatHandler:^(UIAlertAction *action) {
            [self loadTicket];
        }];
    }];
}

#pragma mark - PRVImagePagerDataSource

- (NSArray *) arrayWithImages:(PRVImagePager*)pager{
    return self.program.images;
}

- (UIViewContentMode) contentModeForImage:(NSUInteger)image inPager:(PRVImagePager*)pager{
    return UIViewContentModeScaleAspectFill;
}

#pragma mark - Methods

- (void)prepareView{
    [self.clubInfoView setClub:self.club];
    self.firstFreeView.hidden = !self.club.first_free;
    self.enrollButton.backgroundColor = [UIColor secondColor];
    self.priceLabel.textColor = [UIColor primaryColor];
    [self.clubInfoView setAddressLabelFontSize:15];
    self.dateLabel.text = self.date;
    self.priceLabel.text = [NSString stringWithFormat:@"%ld р.", (long)self.price];
    
    if (self.enrollType != PRVEnrollTicketTypeDefault) {
        [self.enrollButton setTitle:@"ЗАПИСАТЬСЯ" forState:UIControlStateNormal];
        self.freePlacesView.hidden = YES;
        self.groupInfoView.hidden = YES;
    }
    if (self.club.price_minute) {
        self.additionalPriceLabel.text = [NSString stringWithFormat:@"(доп. 1 мин. = %ld руб.)", (long)self.club.price_minute];
    }

    if (self.enrollType == PRVEnrollTicketTypeDefault) {
        self.coach = [self.club coachById:self.hour.coachId];
        self.groupLabel.text = self.group.name;
        self.programLabel.text = self.program.name;
        [self.groupIconImageView prv_setImageWithURL:self.group.icon];
        self.placesLabel.text = [NSString stringWithFormat:@"%ld", (long)self.hour.places];
        self.timeLabel.text = [NSString stringWithFormat:@"%@ (%ld мин.)", self.hour.from, (long)self.hour.duration];
    }
    
    if (self.enrollType == PRVEnrollTicketTypePersonalCoach) {
        self.personalCoachView.hidden = NO;
        self.clubInfoView.hidden = NO;
        NSArray *listTimes = [self.timeModel.value componentsSeparatedByString:@" - "];
        if (listTimes.count) {
            self.timeLabel.text = [NSString stringWithFormat:@"%@ (60 мин.)", listTimes[0]];
        }
    }
    
    if (self.enrollType == PRVEnrollTicketTypeFreeSchedule) {
        self.timeLabel.text = self.time;
        self.freeScheduleView.hidden = NO;
        self.sliderView.hidden = YES;
        self.coachView.hidden = YES;
    }
    
    if (self.coach) {
        [self.coachImageView prv_setImageWithURL:self.coach.image];
        self.coachLabel.text = self.coach.name;
        self.coachRankLabel.text = self.coach.rank;
        self.coachView.hidden = NO;
    }
}

#pragma mark - Actions

- (IBAction)enrollButtonPressed:(id)sender {
    if ([[PRVAppManager sharedInstance] getUser]) {
        [self enroll];
    } else {
        __weak typeof(self) weakSelf = self;
        PRVPreSignUpStep1VC *vc = [[PRVPreSignUpStep1VC alloc] prv_initWithNib];
        vc.sighInForBuyTicket = YES;
        vc.loginCallback = ^{
            [weakSelf enroll];
        };
        UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:vc];
        [self presentViewController:navVC animated:YES completion:nil];
    }
}

- (void)enroll{
    if (self.enrollType == PRVEnrollTicketTypePersonalCoach) {
        NSArray *listTimes = [self.timeModel.value componentsSeparatedByString:@" - "];
        [self prv_addMBProgressHUD];
        [[PRVServerManager sharedManager] buyTicketForPersonalTrainingWithCoachId:self.coach._id date:self.date time:listTimes[0] onSuccess:^(PRVTicketSeverModel *ticket, NSString *error) {
            [self prv_hideMBProgressHUD];
            if (ticket) {
                self.ticket = ticket;
                if (!ticket.isPayment && ticket.price > 0) {
                    [self showPaymentController];
                } else {
                    [self showFinishViewController:ticket];
                }
            } else {
                [self prv_showMessage:error withTitle:nil];
            }
        } onFailure:^(NSError *error, NSInteger statusCode) {
            [self prv_hideMBProgressHUD];
            [self prv_showMessage:error.localizedDescription withTitle:nil];
        }];
        return;
    }
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    if (self.enrollType == PRVEnrollTicketTypeFreeSchedule) {
        params[@"free_time"] = self.time;
        params[@"free"] = @(1);
    } else {
        params[@"time_id"] = @(self.hour._id);
    }
    params[@"date"] = self.date;
    params[@"club_id"] = @(self.club._id);
    [self prv_addMBProgressHUD];
    [[PRVServerManager sharedManager] buyTicketWithParams:[params copy] onSuccess:^(PRVTicketSeverModel *ticket, NSString *error) {
        [self prv_hideMBProgressHUD];
        if (ticket) {
            self.ticket = ticket;
            if (!ticket.isPayment && ticket.price > 0) {
                [self showPaymentController];
            } else {
                [self showFinishViewController:ticket];
            }
        } else {
            [self prv_showMessage:error withTitle:nil];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [self prv_hideMBProgressHUD];
        [self prv_showMessage:error.localizedDescription withTitle:nil];
    }];
}

- (void)cancelTicket{
    __weak typeof(self) weakSelf = self;
    [[PRVServerManager sharedManager] cancelTicketWithID:self.ticket._id onSuccess:^(PRVTicketSeverModel *ticket, NSString *error) {
        typeof(self) strongSelf = weakSelf;
        [strongSelf prv_hideMBProgressHUD];
        if (error) {
            [strongSelf prv_showMessage:error withRepeatHandler:^(UIAlertAction *action) {
                [strongSelf cancelTicket];
            }];
        } else {
            [strongSelf showTicketCancleViewController];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        typeof(self) strongSelf = weakSelf;
        [strongSelf prv_hideMBProgressHUD];
        [strongSelf prv_showMessage:error.localizedDescription withRepeatHandler:^(UIAlertAction *action) {
            [strongSelf cancelTicket];
        }];
    }];
}

- (void)showPaymentController{
    PRVPaymentVC *vc = [[PRVPaymentVC alloc] prv_initWithNib];
    vc.ticket = self.ticket;
    UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:navVC animated:NO completion:nil];
}

- (void)showFinishViewController:(PRVTicketSeverModel *)ticket{
    PRVEnrollFinishVC *vc = [[PRVEnrollFinishVC alloc] init];
    vc.ticket = ticket;
    if (self.enrollType == PRVEnrollTicketTypePersonalCoach) {
        vc.isPersonal = YES;
    }
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)showTicketCancleViewController{
    PRVTicketCancelViewController *vc = [[PRVTicketCancelViewController alloc] initWithNibName:@"PRVTicketCancelViewController" bundle:nil];
    [self.navigationController pushViewController:vc animated:YES];
}

@end
