//
//  PRVOnboardVC.h
//  FitnessApp
//
//  Created by Ruslan on 7/15/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PRVOnboardVC : UIViewController
@property (nonatomic, copy) void (^callback)(void);
@end
