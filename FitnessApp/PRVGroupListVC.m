//
//  PRVEnrollGroupVC.m
//  FitnessApp
//
//  Created by Ruslan on 7/25/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVGroupListVC.h"
#import "PRVGroupListVC+Setup.h"
#import "PRVGroupListVC+Navigation.h"
#import "PRVClubsServerModel.h"
#import "PRVOptionsServerModel.h"
#import "PRVClubInfoView.h"

@interface PRVGroupListVC ()

@property (weak, nonatomic) IBOutlet PRVClubInfoView *clubInfoView;
@property (weak, nonatomic) IBOutlet UIView *freeScheduleView;
@property (weak, nonatomic) IBOutlet UIView *coachView;

@end

@implementation PRVGroupListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Запись";
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self.clubInfoView setClub:self.club];
    [self setupGroupTableView];
    self.freeScheduleView.hidden = ![self.club hasGroupsWithFreeSchdeule];
    if (self.club.coaches.count && [self.club hasGroupsWithFreeSchdeule]) {
        self.coachView.hidden = NO;
    }
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self showProgramListViewControllerForGroup:[self.club groupsWithoutFreeSchedule][indexPath.row]];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (BOOL)hidesBottomBarWhenPushed
{
    return YES;
}

- (IBAction)freeScheduleButtonPressed:(id)sender {
    [self showSelectDayViewController];
}

- (IBAction)coachListButtonPressed:(id)sender {
    [self showCoachListViewController];
}

@end
