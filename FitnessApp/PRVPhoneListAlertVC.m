//
//  PRVPhoneListAlertVC.m
//  FitnessApp
//
//  Created by Ruslan on 8/23/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//
static NSString * const kPhoneTableViewCell = @"PRVPhoneTableViewCell";

#import "PRVPhoneListAlertVC.h"
#import "PRVPhoneTableViewCell.h"
#import "PRVClubsServerModel.h"

@interface PRVPhoneListAlertVC ()

@property (weak, nonatomic) IBOutlet UITableView *phonesTableView;

@end

@implementation PRVPhoneListAlertVC

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.phonesTableView registerNib:[UINib nibWithNibName:kPhoneTableViewCell bundle:nil] forCellReuseIdentifier:kPhoneTableViewCell];
}

#pragma mark - Actions

- (IBAction)cancelButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return self.phones.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PRVPhoneTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kPhoneTableViewCell forIndexPath:indexPath];
    [self configureCell:cell forRowAtIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(PRVPhoneTableViewCell *)cell
    forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.nameLabel.text = self.phones[indexPath.row].name;
    cell.phoneLabel.text = self.phones[indexPath.row].phone;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    __weak typeof(self) weakSelf = self;
    [self dismissViewControllerAnimated:YES completion:^{
        typeof(self) strongSelf = weakSelf;
        if (strongSelf.phoneBlock) {
            strongSelf.phoneBlock(strongSelf.phones[indexPath.row].phone);
        }
    }];
}

@end
