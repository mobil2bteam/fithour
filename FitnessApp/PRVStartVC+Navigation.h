//
//  PRVStartVC+Navigation.h
//  FitnessApp
//
//  Created by Ruslan on 8/27/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVStartVC.h"

@interface PRVStartVC (Navigation)

- (void)showMainViewController;

- (void)showOnboardingViewController;

- (void)showGpsViewController;

@end
