//
//  UIImageView+PRVExtension.m
//  FitnessApp
//
//  Created by Ruslan on 7/12/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "UIImageView+PRVExtension.h"
#import "UIImageView+AFNetworking.h"

@implementation UIImageView (PRVExtension)

- (void)prv_setImageWithURL:(NSString *)url{
    NSURLRequest *imageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:url]
                                                  cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                              timeoutInterval:60];
    [self setImageWithURLRequest:imageRequest
                placeholderImage:nil
                         success:nil
                         failure:nil];
}

@end
