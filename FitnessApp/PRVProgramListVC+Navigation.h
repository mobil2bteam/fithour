//
//  PRVProgramListVC+Navigation.h
//  FitnessApp
//
//  Created by Ruslan on 8/28/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//
@class PRVProgramServerModel;

#import "PRVProgramListVC.h"

@interface PRVProgramListVC (Navigation)

- (void)showProgramDetailViewControllerForProgram:(PRVProgramServerModel *)program;
- (void)showProgramListViewControllerForGroup:(PRVGroupServerModel *)group;
- (void)showSelectDayViewController;

@end
