//
//  PRVClubDetailVC.m
//  FitnessApp
//
//  Created by Ruslan on 22.11.2017.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVClubDetailVC.h"
#import "PRVClubsServerModel.h"
#import "UILabel+PRVExtension.h"

@interface PRVClubDetailVC ()
@property (nonatomic, weak) IBOutlet UILabel *descriptionLabel;
@end

@implementation PRVClubDetailVC

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Описание";
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.automaticallyAdjustsScrollViewInsets = NO;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.descriptionLabel setHTML:self.club._description];
}

@end
