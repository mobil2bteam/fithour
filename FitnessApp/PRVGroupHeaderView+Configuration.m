//
//  PRVGroupHeaderView+Configuration.m
//  FitnessApp
//
//  Created by Ruslan on 8/29/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVGroupHeaderView+Configuration.h"
#import "PRVOptionsServerModel.h"
#import "UIImageView+PRVExtension.h"

@implementation PRVGroupHeaderView (Configuration)

- (void)configureHeaderForGroup:(PRVGroupServerModel *)group{
    [self.groupImageView prv_setImageWithURL:group.icon];
    self.groupLabel.text = group.name;
}

@end
