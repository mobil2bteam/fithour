//
//  PRVLocationServiceImplementation.h
//  FitnessApp
//
//  Created by Ruslan on 9/7/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "PRVLocationService.h"

@protocol PRVLocationServiceDelegate;

@interface PRVLocationServiceImplementation : NSObject <PRVLocationService, CLLocationManagerDelegate>

/**
 The locationService's delegate, which is informed when location is updated
 */
@property (nonatomic, weak) id<PRVLocationServiceDelegate> delegate;

@end
