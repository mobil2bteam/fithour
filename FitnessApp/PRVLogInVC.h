//
//  PRVLogInVC.h
//  FitnessApp
//
//  Created by Ruslan on 7/19/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PRVLogInVC : UIViewController

@property (nonatomic, copy) void (^loginCallback)(void);
@property (nonatomic, assign) BOOL hideSignUpButton;

@end
