//
//  PRVOptionsServerModel.m
//  FitnessApp
//
//  Created by Ruslan on 7/11/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVOptionsServerModel.h"
#import "PRVCache+CoreDataProperties.h"
#import "NSString+PRVExtension.h"
#import "NSDate+PRVComparation.h"

@implementation PRVOptionsServerModel

+ (EKObjectMapping *)objectMapping{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasMany:[PRVUrlServerModel class] forKeyPath:@"options.urls" forProperty:@"urls"];
        [mapping hasMany:[PRVGroupServerModel class] forKeyPath:@"directory.group_list.items" forProperty:@"group_list"];
        [mapping hasMany:[PRVCityServerModel class] forKeyPath:@"directory.city_list.items" forProperty:@"city_list"];
        [mapping hasMany:[PRVPriceServerModel class] forKeyPath:@"directory.prices_filters" forProperty:@"price_list"];
    }];
}

- (instancetype)initWithCache:(PRVCache *)cache{
    self = [super init];
    if (self) {
        NSDictionary *optionsData = [cache.json prv_dictionaryValue];
        self = [EKMapper objectFromExternalRepresentation:optionsData withMapping:[PRVOptionsServerModel objectMapping]];
    }
    return self;
}

- (PRVUrlServerModel *)urlForName:(NSString *)name{
    for (PRVUrlServerModel *url in self.urls) {
        if ([url.name isEqualToString:name]) {
            return url;
        }
    }
    return nil;
}

@end


@implementation PRVUrlServerModel

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"name", @"url", @"metod"]];
    }];
}

@end

@implementation PRVGroupServerModel

+ (EKObjectMapping *)objectMapping{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasMany:[PRVProgramServerModel class] forKeyPath:@"programs"];
        [mapping mapPropertiesFromArray:@[@"name", @"icon", @"icon_select", @"images"]];
        [mapping mapKeyPath:@"description" toProperty:@"_description"];
        [mapping mapKeyPath:@"id" toProperty:@"_id"];
    }];
}

- (NSArray <PRVProgramServerModel *> *)programsForDate:(NSDate *)date{
    NSMutableArray <PRVProgramServerModel *> *programs = [[NSMutableArray alloc] init];
    for (PRVProgramServerModel *program in self.programs) {
        for (PRVScheduleServerModel *schedule in program.schedule) {
            NSDate *scheduleDate = [NSDate dateFromStringDate:schedule.date withDateFormat:@"d.MM.y"];
            if ([date isEqualToDateByDayAndMonth:scheduleDate]) {
                [programs addObject:program];
                break;
            }
        }
    }
    return programs;
}

- (NSArray <PRVScheduleServerModel *> *)schedulesForDate:(NSDate *)date{
    NSMutableArray <PRVScheduleServerModel *> *schedules = [[NSMutableArray alloc] init];
    for (PRVProgramServerModel *program in self.programs) {
        for (PRVScheduleServerModel *schedule in program.schedule) {
            NSDate *scheduleDate = [NSDate dateFromStringDate:schedule.date withDateFormat:@"d.MM.y"];
            if ([date isEqualToDateByDayAndMonth:scheduleDate]) {
                for (PRVHourServerModel *hour in schedule.hours) {
                    PRVScheduleServerModel *copySchedule = [schedule copy];
                    copySchedule.program = program;
                    copySchedule.hours = @[(PRVHourServerModel *)[hour copy]];
                    [schedules addObject:copySchedule];
                }
                break;
            }
        }
    }
    [schedules sortUsingComparator:^NSComparisonResult(PRVScheduleServerModel *a, PRVScheduleServerModel *b) {
        NSDate *first =  [NSDate dateFromStringDate:a.hours[0].from withDateFormat:@"HH:mm"];
        NSDate *second =  [NSDate dateFromStringDate:b.hours[0].from withDateFormat:@"HH:mm"];
        return [first compare:second];
    }];
    return schedules;
}

@end


@implementation PRVProgramServerModel

+ (EKObjectMapping *)objectMapping{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"name", @"images", @"free"]];
        [mapping mapKeyPath:@"description" toProperty:@"_description"];
        [mapping mapKeyPath:@"id" toProperty:@"_id"];
        [mapping hasMany:[PRVScheduleServerModel class] forKeyPath:@"schedule" forProperty:@"schedule"];
    }];
}

- (NSInteger)minimalPrice{
    NSInteger price = 0;
    for (PRVScheduleServerModel *shedule in self.schedule) {
        if (shedule.price) {
            return shedule.price;
        }
    }
    return price;
}

- (NSInteger)duration{
    for (PRVScheduleServerModel *shedule in self.schedule) {
        for (PRVHourServerModel *hour in shedule.hours) {
            if (hour.duration) {
                return hour.duration;
            }
        }
    }
    return 0;
}

- (NSInteger)price{
    for (PRVScheduleServerModel *shedule in self.schedule) {
        if (shedule.price) {
            return shedule.price;
        }
    }
    return 0;
}

- (PRVScheduleServerModel *)scheduleForDate:(NSDate *)date{
    NSDateFormatter *dateformatter = [[NSDateFormatter alloc] init];
    dateformatter.dateFormat = @"d.MM.y";
    NSCalendar * calendar = [NSCalendar currentCalendar];
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay;
    NSDateComponents *comp1 = [calendar components:unitFlags fromDate:date];
    for (PRVScheduleServerModel *schedule in self.schedule) {
        NSDate *scheduleDate = [dateformatter dateFromString:schedule.date];
        NSDateComponents *comp2 = [calendar components:unitFlags fromDate:scheduleDate];
        if (comp1.year == comp2.year && comp1.month == comp2.month && comp1.day == comp2.day) {
            return schedule;
            break;
        }
    }
    return nil;
}

@end


@implementation PRVScheduleServerModel

+ (EKObjectMapping *)objectMapping{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"date", @"week", @"price", @"free"]];
        [mapping mapKeyPath:@"id" toProperty:@"_id"];
        [mapping hasMany:[PRVHourServerModel class] forKeyPath:@"hours" forProperty:@"hours"];
        [mapping hasMany:[PRVTimeServerModel class] forKeyPath:@"time" forProperty:@"times"];
    }];
}

- (id)copyWithZone:(NSZone *)zone{
    PRVScheduleServerModel *newSchedule = [[PRVScheduleServerModel alloc] init];
    newSchedule.date = self.date;
    newSchedule.week = self.week;
    newSchedule.price = self.price;
    newSchedule.hours = self.hours;
    newSchedule.times = self.times;
    return newSchedule;
}

- (NSString *)formattedDay{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"d.MM.y";
    
    NSDate *date = [formatter dateFromString:self.date];
    NSDateFormatter *dayNameFormatter = [[NSDateFormatter alloc] init];
    [dayNameFormatter setDateFormat:@"EE"];
    return [[dayNameFormatter stringFromDate:date] uppercaseString];
}

- (NSString *)formattedData{
    NSDateFormatter *dateformatter = [[NSDateFormatter alloc] init];
    dateformatter.dateFormat = @"d.MM.y";
    NSDate *date = [dateformatter dateFromString:self.date];

    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"d.MM";
    return [formatter stringFromDate:date];
}


- (NSString *)formattedTime{
    NSMutableArray <NSString *> *times = [[NSMutableArray alloc] init];
    for (PRVHourServerModel *time in self.hours) {
        [times addObject:time.from];
    }
    if (times.count > 2) {
        return [NSString stringWithFormat:@"%@...", [[times subarrayWithRange:NSMakeRange(0, 2)]componentsJoinedByString:@","]];
    } else {
        return [times componentsJoinedByString:@","];
    }
}

- (NSString *)formattedFreeSchedule{
    NSMutableArray <NSString *> *times = [[NSMutableArray alloc] init];
    for (PRVHourServerModel *time in self.hours) {
        NSString *timeString = [NSString stringWithFormat:@"%@-%@", time.from, time.to];
        [times addObject:timeString];
    }
    if (times.count > 2) {
        return [NSString stringWithFormat:@"%@...", [[times subarrayWithRange:NSMakeRange(0, 2)]componentsJoinedByString:@";"]];
    } else {
        return [times componentsJoinedByString:@";"];
    }
}

@end


@implementation PRVHourServerModel

- (id)copyWithZone:(NSZone *)zone{
    PRVHourServerModel *newHour = [[PRVHourServerModel alloc] init];
    newHour._id = self._id;
    newHour.from = self.from;
    newHour.to = self.to;
    newHour.duration = self.duration;
    newHour.places = self.places;
    newHour.coachId = self.coachId;
    return newHour;
}

+ (EKObjectMapping *)objectMapping{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"from", @"to", @"places", @"duration", @"coachId"]];
        [mapping mapKeyPath:@"id" toProperty:@"_id"];
    }];
}

@end


@implementation PRVTimeServerModel

- (id)copyWithZone:(NSZone *)zone{
    PRVTimeServerModel *newTime = [[PRVTimeServerModel alloc] init];
    newTime.flag = self.flag;
    newTime.value = self.value;
    return newTime;
}

+ (EKObjectMapping *)objectMapping{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"flag", @"value"]];
    }];
}

- (BOOL)canEnrollForDate:(NSString *)date{
    // check if enroll time is later than current time
    NSArray *times = [_value componentsSeparatedByString:@" - "];
    // if we got time_from and time_to
    if (times.count == 2){
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"d.MM.y HH:mm";
        NSString *stringDate = [NSString stringWithFormat:@"%@ %@", date, times[0]];
        NSDate *scheduleDate = [formatter dateFromString:stringDate];
        scheduleDate = [scheduleDate dateByAddingTimeInterval:-60 * 60];
        if ([[NSDate date] compare:scheduleDate] == NSOrderedDescending) {
            return NO;
        }
    }
    return !_flag;
}

@end

@implementation PRVCityServerModel

+ (EKObjectMapping *)objectMapping{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"name", @"regionId"]];
        [mapping mapKeyPath:@"id" toProperty:@"_id"];
    }];
}

- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.name forKey:@"name"];
    [aCoder encodeInteger:self.regionId forKey:@"regionId"];
    [aCoder encodeInteger:self._id forKey:@"_id"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        self.name = [decoder decodeObjectForKey:@"name"];
        self._id = [decoder decodeIntegerForKey:@"_id"];
        self.regionId = [decoder decodeIntegerForKey:@"regionId"];
    }
    return self;
}

@end

@implementation PRVPriceServerModel

+ (EKObjectMapping *)objectMapping{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"text", @"value"]];
    }];
}

@end

