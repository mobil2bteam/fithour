//
//  PRVRatingViewController.m
//  FitnessApp
//
//  Created by Ruslan on 11/2/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVRatingViewController.h"
#import "StarRatingView.h"
#import "PRVSupportViewController.h"

@interface PRVRatingViewController ()
@property (weak, nonatomic) IBOutlet UIView *ratingView;
@property (strong, nonatomic) StarRatingView *starview;
@end

@implementation PRVRatingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Оценить приложение";
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.starview = [[StarRatingView alloc]initWithFrame:self.ratingView.bounds andRating:100 withLabel:NO animated:NO];
    self.starview.animated = NO;
    [self.starview ratingDidChange];
    [self.ratingView addSubview:self.starview];
}

- (BOOL)hidesBottomBarWhenPushed
{
    return YES;
}

- (IBAction)ratingButtonPressed:(id)sender {
    PRVSupportViewController *vc = [[PRVSupportViewController alloc] prv_initWithNib];
    vc.isFeedback = YES;
    vc.rating = self.starview.rating / 20;
    [self.navigationController pushViewController:vc animated:YES];
    /*
    if (self.starview.rating < 80) {
        PRVSupportViewController *vc = [[PRVSupportViewController alloc] prv_initWithNib];
        vc.isFeedback = YES;
        vc.rating = self.starview.rating / 20;
        [self.navigationController pushViewController:vc animated:YES];
    } else {
        NSString *str = @"https://itunes.apple.com/us/app/fithour/id1339211390?l=ru&ls=1&mt=8";
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: str] options:@{} completionHandler:nil];
    }
     */
}

@end
