//
//  PRVEnrollmentView.m
//  FitnessApp
//
//  Created by Ruslan on 10/26/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVEnrollmentView.h"
#import "PRVDataSource.h"
#import "PRVClubsServerModel.h"
#import "PRVOptionsServerModel.h"
#import "PRVDirectionCollectionViewCell.h"

@interface PRVEnrollmentView()<UICollectionViewDelegate>

@property (nonatomic, strong) PRVDataSource *groupsCollectionViewDataSource;
@property (weak, nonatomic) IBOutlet UIView *groupView;
@property (nonatomic, weak) IBOutlet UIView *groupsView;
@property (weak, nonatomic) IBOutlet UIView *freeView;

@property (nonatomic, strong) IBOutlet UIView *contentView;
@property (nonatomic, weak) IBOutlet UILabel *enrollPriceLabel;
@property (nonatomic, weak) IBOutlet UIButton *groupButton;
@property (nonatomic, weak) IBOutlet UILabel *programLabel;
//@property (nonatomic, weak) IBOutlet UIView *coachView;
@property (nonatomic, weak) IBOutlet UIView *freeScheduleView;
@property (weak, nonatomic) IBOutlet UIView *onlyFreeScheduleView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *groupsCollectionViewWidthConstraint;
@property (nonatomic, weak) IBOutlet UICollectionView *groupsCollectionView;
@property (nonatomic, weak) IBOutlet UIView *programView;
@property (nonatomic, strong) PRVClubServerModel *club;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *coachViewWidthConstratint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *freeScheduleViewWidthConstratint;
@property (weak, nonatomic) IBOutlet UILabel *freeScheduleLabel;
@property (weak, nonatomic) IBOutlet UILabel *coachLabel;
@property (weak, nonatomic) IBOutlet UIStackView *groupsStackView;

@end

static NSString * const kGroupCollectionViewCellIdentifier = @"PRVDirectionCollectionViewCell";

@implementation PRVEnrollmentView

#pragma mark - Setup

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup{
    [[NSBundle mainBundle]loadNibNamed:@"PRVEnrollmentView" owner:self options:nil];
    [self addSubview:_contentView];
    _contentView.frame = self.bounds;
    [self setupGroupsCollectionViewDataSource];
    [self addObserver:self forKeyPath:@"groupsCollectionView.contentSize" options:NSKeyValueObservingOptionNew context:nil];
}

-(void)dealloc{
    [self removeObserver:self forKeyPath:@"groupsCollectionView.contentSize"];
}

#pragma mark - Observation

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    if ([keyPath isEqualToString:@"groupsCollectionView.contentSize"]) {
        CGFloat newWidth = self.groupsCollectionView.collectionViewLayout.collectionViewContentSize.width;
        self.groupsCollectionViewWidthConstraint.constant = newWidth;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self layoutIfNeeded];
        });
    }
}

#pragma mark - Preparation

- (void)configureViewFor:(PRVClubServerModel *)club{
    self.club = club;
    if (!club.first_free) {
        [self.freeView removeFromSuperview];
    }
    // обнулить все данные
    self.groupsStackView.spacing = 1.f;
    self.freeScheduleLabel.text = @"Свободная\nзона";
    self.coachLabel.text = @"Записаться к\nтренеру";

    self.coachViewWidthConstratint.priority = 250;
    self.freeScheduleViewWidthConstratint.priority = 250;
    [self layoutIfNeeded];

    self.enrollPriceLabel.text = [NSString stringWithFormat:@"%ld", (long)[self.club minimalPriceForGroups]];
    [self.groupsCollectionViewDataSource setItems:[_club groupsWithoutFreeSchedule]];
    [self.groupsCollectionView reloadSections:[NSIndexSet indexSetWithIndex:0]];
    
    // hide all
    for (UIView *view in @[self.freeScheduleView, self.programView,  self.groupView, self.groupsView, self.onlyFreeScheduleView]) {
        view.hidden = YES;
    }
    
    if ([self.club areAllGroupsWithFreeSchdeule]) {
        self.groupsView.hidden = NO;
        self.enrollPriceLabel.text = [NSString stringWithFormat:@"%ld", (long)self.club.price_free];
        self.onlyFreeScheduleView.hidden = NO;
        return;
    }

    // если у клуба есть группы со свободным посещением, то показываем свободное посещение, кнопку тренера и другие группы
    if ([self.club hasGroupsWithFreeSchdeule]) {
        self.groupsView.hidden = NO;
        self.freeScheduleView.hidden = NO;
        if (self.club.coaches.count) {
         //   self.coachView.hidden = NO;
        }
        if ([self.club areAllGroupsWithFreeSchdeule]) {
            self.groupsStackView.spacing = 0.f;
            self.freeScheduleLabel.text = @"Расписание свободной зоны";
            self.coachLabel.text = @"Записаться к тренеру";
            self.enrollPriceLabel.text = [NSString stringWithFormat:@"%ld", (long)self.club.price_free];
            CGFloat width = CGRectGetWidth(self.frame) / 2;
            self.coachViewWidthConstratint.constant = width;
            self.freeScheduleViewWidthConstratint.constant = width;
            self.coachViewWidthConstratint.priority = 900;
            self.freeScheduleViewWidthConstratint.priority = 900;
            [self layoutIfNeeded];
        }
        return;
    }
    
    // если у клуба только одна группа, то показываем только эту группу. Если у этой группы одна программа, то показываем группу и программу
    if (self.club.groups.count == 1) {
        [self.groupButton setTitle:self.club.groups[0].name forState:UIControlStateNormal];
        self.groupView.hidden = NO;
        if (self.club.groups[0].programs.count == 1) {
            self.programLabel.text = self.club.groups[0].programs[0].name;
            self.programView.hidden = NO;
        }
        return;
    }
    // иначе показываются все группы
    self.groupsView.hidden = NO;
}


- (void)setupGroupsCollectionViewDataSource{
    PRVCellConfigureBlock configureCell = ^(PRVDirectionCollectionViewCell *cell, PRVGroupServerModel *group) {
        [cell configureCellForGroup:group];
        cell.layer.cornerRadius = 15;
    };
    self.groupsCollectionViewDataSource = [[PRVDataSource alloc] initWithItems:[self.club groupsWithoutFreeSchedule] cellIdentifier:kGroupCollectionViewCellIdentifier configureCellBlock:configureCell];
    self.groupsCollectionView.dataSource = self.groupsCollectionViewDataSource;
    self.groupsCollectionView.delegate = self;
    [self.groupsCollectionView registerNib:[PRVDirectionCollectionViewCell prv_nib] forCellWithReuseIdentifier:kGroupCollectionViewCellIdentifier];
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    PRVGroupServerModel *group = [self.groupsCollectionViewDataSource itemAtIndexPath:indexPath];
    if (self.delegate && [self.delegate respondsToSelector:@selector(enrollmentView:didSelectGroup:)]) {
        [self.delegate enrollmentView:self didSelectGroup:group];
    }
    [collectionView deselectItemAtIndexPath:indexPath animated:NO];
}

#pragma mark - Actions

- (IBAction)enrollButtonPressed:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(enrollSelected:)]) {
        [self.delegate enrollSelected:self];
    }
}

- (IBAction)coachEnrollButtonPressed:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(coachSelected:)]) {
        [self.delegate coachSelected:self];
    }
}

- (IBAction)freeScheduleButtonPressed:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(freeScheduleSelected:)]) {
        [self.delegate freeScheduleSelected:self];
    }
}

- (IBAction)programButtonPressed:(id)sender {
    PRVGroupServerModel *group = self.club.groups[0];
    if (self.delegate && [self.delegate respondsToSelector:@selector(enrollmentView:didSelectGroup:)]) {
        [self.delegate enrollmentView:self didSelectGroup:group];
    }
}

@end
