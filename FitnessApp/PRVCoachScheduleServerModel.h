//
//  PRVCoachScheduleServerModel.h
//  FitnessApp
//
//  Created by Ruslan on 9/18/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EasyMapping/EasyMapping.h"

@class PRVScheduleServerModel;

@interface PRVCoachScheduleServerModel : NSObject <EKMappingProtocol>
@property (nonatomic, assign) NSInteger coachId;
@property (nonatomic, strong) NSArray<PRVScheduleServerModel *> *items;
- (NSArray <NSArray<PRVScheduleServerModel *> *> *)splitScheduleByWeeks;
+ (EKObjectMapping *)objectMapping;
@end
