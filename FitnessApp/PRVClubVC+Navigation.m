//
//  PRVClubVC+Navigation.m
//  FitnessApp
//
//  Created by Ruslan on 8/27/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//
#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

#import "PRVClubVC+Navigation.h"
#import "PRVClubsServerModel.h"
#import "PRVOptionsServerModel.h"
#import "PRVGroupListVC.h"
#import "PRVCoachesListVC.h"
#import "PRVProgramDetailVC.h"
#import "PRVProgramListVC.h"
#import "PRVPhoneListAlertVC.h"
#import <SafariServices/SafariServices.h>
#import "PRVSelectDayVC.h"
#import "PRVAddReviewVC.h"
#import "PRVClubDetailVC.h"

@implementation PRVClubVC (Navigation)

- (void)showGroupListViewController{
    PRVGroupListVC *vc = [[PRVGroupListVC alloc] prv_initWithNib];
    vc.club = self.club;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)showProgramListViewControllerWithFreeSchedule{
    PRVProgramListVC *vc = [[PRVProgramListVC alloc] prv_initWithNib];
    vc.club = self.club;
    vc.isFreeSchedule = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)showCoachListViewController{
    PRVCoachesListVC *vc = [[PRVCoachesListVC alloc] prv_initWithNib];
    vc.club = self.club;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)showCoachListViewControllerForPersonalTraining{
    PRVCoachesListVC *vc = [[PRVCoachesListVC alloc] prv_initWithNib];
    vc.club = self.club;
    vc.isPersonalTraining = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)showSafariViewControllerWithUrl:(NSString *)urlString{
    NSURL *url = [NSURL URLWithString:urlString];
    if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0")){
        [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
    } else {
        [[UIApplication sharedApplication] openURL:url];
    }
}

- (void)showProgramListViewControllerForGroup:(PRVGroupServerModel *)group{
    if (group.programs.count == 1) {
        PRVProgramDetailVC *vc = [[PRVProgramDetailVC alloc] prv_initWithNib];
        vc.group = group;
        vc.club = self.club;
        vc.program = group.programs[0];
        [self.navigationController pushViewController:vc animated:YES];
    }
    if (group.programs.count > 1) {
        PRVProgramListVC *vc = [[PRVProgramListVC alloc] prv_initWithNib];
        vc.group = group;
        vc.club = self.club;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (void)showPhoneListViewControllerForPhones:(NSArray <PRVPhoneServerModel *> *)phones phoneBlock:(void (^)(NSString *phoneNumber))phoneBlock{
    PRVPhoneListAlertVC *vc = [[PRVPhoneListAlertVC alloc] prv_initWithNib];
    vc.phones = phones;
    vc.phoneBlock = phoneBlock;
    vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:vc animated:YES completion:nil];
}

- (void)showSelectDayViewController{
    PRVSelectDayVC *vc = [[PRVSelectDayVC alloc] prv_initWithNib];
    vc.club = self.club;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)showClubDetailViewController{
    PRVClubDetailVC *vc = [[PRVClubDetailVC alloc] prv_initWithNib];
    vc.club = self.club;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)showAddReviewViewController{
    __weak typeof(self) weakSelf = self;
    PRVAddReviewVC *vc = [[PRVAddReviewVC alloc] prv_initWithNib];
    vc.clubId = self.club._id;
    vc.reviewBlock = ^(PRVReviewServerModel *review){
        __weak typeof(self) strongSelf = weakSelf;
        strongSelf.club.user_review = review;
    };
    [self.navigationController pushViewController:vc animated:YES];
}

@end
