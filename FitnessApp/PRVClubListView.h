//
//  PRVClubListView.h
//  FitnessApp
//
//  Created by Ruslan on 7/25/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

@class PRVClubListView;

#import "PRVClubCollectionViewCell.h"

@protocol PRVClubListViewDelegate <NSObject>

@optional;
- (void)clubListView:(PRVClubListView *)clubListView didSelectClubAtIndex:(NSInteger)index;
- (void)loadMore:(PRVClubListView *)clubListView;

@end


#import <UIKit/UIKit.h>
#import "PRVDataSource.h"

@interface PRVClubListView : UIView

@property (nonatomic, weak) id <PRVClubListViewDelegate> delegate;
@property (nonatomic, strong) PRVDataSource *dataSource;

- (void)reloadData;
- (void)clear;
- (void)setLoadMoreEnabled:(BOOL)enabled;

@end
