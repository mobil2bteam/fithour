//
//  PRVGroupListVC+Setup.h
//  FitnessApp
//
//  Created by Ruslan on 8/28/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVGroupListVC.h"

@interface PRVGroupListVC (Setup)

- (void)setupGroupTableView;

@end
