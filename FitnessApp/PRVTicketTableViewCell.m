//
//  PRVTicketTableViewCell.m
//  FitnessApp
//
//  Created by Ruslan on 8/18/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVTicketTableViewCell.h"
#import "UIView+PRVExtensions.h"

@implementation PRVTicketTableViewCell

- (void)prepareForReuse{
    self.clubLabel.text = @"";
    self.addressLabel.text = @"";
    self.timeLabel.text = @"";
    self.programLabel.text = @"";
    self.logoImageView.image = [UIImage imageNamed:@"Timesport_logo"];
    self.coachImageView.image = [UIImage imageNamed:@"trainer_stock_icon"];
    self.coachImageView.hidden = YES;
    self.logoImageView.hidden = YES;
}

@end
