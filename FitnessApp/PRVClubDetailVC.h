//
//  PRVClubDetailVC.h
//  FitnessApp
//
//  Created by Ruslan on 22.11.2017.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PRVClubServerModel;

@interface PRVClubDetailVC : UIViewController
@property (nonatomic, strong) PRVClubServerModel *club;
@end
