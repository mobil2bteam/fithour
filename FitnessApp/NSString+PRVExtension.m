//
//  NSString+PRVExtension.m
//  FitnessApp
//
//  Created by Ruslan on 7/11/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "NSString+PRVExtension.h"

@implementation NSString (PRVExtension)

- (NSDictionary *)prv_dictionaryValue{
    NSData *data = [self dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data
                                                                 options:kNilOptions
                                                                   error:nil];
    return jsonResponse;
}

@end
