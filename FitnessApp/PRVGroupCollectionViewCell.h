//
//  PRVGroupCollectionViewCell.h
//  FitnessApp
//
//  Created by Ruslan on 7/23/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PRVGroupCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *groupImageView;

@property (weak, nonatomic) IBOutlet UILabel *groupLabel;

@end
