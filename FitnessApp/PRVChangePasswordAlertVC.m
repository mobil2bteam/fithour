//
//  PRVChangePasswordAlertVC.m
//  FitnessApp
//
//  Created by Ruslan on 9/15/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVChangePasswordAlertVC.h"
#import "UIView+PRVExtensions.h"
#import "PRVServerManager.h"
#import "UIViewController+PRVExtensions.h"

@interface PRVChangePasswordAlertVC ()

@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTextField;

@end

@implementation PRVChangePasswordAlertVC

#pragma mark - Actions

- (IBAction)cancelButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)changePasswordButtonPressed:(id)sender {
    [self.view endEditing:YES];
    if (!self.passwordTextField.text.length) {
        [self.passwordTextField prv_shake];
        return;
    }
    if (!self.confirmPasswordTextField.text.length) {
        [self.confirmPasswordTextField prv_shake];
        return;
    }
    
    if (![self.passwordTextField.text isEqualToString:self.confirmPasswordTextField.text]) {
        [self prv_showMessage:@"Пароли не совпадают" withTitle:nil];
        return;
    }
    
    [self prv_addMBProgressHUD];
    [[PRVServerManager sharedManager] changePasswordWithNewPassword:self.passwordTextField.text onSuccess:^(PRVUserServerModel *user, NSString *error) {
        [self prv_hideMBProgressHUD];
        if (error) {
            [self prv_showMessage:error withTitle:nil];
        } else {
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [self prv_hideMBProgressHUD];
        [self prv_showMessage:error.localizedDescription withTitle:nil];
    }];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == self.passwordTextField) {
        [self.confirmPasswordTextField becomeFirstResponder];
    }
    [textField resignFirstResponder];
    return YES;
}

@end
