//
//  PRVSupportViewController.m
//  FitnessApp
//
//  Created by Ruslan on 11/2/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVSupportViewController.h"
#import "PRVServerManager.h"
#import "UIView+PRVExtensions.h"
#import "UIViewController+PRVExtensions.h"
#import "PRVAppManager.h"
#import "PRVUserServerModel.h"

@interface PRVSupportViewController ()

@property (weak, nonatomic) IBOutlet UIView *nameView;

@property (weak, nonatomic) IBOutlet UIView *emailView;

@property (weak, nonatomic) IBOutlet UIView *feedbackView;

@property (weak, nonatomic) IBOutlet UILabel *textLabel;

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;

@property (weak, nonatomic) IBOutlet UITextView *feedbackTextView;

@end

@implementation PRVSupportViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    PRVUserServerModel *user = [[PRVAppManager sharedInstance] getUser];
    if (user) {
        self.nameTextField.text = user.info.name;
        self.emailTextField.text = user.info.email;
    }
    if (self.isFeedback) {
        self.textLabel.text = @"Отзыв о приложении";
        self.navigationItem.title = @"Отзыв";
    } else {
        self.textLabel.text = @"Обращение";
        self.title = @"Обращение в тех.поддержку";
    }
    for (UIView *view in @[self.nameView, self.feedbackView, self.emailView]) {
        view.layer.borderWidth = 1.0;
        view.layer.cornerRadius = 4;
        view.layer.masksToBounds = YES;
        view.layer.borderColor = [UIColor lightGrayColor].CGColor;
    }
}

#pragma mark - Actions

- (IBAction)sendButtonPressed:(id)sender {
    if (!self.nameTextField.text.length) {
        [self.nameTextField prv_shake];
        return;
    }
    if (!self.emailTextField.text.length) {
        [self.emailTextField prv_shake];
        return;
    }
    if (!self.feedbackTextView.text.length) {
        [self.feedbackView prv_shake];
        return;
    }
    NSString *name = self.nameTextField.text;
    NSString *email = self.emailTextField.text;
    NSString *text = self.feedbackTextView.text;
    if (self.isFeedback) {
        __weak typeof(self) weakSelf = self;
        [self prv_addMBProgressHUD];
        [[PRVServerManager sharedManager] rateAppWith:self.rating name:name email:email text:text onSuccess:^{
            typeof(weakSelf) strongSelf = weakSelf;
            [strongSelf prv_hideMBProgressHUD];
            [strongSelf prv_showMessage:@"Ваша оценка отправлена" withTitle:nil];
        } onFailure:^(NSError *error, NSInteger statusCode) {
            typeof(weakSelf) strongSelf = weakSelf;
            [strongSelf prv_hideMBProgressHUD];
            [strongSelf prv_showMessage:error.localizedDescription withTitle:nil];
        }];
    } else {
        __weak typeof(self) weakSelf = self;
        [self prv_addMBProgressHUD];
        [[PRVServerManager sharedManager] supportWith:name email:email text:text onSuccess:^{
            typeof(weakSelf) strongSelf = weakSelf;
            [strongSelf prv_hideMBProgressHUD];
            [strongSelf prv_showMessage:@"Ваше обращение отправлено" withTitle:nil];
        } onFailure:^(NSError *error, NSInteger statusCode) {
            typeof(weakSelf) strongSelf = weakSelf;
            [strongSelf prv_hideMBProgressHUD];
            [strongSelf prv_showMessage:error.localizedDescription withTitle:nil];
        }];
    }
}

- (BOOL)hidesBottomBarWhenPushed
{
    return YES;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

@end
