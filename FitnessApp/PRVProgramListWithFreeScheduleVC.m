//
//  PRVProgramListWithFreeScheduleVC.m
//  FitnessApp
//
//  Created by Ruslan on 26.11.2017.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVProgramListWithFreeScheduleVC.h"
#import "PRVDayCollectionViewCell+Configuration.h"
#import "PRVDataSource.h"
#import "PRVOptionsServerModel.h"
#import "PRVFreeScheduleProgramTableViewCell.h"
#import "PRVProgramFreeScheduleDetailVC.h"
#import "PRVProgramConfirmVC.h"
#import "PRVClubsServerModel.h"
#import "PRVProgramFreeScheduleDetailVC.h"
#import "PRVSelectTimeVC.h"

@interface PRVProgramListWithFreeScheduleVC () <UICollectionViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *programsTableView;
@property (nonatomic, strong) NSArray <NSDate *> *dates;
@property (nonatomic, strong) NSDate *selectedDate;
@property (weak, nonatomic) IBOutlet UIButton *enrollButton;
@end

static NSString * const kDayCollectionViewCellIdentifier = @"PRVDayCollectionViewCell";
static NSString * const kProgramTableViewCellIdentifier = @"PRVFreeScheduleProgramTableViewCell";


@implementation PRVProgramListWithFreeScheduleVC

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupDaysCollectionView];
    self.selectedDate = [self dates][0];
    [self.programsTableView registerNib:[PRVFreeScheduleProgramTableViewCell prv_nib] forCellReuseIdentifier:kProgramTableViewCellIdentifier];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.title = @"Программы";
}

#pragma mark - Getters

- (NSArray <NSDate *> *)dates{
    if (_dates) {
        return _dates;
    }
    NSMutableArray <NSDate *> *dates = [[NSMutableArray alloc] init];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"d.MM.y";
    for (NSInteger i = 0; i < 30; i++) {
        NSDate *today = [NSDate date];
        NSDate *currentDate = [today dateByAddingTimeInterval:24 * 60 * 60 * i];
        [dates addObject:currentDate];
    }
    _dates = [dates copy];
    return _dates;
}

- (NSArray <PRVScheduleServerModel *> *)schedules{
    return [self.group schedulesForDate:self.selectedDate];
}


-(void)setSelectedDate:(NSDate *)selectedDate{
    _selectedDate = selectedDate;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"d.MM.y";
    NSString *title = [NSString stringWithFormat:@"ЗАПИСАТЬСЯ НА %@", [formatter stringFromDate:_selectedDate]];
    [_enrollButton setTitle:title forState:UIControlStateNormal];
}

- (IBAction)enrollButtonPressed:(id)sender {
    PRVSelectTimeVC *vc = [[PRVSelectTimeVC alloc] prv_initWithNib];
    vc.club = self.club;
    vc.selectedDate = self.selectedDate;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Setup

- (void)setupDaysCollectionView{
    PRVCellConfigureBlock configureCell = ^(PRVDayCollectionViewCell *cell, NSDate *date) {
        [cell configureCellForDate:date];
        if (date == self.selectedDate) {
            [cell setSelected:YES];
        }
    };
    ((UICollectionViewFlowLayout *)self.daysCollectionView.collectionViewLayout).estimatedItemSize = CGSizeMake(60, 60);
    self.daysCollectionViewDataSource = [[PRVDataSource alloc] initWithItems:[self dates] cellIdentifier:kDayCollectionViewCellIdentifier configureCellBlock:configureCell];
    self.daysCollectionView.dataSource = self.daysCollectionViewDataSource;
    [self.daysCollectionView registerNib:[PRVDayCollectionViewCell prv_nib] forCellWithReuseIdentifier:kDayCollectionViewCellIdentifier];
    [self.daysCollectionView reloadData];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self schedules].count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PRVFreeScheduleProgramTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kProgramTableViewCellIdentifier forIndexPath:indexPath];
    PRVScheduleServerModel *schedule = [self schedules][indexPath.row];
    PRVProgramServerModel *program = schedule.program;
    PRVHourServerModel *hour = schedule.hours[0];
    cell.hoursLabel.text = [NSString stringWithFormat:@"%@ - %@", hour.from, hour.to];
    cell.programLabel.text = program.name;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    PRVProgramFreeScheduleDetailVC *vc = [[PRVProgramFreeScheduleDetailVC alloc] prv_initWithNib];
    PRVScheduleServerModel *schedule = [self schedules][indexPath.row];
    PRVProgramServerModel *program = schedule.program;
    vc.club = self.club;
    vc.group = self.group;
    vc.program = program;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    self.selectedDate = self.dates[indexPath.row];
    [self.daysCollectionView reloadData];
    [self.programsTableView reloadData];
}

@end
