//
//  PRVProgramDetailVC+Navigation.m
//  FitnessApp
//
//  Created by Ruslan on 8/28/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVProgramDetailVC+Navigation.h"
#import "PRVOptionsServerModel.h"
#import "PRVProgramConfirmVC.h"

@implementation PRVProgramDetailVC (Navigation)

- (void)showProgramConfirmViewControllerWithHour:(PRVHourServerModel *)hour{
    if (!hour.places) {
        UIAlertController *vc = [UIAlertController prv_alertWithTitle:@"Ошибка" message:@"На это время не осталось свободных мест" cancelButtonTitle:@"Ок" cancelBlock:nil];
        [self presentViewController:vc animated:YES completion:nil];
    } else {
        PRVProgramConfirmVC *vc = [[PRVProgramConfirmVC alloc] prv_initWithNib];
        vc.club = self.club;
        vc.program = self.program;
        vc.date = self.selectedDay.date;
        vc.hour = hour;
        vc.group = self.group;
        vc.price = self.selectedDay.price;
        vc.enrollType = PRVEnrollTicketTypeDefault;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

@end
