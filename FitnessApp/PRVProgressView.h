//
//  PRVProgressView.h
//  FitnessApp
//
//  Created by Ruslan on 9/25/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class PRVTicketSeverModel;

@interface PRVProgressView : UIView

- (void)prepareViewForTicket:(PRVTicketSeverModel *)ticket;

@end
