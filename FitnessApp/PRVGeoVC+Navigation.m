//
//  PRVGeoVC+Navigation.m
//  FitnessApp
//
//  Created by Ruslan on 8/27/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVGeoVC+Navigation.h"
#import "PRVViewController.h"

@implementation PRVGeoVC (Navigation)

- (void)showMainViewController{
    PRVViewController *vc = [[PRVViewController alloc] init];
    [self presentViewController:vc animated:NO completion:nil];
}

@end
