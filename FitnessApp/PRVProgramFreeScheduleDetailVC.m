//
//  PRVProgramFreeScheduleDetailVC.m
//  FitnessApp
//
//  Created by Ruslan on 9/12/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVProgramFreeScheduleDetailVC.h"
#import "PRVClubInfoView.h"
#import "PRVClubsServerModel.h"
#import "PRVOptionsServerModel.h"
#import "PRVDataSource.h"
#import <NSAttributedString-DDHTML/NSAttributedString+DDHTML.h>
#import "PRVFreeScheduleTableViewCell.h"
#import "PRVProgramConfirmVC.h"
#import "PRVImagePager.h"
#import "UILabel+PRVExtension.h"

static NSString * const kTableViewCellIdentifier = @"PRVFreeScheduleTableViewCell";

@interface PRVProgramFreeScheduleDetailVC () <PRVImagePagerDataSource>
@property (weak, nonatomic) IBOutlet PRVClubInfoView *clubInfoView;
@property (weak, nonatomic) IBOutlet PRVImagePager *sliderView;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (nonatomic, strong) PRVDataSource *sliderDataSource;
@end

@implementation PRVProgramFreeScheduleDetailVC

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.clubInfoView setClub:self.club];
    self.title = self.program.name;
    if (self.program.images.count) {
        self.sliderView.dataSource = self;
        self.sliderView.imageCounterDisabled = YES;
    } else {
        self.sliderView.hidden = YES;
    }
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.descriptionLabel setHTML:self.program._description];
}

#pragma mark - PRVImagePagerDataSource

- (NSArray *) arrayWithImages:(PRVImagePager*)pager{
    return self.program.images;
}

- (UIViewContentMode) contentModeForImage:(NSUInteger)image inPager:(PRVImagePager*)pager{
    return UIViewContentModeScaleAspectFill;
}

@end

