//
//  PRVGroupListVC+Setup.m
//  FitnessApp
//
//  Created by Ruslan on 8/28/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVGroupListVC+Setup.h"
#import "PRVOptionsServerModel.h"
#import "PRVDataSource.h"
#import "PRVClubsServerModel.h"
#import "UIImageView+PRVExtension.h"
#import "PRVGroupTableViewCell+Configuration.h"

static NSString * const kGroupTableViewCellIdentifier = @"PRVGroupTableViewCell";

@implementation PRVGroupListVC (Setup)

- (void)setupGroupTableView{
    PRVCellConfigureBlock configureCell = ^(PRVGroupTableViewCell *cell, PRVGroupServerModel *group) {
        [cell configureCellForGroup:group];
    };
    self.groupsTableView.rowHeight = UITableViewAutomaticDimension;
    self.groupsTableView.estimatedRowHeight = 110.f;
    self.groupTableViewDataSource = [[PRVDataSource alloc] initWithItems:[self.club groupsWithoutFreeSchedule]  cellIdentifier:kGroupTableViewCellIdentifier  configureCellBlock:configureCell];
    self.groupsTableView.dataSource = self.groupTableViewDataSource;
    [self.groupsTableView registerNib:[PRVGroupTableViewCell prv_nib] forCellReuseIdentifier:kGroupTableViewCellIdentifier];
    self.groupsTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

@end
