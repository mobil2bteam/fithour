//
//  PRVPreSignUpStep2VC.h
//  FitnessApp
//
//  Created by Ruslan on 16.07.2018.
//  Copyright © 2018 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PRVPreSignUpStep2VC : UIViewController
@property (nonatomic, copy) void (^loginCallback)(void);
@property (nonatomic, assign) BOOL sighInForBuyTicket;
@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *password;
@end
