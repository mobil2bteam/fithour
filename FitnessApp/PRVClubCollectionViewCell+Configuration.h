//
//  PRVClubCollectionViewCell+Configuration.h
//  FitnessApp
//
//  Created by Ruslan on 8/24/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVClubCollectionViewCell.h"

@class PRVClubServerModel;
@class PRVClubDataModel;

@interface PRVClubCollectionViewCell (Configuration)

- (void)configureCellForClub:(PRVClubServerModel *)club;
- (void)configureCellForFavoriteClub:(PRVClubDataModel *)favoriteClub deleteBlock:(void(^)())deleteBlock;

@end
