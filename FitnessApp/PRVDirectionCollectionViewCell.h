//
//  PRVDirectionCollectionViewCell.h
//  FitnessApp
//
//  Created by Ruslan on 7/12/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//
@class PRVGroupServerModel;

#import <UIKit/UIKit.h>

@interface PRVDirectionCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *gradientImageView;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;

- (void)configureCellForGroup:(PRVGroupServerModel *)group;

@end
