//
//  PRVSelectTimeVC.m
//  FitnessApp
//
//  Created by Ruslan on 9/13/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVSelectTimeVC.h"
#import "PRVSelectTimeVC+Navigation.h"
#import "PRVOptionsServerModel.h"
#import "PRVClubsServerModel.h"
#import "PRVProgramConfirmVC.h"
#import "PRVDataSource.h"
#import "PRVOptionsServerModel.h"
#import "NSDate+PRVComparation.h"

@interface PRVSelectTimeVC ()
@property (weak, nonatomic) IBOutlet UITableView *timesTableView;
@property (nonatomic, strong) NSArray <NSString *> *times;
@property (nonatomic, strong) PRVDataSource *timesTableViewDataSource;
@end

@implementation PRVSelectTimeVC

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Время";
    [self setupTimesTableView];
}

#pragma mark - Methods

- (void)setupTimesTableView{
    PRVCellConfigureBlock configureCell = ^(UITableViewCell *cell, NSString *time) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.text = time;
        cell.textLabel.font = [UIFont fontWithName:@"Rubik-Regular" size:20];
    };
    self.timesTableViewDataSource = [[PRVDataSource alloc] initWithItems:self.times  cellIdentifier:@"cell"  configureCellBlock:configureCell];
    self.timesTableView.dataSource = self.timesTableViewDataSource;
    [self.timesTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    self.timesTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

#pragma mark - Getters

- (NSArray <NSDate *> *)hoursForCurrentDate{
    NSDate *startTime;
    NSDate *endTime;
    for (PRVGroupServerModel *group in self.club.groups) {
        for (PRVProgramServerModel *program in group.programs) {
            if (program.free) {
                for (PRVScheduleServerModel *schedule in program.schedule) {
                    NSDate *scheduleDate = [NSDate dateFromStringDate:schedule.date withDateFormat:@"d.MM.y"];
                    if ([self.selectedDate isEqualToDateByDayAndMonth:scheduleDate]) {
                        for (PRVHourServerModel *hour in schedule.hours) {
                            NSDate *fromHours = [NSDate dateFromStringDate:hour.from withDateFormat:@"HH:mm"];
                            NSDate *toHours = [NSDate dateFromStringDate:hour.to withDateFormat:@"HH:mm"];
                            if (startTime == nil) {
                                startTime = fromHours;
                                endTime = toHours;
                            } else {
                                if ([fromHours compare:startTime] == NSOrderedAscending) {
                                    
                                    startTime = fromHours;
                                }
                                if ([toHours compare:endTime] == NSOrderedDescending) {
                                    endTime = toHours;
                                }
                            }
                            NSLog(@"from - %@", hour.from);
                            NSLog(@"to - %@", hour.to);
                        }
                    }
                }
            }
        }
    }
    
    return @[startTime, endTime];
}

- (NSArray <NSString *> *)times{
    if (_times) {
        return _times;
    }
    NSMutableArray <NSString *> *tempTimes = [[NSMutableArray alloc] init];
//    PRVWorkingHourServerModel *workingHours = [self.club workingHoursForDate:self.selectedDate];
    if ([self hoursForCurrentDate].count > 0) {
        NSDateFormatter *hourFormatter = [[NSDateFormatter alloc] init];
        [hourFormatter setDateFormat:@"HH:mm"];
        NSDate *startTime = [self hoursForCurrentDate][0];
        NSDate *finishTime = [self hoursForCurrentDate][1];
        BOOL today = [[NSCalendar currentCalendar] isDateInToday:self.selectedDate];
        if (today){
            // Check start working time
            NSDateFormatter *hourFormatter = [[NSDateFormatter alloc] init];
            [hourFormatter setDateFormat:@"HH:mm"];
            NSString *currentHour = [hourFormatter stringFromDate:[NSDate date]];
            NSDate *currentHourDate = [hourFormatter dateFromString:currentHour];
            if ([currentHourDate compare:startTime] == NSOrderedDescending) {
                // Add 1 hour to current hour
                startTime = [currentHourDate dateByAddingTimeInterval:60 * 60];
                NSDateFormatter *minutesFormatter = [[NSDateFormatter alloc] init];
                [minutesFormatter setDateFormat:@"mm"];
                NSInteger minutes = [[minutesFormatter stringFromDate:startTime] integerValue];
                if (minutes % 30) {
                    startTime = [startTime dateByAddingTimeInterval:labs((minutes % 30) - 30) * 60];
                }
            }
        }
        while ([finishTime compare:startTime] == NSOrderedDescending || [finishTime compare:startTime] == NSOrderedSame) {
            [tempTimes addObject:[hourFormatter stringFromDate:startTime]];
            startTime = [startTime dateByAddingTimeInterval:30 * 60];
        }
    }
    _times = [tempTimes copy];
    return _times;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self showProgramConfirmViewControllerForDate:self.selectedDate time:[self.timesTableViewDataSource itemAtIndexPath:indexPath]];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

@end
