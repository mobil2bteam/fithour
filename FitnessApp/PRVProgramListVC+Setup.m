//
//  PRVProgramListVC+Setup.m
//  FitnessApp
//
//  Created by Ruslan on 8/28/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVProgramListVC+Setup.h"
#import "PRVDataSource.h"
#import "PRVProgramTableViewCell+Configuration.h"
#import "PRVOptionsServerModel.h"
#import "PRVSectionedDataSource.h"
#import "PRVClubsServerModel.h"
#import "PRVGroupHeaderView+Configuration.h"
#import "PRVGroupTableViewCell+Configuration.h"

static NSString * const kProgramTableViewCellIdentifier = @"PRVProgramTableViewCell";
static NSString * const kGroupTableViewCellIdentifier = @"PRVGroupTableViewCell";
static NSString * const kGroupHeaderViewIdentifier = @"PRVGroupHeaderView";

@implementation PRVProgramListVC (Setup)

- (void)setupTableView{
    self.programsTableView.rowHeight = UITableViewAutomaticDimension;
    self.programsTableView.estimatedRowHeight = 170.f;
    PRVCellConfigureBlock configureCell;
    NSArray *items;
    if (self.isFreeSchedule) {
        items = [self test];
        configureCell = ^(PRVGroupTableViewCell *cell, PRVGroupServerModel *group) {
             [cell configureCellForGroup:group];
        };
    } else {
        items = self.group.programs;
        configureCell = ^(PRVProgramTableViewCell *cell, PRVProgramServerModel *program) {
            [cell configureCellWithProgram:program];
        };
    }
    if (self.isFreeSchedule) {
        [self.programsTableView registerNib:[PRVGroupTableViewCell prv_nib] forCellReuseIdentifier:kGroupTableViewCellIdentifier];
        self.programsDataSource = [[PRVDataSource alloc] initWithItems:items cellIdentifier:kGroupTableViewCellIdentifier configureCellBlock:configureCell];
    } else {
        [self.programsTableView registerNib:[PRVProgramTableViewCell prv_nib] forCellReuseIdentifier:kProgramTableViewCellIdentifier];
        self.programsDataSource = [[PRVDataSource alloc] initWithItems:items cellIdentifier:kProgramTableViewCellIdentifier configureCellBlock:configureCell];
    }
    self.programsTableView.dataSource = self.programsDataSource;
}

- (NSArray *)test{
    NSMutableArray <PRVGroupServerModel *> *test = [[NSMutableArray alloc] init];
    for (PRVGroupServerModel *group in [self.club groupsWithFreeSchedule]) {
        [test addObject:group];
    }
    return [test copy];
}

@end
