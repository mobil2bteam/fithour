//
//  PRVProgramDetailVC+Setup.m
//  FitnessApp
//
//  Created by Ruslan on 8/28/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVProgramDetailVC+Setup.h"
#import "PRVOptionsServerModel.h"
#import "PRVScheduleCollectionViewCell+Configuration.h"
#import "PRVDataSource.h"
#import "PRVClubsServerModel.h"
#import "PRVSectionedDataSource.h"
#import "PRVProgramServerModel+Extension.h"
#import "PRVDayCollectionViewCell+Configuration.h"

static NSString * const kScheduleCollectionViewCellIdentifier = @"PRVScheduleCollectionViewCell";
static NSString * const kDayCollectionViewCellIdentifier = @"PRVDayCollectionViewCell";

@implementation PRVProgramDetailVC (Setup)

- (void)setupScheduleCollectionView{
    __weak typeof(self) weakSelf = self;
    PRVCellConfigureBlock configureCell = ^(PRVScheduleCollectionViewCell *cell, PRVHourServerModel *hour) {
        typeof(self) strongSelf = weakSelf;
        [cell configureCellForHour:hour];
        [cell configureCellForCoach:[strongSelf.club coachById:hour.coachId]];
    };
    self.scheduleCollectionViewDataSource = [[PRVDataSource alloc] initWithItems:self.selectedDay.hours cellIdentifier:kScheduleCollectionViewCellIdentifier configureCellBlock:configureCell];
    self.scheduleCollectionView.dataSource = self.scheduleCollectionViewDataSource;
    [self.scheduleCollectionView registerNib:[PRVScheduleCollectionViewCell prv_nib] forCellWithReuseIdentifier:kScheduleCollectionViewCellIdentifier];
}

- (void)setupDaysCollectionView{
    __weak typeof(self) weakSelf = self;
    PRVSectionedCellConfigureBlock configureCell = ^(PRVDayCollectionViewCell *cell, PRVScheduleServerModel *schedule, NSIndexPath *indexPath) {
        typeof(self) strongSelf = weakSelf;
        [cell configureCellWithSheduleForDate:schedule];        
        cell.backgroundColor = indexPath.section % 2 == 0 ? [UIColor whiteColor] : [UIColor backgroundColor];
        if (schedule == strongSelf.selectedDay) {
            [strongSelf.daysCollectionView selectItemAtIndexPath:indexPath animated:NO scrollPosition:UICollectionViewScrollPositionNone];
            [cell setSelected:YES];
        }
    };
    ((UICollectionViewFlowLayout *)self.daysCollectionView.collectionViewLayout).estimatedItemSize = CGSizeMake(60, 60);
    self.daysCollectionViewDataSource = [[PRVSectionedDataSource alloc] initWithItems:[self.program splitScheduleByWeeks] cellIdentifier:kDayCollectionViewCellIdentifier configureCellBlock:configureCell];
    self.daysCollectionView.dataSource = self.daysCollectionViewDataSource;
    [self.daysCollectionView registerNib:[PRVDayCollectionViewCell prv_nib] forCellWithReuseIdentifier:kDayCollectionViewCellIdentifier];
    [self.daysCollectionView reloadData];
}

@end
