//
//  PRVClubVC.m
//  FitnessApp
//
//  Created by Ruslan on 7/16/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//
#import "PRVClubVC.h"
#import "PRVClubVC+Navigation.h"
#import "PRVClubVC+Setup.h"
#import "PRVDataSource.h"
#import "PRVClubsServerModel.h"
#import "PRVServerManager.h"
#import "PRVOptionsServerModel.h"
#import "PRVClubPreviewView.h"
#import "PRVUserServerModel.h"
#import "PRVAppManager.h"
#import <NSAttributedString-DDHTML/NSAttributedString+DDHTML.h>
#import "PRVPaymentVC.h"
#import "PRVDataManager.h"
#import "PRVActiveTicketView.h"
#import "PRVEnrollmentView.h"
#import "PRVImagePager.h"

@interface PRVClubVC () <PRVEnrollmentViewDelegate, PRVImagePagerDataSource>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *enrollmentViewHeightConstraint;

@property (nonatomic, weak) IBOutlet PRVClubPreviewView *clubPreviewView;
@property (nonatomic, weak) IBOutlet PRVActiveTicketView *activeTicketView;
@property (nonatomic, weak) IBOutlet UIView *commentsView;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *commentsTableViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *reviewView;
@property (weak, nonatomic) IBOutlet UIButton *reviewButton;
@property (weak, nonatomic) IBOutlet PRVEnrollmentView *enrollmentView;
@property (weak, nonatomic) IBOutlet UIView *errorView;
@property (nonatomic, weak) IBOutlet PRVImagePager *sliderView;
@property (weak, nonatomic) IBOutlet UIView *coachInfoView;
@end

@implementation PRVClubVC

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Клуб";
    [self setupCommentsTableView];
    self.sliderView.dataSource = self;
    self.sliderView.imageCounterDisabled = YES;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.scrollView.hidden = YES;
    [self.reviewButton setTitleColor:[UIColor primaryColor] forState:UIControlStateNormal];
    
    // Setup active ticket view
    __weak typeof(self) weakSelf = self;
    self.activeTicketView.ticketBlock = ^(PRVTicketSeverModel *ticket) {
        typeof(self) strongSelf = weakSelf;
        [strongSelf ticketPressed:ticket];
    };
    self.enrollmentView.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self checkActiveTickets];
    [self showAddReviewViewIfNeeded];
}

- (void)dealloc {
    [self.commentsTableView removeObserver:self forKeyPath:@"contentSize"];
}

- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    self.reviewButton.layer.cornerRadius = 4.f;
    self.reviewButton.layer.borderWidth = 1.f;
    self.reviewButton.layer.borderColor = [UIColor primaryColor].CGColor;
    self.reviewButton.layer.masksToBounds = YES;
}


#pragma mark - PRVImagePagerDataSource

- (NSArray *) arrayWithImages:(PRVImagePager*)pager{
    return self.club.images;
}

- (UIViewContentMode) contentModeForImage:(NSUInteger)image inPager:(PRVImagePager*)pager{
    return UIViewContentModeScaleAspectFill;
}

#pragma mark - Observation

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    if(object == self.commentsTableView && [keyPath isEqualToString:@"contentSize"]) {
        self.commentsTableViewHeightConstraint.constant = self.commentsTableView.contentSize.height;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view setNeedsUpdateConstraints];
        });
    }
}

#pragma mark - Setup

- (void)configureViews{
    [self.clubPreviewView configureForClub:self.club];
    self.coachInfoView.hidden = !self.club.coaches.count;
    if (self.club.first_free) {
        self.enrollmentViewHeightConstraint.constant = 110 + 26;
    } else {
        self.enrollmentViewHeightConstraint.constant = 110;
    }
    [self.view layoutIfNeeded];
}

- (void)showAddReviewViewIfNeeded{
    if ([[PRVAppManager sharedInstance] getUser] && !self.club.user_review) {
        self.reviewView.hidden = NO;
    }
    self.reviewView.hidden = YES;
}

- (void)showCommentsViewIfNeeded{
    if (self.club.reviews.count) {
        [self.commentsTableView reloadData];
        self.commentsView.hidden = NO;
    } else {
        self.commentsView.hidden = YES;
    }
}

#pragma mark - Methods

- (void)ticketPressed:(PRVTicketSeverModel *)ticket{
    if (!ticket.isPayment) {
        PRVPaymentVC *vc = [[PRVPaymentVC alloc] prv_initWithNib];
        vc.ticket = ticket;
        UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:vc];
        [self presentViewController:navVC animated:YES completion:nil];
    }
}

- (void)checkActiveTickets{
    self.activeTicketView.hidden = YES;
    if (![[PRVAppManager sharedInstance] getUser]) {
        return;
    }
    PRVTicketSeverModel *ticket = [[[PRVAppManager sharedInstance] getUser] ticketForClub:self.club._id];
    if (ticket) {
        [self.activeTicketView prepareViewForTicket:ticket];
        self.activeTicketView.hidden = NO;
    }
}

- (void)loadClubWithID:(NSInteger)clubId lat:(double)lat lng:(double)lng{
    self.scrollView.hidden = YES;
    self.errorView.hidden = YES;
    [self prv_addMBProgressHUD];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"d.MM.y";
    NSString *today = [formatter stringFromDate:[NSDate date]];
    
        __weak typeof(self) weakSelf = self;
    void (^successBlock)(PRVClubServerModel *club, NSString *error) = ^(PRVClubServerModel *club, NSString *error){
        typeof (self) strongSelf = weakSelf;
        strongSelf.club = club;
        [strongSelf prv_hideMBProgressHUD];
    };
    void (^failureBlock)(NSError *error, NSInteger statusCode) = ^(NSError *error, NSInteger statusCode){
        typeof (self) strongSelf = weakSelf;
        strongSelf.errorView.hidden = NO;
        [strongSelf prv_hideMBProgressHUD];
        UIAlertController *alertVC = [UIAlertController prv_alertWithTitle:@"Ошибка" message:error.localizedDescription cancelButtonTitle:@"Ok" cancelBlock:nil];
        [strongSelf presentViewController:alertVC animated:YES completion:nil];
    };
    [[PRVServerManager sharedManager] getClubWithId:clubId userlat:lat userLng:lng currentDate:today  onSuccess:successBlock onFailure:failureBlock];
}

#pragma mark - Setters

- (void)setClub:(PRVClubServerModel *)club{
    _club = club;
    [self.enrollmentView configureViewFor:club];
    [self.sliderDataSource setItems:club.images];
    [self.commentsTableViewDataSource setItems:club.reviews];
    [self.sliderView reloadData];
    [self configureViews];
    [self checkActiveTickets];
    [self showCommentsViewIfNeeded];
    [self showAddReviewViewIfNeeded];
    self.scrollView.hidden = NO;
}

#pragma mark - Actions

- (IBAction)coachesInfoButtonPressed:(id)sender {
    [self showCoachListViewController];
}

- (IBAction)siteButtonPressed:(id)sender {
    if (self.club.site.length) {
        [self showSafariViewControllerWithUrl:self.club.site];
    }
}

- (IBAction)addToFavoriteButtonPressed:(id)sender {
    __weak typeof(self) weakSelf = self;
    PRVDataManager *dataManager = [[PRVDataManager alloc] init];
    if ([dataManager isExistClubForId:self.club._id]) {
        UIAlertController *alerVC = [UIAlertController alertControllerWithTitle:nil message:@"Клуб уже добавлен в избранные" preferredStyle:UIAlertControllerStyleAlert];
        [alerVC prv_addActionWithTitle:@"Ок" style:UIAlertActionStyleDefault actionBlock:nil];
        [self presentViewController:alerVC animated:YES completion:nil];
        return;
    }
    [dataManager addClubWithId:self.club._id name:self.club.name address:self.club.address rating:self.club.rating image:self.club.logo completionBlock:^(NSError *error) {
        typeof(self) strongSelf = weakSelf;
        NSString *message;
        if (error) {
            message = error.localizedDescription;
        } else {
            message = @"Клуб добавлен в избранные";
        }
        UIAlertController *alerVC = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleAlert];
        [alerVC prv_addActionWithTitle:@"Ок" style:UIAlertActionStyleDefault actionBlock:nil];
        [strongSelf presentViewController:alerVC animated:YES completion:nil];
    }];
}

- (IBAction)callButtonPressed:(id)sender {
    if (!self.club.phones.count) {
        return;
    }
    if (self.club.phones.count == 1) {
        [self callPhoneNumber:self.club.phones[0].phone];
    } else {
        __weak typeof(self) weakSelf = self;
        [self showPhoneListViewControllerForPhones:self.club.phones phoneBlock:^(NSString *phoneNumber) {
            typeof(self) strongSelf = weakSelf;
            [strongSelf callPhoneNumber:phoneNumber];
        }];
    }
}

- (void)callPhoneNumber:(NSString *)phoneNumber{
    NSString *onlyDigits = [[phoneNumber componentsSeparatedByCharactersInSet:
                                                     [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                                                    componentsJoinedByString:@""];
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@", onlyDigits]];
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl options:@{} completionHandler:nil];
    } else {
        UIAlertController *vc = [UIAlertController prv_alertWithTitle:nil message:@"Вы не можете совершать звонок с данного устройства" cancelButtonTitle:@"Ок" cancelBlock:nil];
        [self presentViewController:vc animated:YES completion:nil];
    }
}

- (IBAction)addReviewButtonPressed:(id)sender {
    [self showAddReviewViewController];
}

- (IBAction)clubInfoButtonPressed:(id)sender {
    [self showClubDetailViewController];
}

- (BOOL)hidesBottomBarWhenPushed
{
    return YES;
}

#pragma mark - PRVEnrollmentViewDelegate

- (void)enrollSelected:(PRVEnrollmentView *)enrollmentView{
    if ([self.club areAllGroupsWithFreeSchdeule]) {
        [self showSelectDayViewController];
    } else {
        [self showGroupListViewController];
    }
}

- (void)enrollmentView:(PRVEnrollmentView *)enrollmentView didSelectGroup:(PRVGroupServerModel *)group{
    [self showProgramListViewControllerForGroup:group];
}

- (void)freeScheduleSelected:(PRVEnrollmentView *)enrollmentView{
    [self showProgramListViewControllerWithFreeSchedule];
}

- (void)coachSelected:(PRVEnrollmentView *)enrollmentView{
    [self showCoachListViewControllerForPersonalTraining];
}

@end
