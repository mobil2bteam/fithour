//
//  PRVProgramDetailVC.h
//  FitnessApp
//
//  Created by Ruslan on 7/29/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PRVProgramServerModel;
@class PRVClubServerModel;
@class PRVGroupServerModel;
@class PRVDataSource;
@class PRVScheduleServerModel;
@class PRVSectionedDataSource;
@class PRVCoachServerModel;

@interface PRVProgramDetailVC : UIViewController

@property (nonatomic, strong) PRVProgramServerModel *program;
@property (nonatomic, strong) PRVClubServerModel *club;
@property (nonatomic, strong) PRVGroupServerModel *group;

@property (nonatomic, weak) IBOutlet UICollectionView *scheduleCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *daysCollectionView;

@property (nonatomic, strong) PRVDataSource *scheduleCollectionViewDataSource;
@property (nonatomic, strong) PRVSectionedDataSource *daysCollectionViewDataSource;
@property (nonatomic, strong) PRVScheduleServerModel *selectedDay;

@end
