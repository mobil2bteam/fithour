//
//  PRVProgramTableViewCell+Configuration.h
//  FitnessApp
//
//  Created by Ruslan on 8/24/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVProgramTableViewCell.h"

@class PRVProgramServerModel;
@class PRVGroupServerModel;

@interface PRVProgramTableViewCell (Configuration)

- (void)configureCellWithProgram:(PRVProgramServerModel *)program;

- (void)configureCellWithGroup:(PRVGroupServerModel *)group;

@end
