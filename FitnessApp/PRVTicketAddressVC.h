//
//  PRVTicketAddressVC.h
//  FitnessApp
//
//  Created by Ruslan on 8/19/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//
@class PRVTicketSeverModel;

#import <UIKit/UIKit.h>

@interface PRVTicketAddressVC : UIViewController

@property (nonatomic, strong) PRVTicketSeverModel *ticket;

@end
