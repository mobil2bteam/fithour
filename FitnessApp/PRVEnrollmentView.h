//
//  PRVEnrollmentView.h
//  FitnessApp
//
//  Created by Ruslan on 10/26/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PRVClubServerModel;
@class PRVEnrollmentView;
@class PRVGroupServerModel;

@protocol PRVEnrollmentViewDelegate <NSObject>

@optional
- (void)enrollmentView:(PRVEnrollmentView *)enrollmentView didSelectGroup:(PRVGroupServerModel *)group;
- (void)enrollSelected:(PRVEnrollmentView *)enrollmentView;
- (void)freeScheduleSelected:(PRVEnrollmentView *)enrollmentView;
- (void)coachSelected:(PRVEnrollmentView *)enrollmentView;

@end

@interface PRVEnrollmentView : UIView

@property (nonatomic, weak) id <PRVEnrollmentViewDelegate> delegate;

- (void)configureViewFor:(PRVClubServerModel *)club;

@end
