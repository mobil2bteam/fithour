//
//  PRVGroupTableViewCell+Configuration.h
//  FitnessApp
//
//  Created by Ruslan on 8/29/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//
@class PRVGroupServerModel;

#import "PRVGroupTableViewCell.h"

@interface PRVGroupTableViewCell (Configuration)

- (void)configureCellForGroup:(PRVGroupServerModel *)group;

@end
