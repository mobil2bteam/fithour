//
//  PRVGroupListVC+Navigation.m
//  FitnessApp
//
//  Created by Ruslan on 8/28/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVGroupListVC+Navigation.h"
#import "PRVProgramListVC.h"
#import "PRVProgramDetailVC.h"
#import "PRVClubsServerModel.h"
#import "PRVOptionsServerModel.h"
#import "PRVSelectDayVC.h"
#import "PRVCoachesListVC.h"

@implementation PRVGroupListVC (Navigation)

- (void)showProgramListViewControllerForGroup:(PRVGroupServerModel *)group{
    if (group.programs.count == 1) {
        PRVProgramDetailVC *vc = [[PRVProgramDetailVC alloc] prv_initWithNib];
        vc.group = group;
        vc.club = self.club;
        vc.program = group.programs[0];
        [self.navigationController pushViewController:vc animated:YES];
    }
    if (group.programs.count > 1) {
        PRVProgramListVC *vc = [[PRVProgramListVC alloc] prv_initWithNib];
        vc.group = group;
        vc.club = self.club;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (void)showSelectDayViewController{
    PRVSelectDayVC *vc = [[PRVSelectDayVC alloc] prv_initWithNib];
    vc.club = self.club;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)showCoachListViewController{
    PRVCoachesListVC *vc = [[PRVCoachesListVC alloc] prv_initWithNib];
    vc.isPersonalTraining = YES;
    vc.club = self.club;
    [self.navigationController pushViewController:vc animated:YES];
}

@end
