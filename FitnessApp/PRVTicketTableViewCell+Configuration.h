//
//  PRVTicketTableViewCell+Configuration.h
//  FitnessApp
//
//  Created by Ruslan on 9/13/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVTicketTableViewCell.h"

@class PRVTicketSeverModel;

@interface PRVTicketTableViewCell (Configuration)

- (void)configureCellForTicket:(PRVTicketSeverModel *)ticket;

@end
