//
//  PRVProgramTableViewCell.m
//  FitnessApp
//
//  Created by Ruslan on 7/28/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVProgramTableViewCell.h"

@interface PRVProgramTableViewCell() <UICollectionViewDelegate>

@end

@implementation PRVProgramTableViewCell

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated{
    [super setHighlighted:highlighted animated:animated];
    CGFloat alpha = 0.35;
    if (highlighted) {
        alpha = 0.9;
    }
    [UIView animateWithDuration:0.15 animations:^{
        self.overlayView.alpha = alpha;
    }];
}

//- (CGSize)systemLayoutSizeFittingSize:(CGSize)targetSize
//        withHorizontalFittingPriority:(UILayoutPriority)horizontalFittingPriority
//              verticalFittingPriority:(UILayoutPriority)verticalFittingPriority{
//    CGSize size = [super systemLayoutSizeFittingSize:targetSize withHorizontalFittingPriority:horizontalFittingPriority verticalFittingPriority:verticalFittingPriority];
//    [self.scheduleView layoutIfNeeded];
//    size.height += CGRectGetHeight(self.scheduleView.frame);
//    return size;
//}

@end
