//
//  PRVProgramTableViewCell+Configuration.m
//  FitnessApp
//
//  Created by Ruslan on 8/24/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVProgramTableViewCell+Configuration.h"
#import "PRVProgramServerModel+Extension.h"
#import "UIImageView+PRVExtension.h"
#import "PRVScheduleView.h"
#import "PRVDayCollectionViewCell+Configuration.h"

@implementation PRVProgramTableViewCell (Configuration)

- (void)configureCellWithProgram:(PRVProgramServerModel *)program{
    if (program.images.count) {
        [self.programImageView prv_setImageWithURL:program.images[0]];
    }
    self.programNameLabel.text = [program.name uppercaseString];
    NSInteger price = [program price];
    NSInteger duration = [program duration];
    if (!program.free) {
        if (duration) {
            self.priceLabel.text = [NSString stringWithFormat:@"%ld мин./%ld.0 руб.", (long)duration, (long)price];
        } else {
            self.priceLabel.text = [NSString stringWithFormat:@"%ld.0 руб.", (long)price];
        }
    }
}

- (void)configureCellWithGroup:(PRVGroupServerModel *)group{
    if (group.images.count) {
        [self.programImageView prv_setImageWithURL:group.images[0]];
    }
    self.programNameLabel.text = [group.name uppercaseString];
    self.priceLabel.text = @"";
}

- (void)prepareForReuse{
    [super prepareForReuse];
    self.programImageView.image = nil;
    self.programNameLabel.text = @"";
    self.priceLabel.text = @"";
}

@end
