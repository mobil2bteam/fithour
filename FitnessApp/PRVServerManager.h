//
//  PRVServerManager.h
//  FitnessApp
//
//  Created by Ruslan on 7/10/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//
@class PRVClubsServerModel;
@class PRVUserServerModel;
@class PRVClubServerModel;
@class PRVTicketSeverModel;
@class PRVReviewServerModel;
@class PRVCoachScheduleServerModel;

#import <Foundation/Foundation.h>


@interface PRVServerManager : NSObject

+ (instancetype)sharedManager;

- (void)loadOptionsOnSuccess:(void(^)()) success
                   onFailure:(void(^)(NSError *error)) failure;

- (void)filterClubsWithParams:(NSDictionary *)params
                    onSuccess:(void(^)(PRVClubsServerModel *clubs, NSString *error)) success
                    onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure;

- (void)getClubWithId:(NSInteger)clubId
              userlat:(CGFloat)userLat
              userLng:(CGFloat)userLng
                 currentDate:(NSString *)currentDate // format - d.MM.y
                onSuccess:(void(^)(PRVClubServerModel *club, NSString *error)) success
                onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure;

- (void)logInWithParams:(NSDictionary *)params
              onSuccess:(void(^)(PRVUserServerModel *user, NSString *error)) success
              onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure;

- (void)signUpWithLogin:(NSString *)login
                  phone:(NSString *)phone
                  email:(NSString *)email
               password:(NSString *)password
               passport:(NSString *)passport
                  image:(UIImage *)image
                 image2:(UIImage *)image2
              onSuccess:(void(^)(PRVUserServerModel *user, NSString *error)) success
              onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure;

- (void)buyTicketWithParams:(NSDictionary *)params
                  onSuccess:(void(^)(PRVTicketSeverModel *ticket, NSString *error)) success
                  onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure;

- (void)payTicketWithID:(NSInteger)ticketID
              onSuccess:(void(^)(PRVTicketSeverModel *ticket, NSString *error)) success
              onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure;

- (void)cancelTicketWithID:(NSInteger)ticketID
              onSuccess:(void(^)(PRVTicketSeverModel *ticket, NSString *error)) success
              onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure;

- (void)changePasswordWithNewPassword:(NSString *)newPassword
              onSuccess:(void(^)(PRVUserServerModel *user, NSString *error)) success
              onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure;

- (void)restorePasswordWithEmail:(NSString *)email
                       onSuccess:(void(^)(NSString *message, NSString *error)) success
                       onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure;

- (void)addReviewWithText:(NSString *)text
                   rating:(NSInteger)rating
                   clubId:(NSInteger)clubId
                       onSuccess:(void(^)(PRVReviewServerModel *review, NSString *error)) success
                       onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure;

- (void)scheduleForCoachWithId:(NSInteger)coachId
                     onSuccess:(void(^)(PRVCoachScheduleServerModel *schedule, NSString *error)) success
                     onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure;

- (void)buyTicketForPersonalTrainingWithCoachId:(NSInteger)coachId
                                           date:(NSString *)date
                                           time:(NSString *)time
                  onSuccess:(void(^)(PRVTicketSeverModel *ticket, NSString *error)) success
                  onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure;

- (void)loadTicketsOnSuccess:(void(^)(void)) success;

- (void)getTicketWithID:(NSInteger)ticketID
              onSuccess:(void(^)(PRVTicketSeverModel *ticket, NSString *error)) success
              onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure;

#pragma mark - Support and Rating

- (void)rateAppWith:(NSInteger)rating
               name:(NSString *)name
              email:(NSString *)email
               text:(NSString *)text
          onSuccess:(void(^)()) success
          onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure;

- (void)supportWith:(NSString *)name
              email:(NSString *)email
               text:(NSString *)text
          onSuccess:(void(^)()) success
          onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure;

- (void)preSignUpWithParams:(NSDictionary *)params
                  onSuccess:(void(^)(void)) success
                  onFailure:(void(^)(NSString *error)) failure;
@end
