//
//  PRVCoachTableViewCell.m
//  FitnessApp
//
//  Created by Ruslan on 7/27/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVCoachTableViewCell.h"
#import "UIColor+PRVHexColor.h"
#import "PRVOptionsServerModel.h"
#import "PRVDirectionCollectionViewCell.h"
#import "UIImageView+PRVExtension.h"
#import "PRVCoachImageView.h"

@interface PRVCoachTableViewCell() <UICollectionViewDelegate, UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UIView *gradientView;

@end

@implementation PRVCoachTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.groupsCollectionView.dataSource = self;
    ((UICollectionViewFlowLayout *)self.groupsCollectionView.collectionViewLayout).itemSize = CGSizeMake(60, 30);
    [self.groupsCollectionView registerNib:[PRVDirectionCollectionViewCell prv_nib] forCellWithReuseIdentifier:@"PRVDirectionCollectionViewCell"];
}

- (void)prepareForReuse{
    self.coachImageView.image = [UIImage imageNamed:@"trainer_stock_icon"];
}

- (void)setGroups:(NSArray<PRVGroupServerModel *> *)groups{
    _groups = groups;
    [_groupsCollectionView reloadData];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.groups.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    PRVDirectionCollectionViewCell *cell = (PRVDirectionCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([PRVDirectionCollectionViewCell class]) forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    cell.layer.cornerRadius = 15;
    return cell;
}

- (void)configureCell:(PRVDirectionCollectionViewCell *)cell atIndexPath:(NSIndexPath *)indexPath{
    PRVGroupServerModel *group = self.groups[indexPath.item];
    [cell.iconImageView prv_setImageWithURL:group.icon];
}

@end
