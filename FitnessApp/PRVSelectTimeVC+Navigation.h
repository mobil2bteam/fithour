//
//  PRVSelectTimeVC+Navigation.h
//  FitnessApp
//
//  Created by Ruslan on 9/20/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVSelectTimeVC.h"

@interface PRVSelectTimeVC (Navigation)

- (void)showProgramConfirmViewControllerForDate:(NSDate *)date time:(NSString *)time;

@end
