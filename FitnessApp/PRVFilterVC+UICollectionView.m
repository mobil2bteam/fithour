//
//  PRVFilterVC+UICollectionView.m
//  FitnessApp
//
//  Created by Ruslan on 7/23/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVFilterVC+UICollectionView.h"
#import "PRVOptionsServerModel.h"
#import "PRVGroupCollectionViewCell.h"
#import "PRVFilterCollectionViewCell.h"
#import "PRVFilterManager.h"
#import "UIImageView+PRVExtension.h"
#import "UIColor+PRVHexColor.h"

@implementation PRVFilterVC (UICollectionView)

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (collectionView == self.groupsCollectionView) {
        return [self.filterManager getGroupsOptions].count;
    }
    if (collectionView == self.datesCollectionView) {
        return [self.filterManager getDatesOptions].count;
    }
    if (collectionView == self.timesCollectionView) {
        return [self.filterManager getTimesOptions].count;
    }
    if (collectionView == self.pricesCollectionView) {
        return [self.filterManager getPricesOptions].count;
    }
    return 0;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat width, height;
    height = 50.f;
    width = 120.f;
    if (collectionView == self.groupsCollectionView) {
        width = (CGRectGetWidth(self.groupsCollectionView.frame) - 10) / 2;
    }
    if (collectionView == self.timesCollectionView) {
        width = 80.f;
    }
    return CGSizeMake(width, height);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView == self.groupsCollectionView){
        PRVGroupCollectionViewCell *cell = (PRVGroupCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([PRVGroupCollectionViewCell class]) forIndexPath:indexPath];
        [self configureGroupCell:cell atIndexPath:indexPath];
        return cell;
    } else {
       PRVFilterCollectionViewCell *cell = (PRVFilterCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([PRVFilterCollectionViewCell class]) forIndexPath:indexPath];
        if (collectionView == self.datesCollectionView) {
            [self configureDateCell:cell atIndexPath:indexPath];
        }
        if (collectionView == self.timesCollectionView) {
            [self configureTimeCell:cell atIndexPath:indexPath];
        }
        if (collectionView == self.pricesCollectionView) {
            [self configurePriceCell:cell atIndexPath:indexPath];
        }
        return cell;
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView == self.datesCollectionView) {
        PRVDateModel *date = [self.filterManager getDatesOptions][indexPath.item];
        self.selectedDate = date;
        [self.filterManager setSelectedDate:self.filterManager.getDatesOptions[indexPath.item]];
        [self.timesCollectionView reloadData];
    }
    if (collectionView == self.timesCollectionView) {
        NSString *time = [self.filterManager getTimesOptions][indexPath.item];
        self.selectedTime = time;
    }
    if (collectionView == self.pricesCollectionView) {
        PRVPriceServerModel *price = [self.filterManager getPricesOptions][indexPath.item];
        self.selectedPrice = price;
    }
    if (collectionView == self.groupsCollectionView) {
        [self.selectedGroupsArray addObject:self.filterManager.getGroupsOptions[indexPath.item]];
    }
    [collectionView reloadItemsAtIndexPaths:@[indexPath]];
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView == self.datesCollectionView) {
        self.selectedDate = nil;
        self.selectedTime = nil;
    }
    if (collectionView == self.timesCollectionView) {
        self.selectedTime = nil;
    }
    if (collectionView == self.pricesCollectionView) {
        self.selectedPrice = nil;
    }
    if (collectionView == self.groupsCollectionView) {
        [self.selectedGroupsArray removeObject:self.filterManager.getGroupsOptions[indexPath.item]];
    }
    [collectionView reloadItemsAtIndexPaths:@[indexPath]];
}

- (void)configureGroupCell:(PRVGroupCollectionViewCell *)cell atIndexPath:(NSIndexPath *)indexPath{
    PRVGroupServerModel *group = [self.filterManager getGroupsOptions][indexPath.item];
    cell.groupLabel.text = group.name;
    if ([self.selectedGroupsArray containsObject:group]) {
        [cell setSelected:YES];
        [self.groupsCollectionView selectItemAtIndexPath:indexPath animated:YES scrollPosition:UICollectionViewScrollPositionNone];
        [cell.groupImageView prv_setImageWithURL:group.icon_select];
    } else {
        [cell setSelected:NO];
        [self.groupsCollectionView deselectItemAtIndexPath:indexPath animated:YES];
        [cell.groupImageView prv_setImageWithURL:group.icon];
    }
}

- (void)configureDateCell:(PRVFilterCollectionViewCell *)cell atIndexPath:(NSIndexPath *)indexPath{
    PRVDateModel *date = [self.filterManager getDatesOptions][indexPath.item];
    cell.nameLabel.text = date.text;
    if ([self.selectedDate.text isEqualToString:date.text]) {
        [cell setSelected:YES];
        [self.datesCollectionView selectItemAtIndexPath:indexPath animated:YES scrollPosition:UICollectionViewScrollPositionNone];
    } else {
        [cell setSelected:NO];
        [self.datesCollectionView deselectItemAtIndexPath:indexPath animated:YES];
    }
}

- (void)configureTimeCell:(PRVFilterCollectionViewCell *)cell atIndexPath:(NSIndexPath *)indexPath{
    NSString *time = [self.filterManager getTimesOptions][indexPath.item];
    cell.nameLabel.text = time;
    if ([self.selectedTime isEqualToString:time]) {
        [cell setSelected:YES];
        [self.timesCollectionView selectItemAtIndexPath:indexPath animated:YES scrollPosition:UICollectionViewScrollPositionNone];
    } else {
        [cell setSelected:NO];
        [self.timesCollectionView deselectItemAtIndexPath:indexPath animated:YES];
    }
}

- (void)configurePriceCell:(PRVFilterCollectionViewCell *)cell atIndexPath:(NSIndexPath *)indexPath{
    PRVPriceServerModel *price = [self.filterManager getPricesOptions][indexPath.item];
    cell.nameLabel.text = price.text;
    if ([self.selectedPrice.text isEqualToString:price.text]) {
        [cell setSelected:YES];
        [self.pricesCollectionView selectItemAtIndexPath:indexPath animated:YES scrollPosition:UICollectionViewScrollPositionNone];
    } else {
        [cell setSelected:NO];
        [self.pricesCollectionView deselectItemAtIndexPath:indexPath animated:YES];
    }
}

@end
