//
//  PRVSheduleView.m
//  FitnessApp
//
//  Created by Ruslan on 8/24/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVScheduleView.h"
#import "PRVDayCollectionViewCell+Configuration.h"

@interface PRVScheduleView() <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, strong) NSArray <NSArray <PRVScheduleServerModel *> *> *schedules;
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, copy) PRVScheduleViewConfigurationBlock configureBlock;

@end

@implementation PRVScheduleView

#pragma mark - Lifecycel

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupCollectionViewWithFrame:frame];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setupCollectionViewWithFrame:self.bounds];
    }
    return self;
}

- (void)setConfigureBlock:(PRVScheduleViewConfigurationBlock)configureBlock{
    _configureBlock = configureBlock;
}

#pragma mark - Setup

- (void)setAllowsSelection:(BOOL)allow{
    [self.collectionView setAllowsSelection:allow];
}

- (void)setupCollectionViewWithFrame:(CGRect)frame{
    self.collectionView.showsHorizontalScrollIndicator = NO;
    self.collectionView = [[UICollectionView alloc] initWithFrame:frame collectionViewLayout:[self customLayout]];
    [self.collectionView setAllowsSelection:NO];
    self.collectionView.autoresizingMask = (UIViewAutoresizingFlexibleWidth |
                                  UIViewAutoresizingFlexibleHeight);
    self.collectionView.showsHorizontalScrollIndicator = NO;
    self.collectionView.backgroundColor = [UIColor whiteColor];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    [self.collectionView registerNib:[UINib nibWithNibName:@"PRVDayCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"PRVDayCollectionViewCell"];
    [self addSubview:self.collectionView];
}

#pragma mark - Getter/setter

- (UICollectionViewFlowLayout *)customLayout{
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    layout.estimatedItemSize = CGSizeMake(60, CGRectGetHeight(self.bounds));
    layout.minimumLineSpacing = 0.f;
    layout.minimumInteritemSpacing = 0.f;
    layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    return layout;
}

- (void)setSchedules:(NSArray<NSArray<PRVScheduleServerModel *> *> *)schedules{
    _schedules = schedules;
    [_collectionView reloadData];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return _schedules.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _schedules[section].count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    PRVDayCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PRVDayCollectionViewCell" forIndexPath:indexPath];
    PRVScheduleServerModel *schedule = _schedules[indexPath.section][indexPath.row];
    [cell configureCellWithShedule:schedule];
    if (_configureBlock) {
        _configureBlock(cell, schedule);
    }
    if (indexPath.section % 2 == 0) {
        cell.backgroundColor = [UIColor whiteColor];
    } else {
        cell.backgroundColor = [UIColor prv_colorWithHex:@"#F3F3F3"];
    }
    if (schedule == self.selectedSchedule) {
        [cell setSelected:YES];
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath{
    self.selectedSchedule = nil;
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    if (indexPath.section % 2 == 0) {
        cell.backgroundColor = [UIColor whiteColor];
    } else {
        cell.backgroundColor = [UIColor prv_colorWithHex:@"#F3F3F3"];
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    if (self.delegate) {
        [self.delegate scheduleView:self didSelectSchedule:_schedules[indexPath.section][indexPath.row]];
        self.selectedSchedule = _schedules[indexPath.section][indexPath.row];
    }
}

@end
