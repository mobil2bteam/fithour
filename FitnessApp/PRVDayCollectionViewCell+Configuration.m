//
//  PRVDayCollectionViewCell+Configuration.m
//  FitnessApp
//
//  Created by Ruslan on 8/24/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVDayCollectionViewCell+Configuration.h"
#import "PRVOptionsServerModel.h"

@implementation PRVDayCollectionViewCell (Configuration)

- (void)configureCellWithShedule:(PRVScheduleServerModel *)schedule{
    self.dayLabel.text = [schedule formattedDay];
    self.timeLabel.text = [schedule formattedTime];
}

- (void)configureCellWithSheduleForDate:(PRVScheduleServerModel *)schedule{
    self.dayLabel.text = [schedule formattedData];
    self.timeLabel.text = [schedule formattedDay];
}

- (void)configureCellForDate:(NSDate *)date{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"d.MM";
    self.dayLabel.text = [formatter stringFromDate:date];
 
    NSDateFormatter *dayNameFormatter = [[NSDateFormatter alloc] init];
    [dayNameFormatter setDateFormat:@"EE"];
    self.timeLabel.text = [[dayNameFormatter stringFromDate:date] uppercaseString];
}

@end
