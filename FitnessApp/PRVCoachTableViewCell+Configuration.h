//
//  PRVCoachTableViewCell+Configuration.h
//  FitnessApp
//
//  Created by Ruslan on 9/20/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVCoachTableViewCell.h"

@class PRVCoachServerModel;

@interface PRVCoachTableViewCell (Configuration)

- (void)configureCellForCoach:(PRVCoachServerModel *)coach;

@end
