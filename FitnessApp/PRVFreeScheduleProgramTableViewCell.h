//
//  PRVFreeScheduleProgramTableViewCell.h
//  FitnessApp
//
//  Created by Ruslan on 27.11.2017.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PRVProgramServerModel;

@interface PRVFreeScheduleProgramTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *hoursLabel;
@property (weak, nonatomic) IBOutlet UILabel *programLabel;

- (void)configureCellWithProgram:(PRVProgramServerModel *)program forDate:(NSDate *)date;
@end
