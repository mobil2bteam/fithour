//
//  PRVCoachDetailVC.m
//  FitnessApp
//
//  Created by Ruslan on 7/27/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVCoachDetailVC.h"
#import "PRVCoachDetailVC+Navigation.h"
#import "UIImageView+PRVExtension.h"
#import "PRVClubsServerModel.h"
#import "PRVEnrollCoachVC.h"
#import "UILabel+PRVExtension.h"

@interface PRVCoachDetailVC ()
@property (weak, nonatomic) IBOutlet UIImageView *coachImageView;
@property (weak, nonatomic) IBOutlet UIImageView *coachBigImageView;
@property (weak, nonatomic) IBOutlet UILabel *coachNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *coachRankLabel;
@property (weak, nonatomic) IBOutlet UILabel *coachPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *coachDescriptionLabel;
@property (weak, nonatomic) IBOutlet UIButton *enrollButton;
@end

@implementation PRVCoachDetailVC

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = self.coach.name;
    [self prepareView];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.coachDescriptionLabel setHTML:self.coach._description];
}

#pragma mark - Methods

- (void)prepareView{
    self.enrollButton.backgroundColor = [UIColor primaryColor];
    self.coachNameLabel.text = self.coach.name;
    self.coachRankLabel.text = self.coach.rank;
    self.coachPriceLabel.textColor = [UIColor primaryColor];
    self.coachPriceLabel.text = [NSString stringWithFormat:@"%ld р./час", (long)self.coach.price_hour];
    if (self.coach.image.length) {
        [self.coachImageView prv_setImageWithURL:self.coach.image];
        [self.coachBigImageView prv_setImageWithURL:self.coach.image];
    }
    self.enrollButton.hidden = ![self.club hasGroupsWithFreeSchdeule];
}

#pragma mark - Actions

- (IBAction)enrollButtonPressed:(id)sender {
    /*
     PRVEnrollCoachVC *vc = [[PRVEnrollCoachVC alloc] prv_initWithNib];
    vc.coach = self.coach;
    vc.club = self.club;
    [self.navigationController pushViewController:vc animated:YES];
     */
    if (!self.club.phones.count) {
        return;
    }
    if (self.club.phones.count == 1) {
        [self callPhoneNumber:self.club.phones[0].phone];
    } else {
        __weak typeof(self) weakSelf = self;
        [weakSelf showPhoneListViewControllerForPhones:self.club.phones phoneBlock:^(NSString *phoneNumber) {
            typeof(self) strongSelf = weakSelf;
            [strongSelf callPhoneNumber:phoneNumber];
        }];
    }
}

- (void)callPhoneNumber:(NSString *)phoneNumber{
    NSString *onlyDigits = [[phoneNumber componentsSeparatedByCharactersInSet:
                             [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                            componentsJoinedByString:@""];
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@", onlyDigits]];
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl options:@{} completionHandler:nil];
    } else {
        UIAlertController *vc = [UIAlertController prv_alertWithTitle:nil message:@"Вы не можете совершать звонок с данного устройства" cancelButtonTitle:@"Ок" cancelBlock:nil];
        [self presentViewController:vc animated:YES completion:nil];
    }
}

@end
