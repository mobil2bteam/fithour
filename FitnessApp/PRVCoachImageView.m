//
//  PRVCoachImageView.m
//  FitnessApp
//
//  Created by Ruslan on 9/19/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVCoachImageView.h"
#import "UIView+PRVExtensions.h"

@interface PRVCoachImageView()
@property (nonatomic, strong) CAShapeLayer *gradienLayer;
@end

@implementation PRVCoachImageView

- (void)setGradientBorderWidth:(CGFloat)gradientBorderWidth{
    _gradientBorderWidth = gradientBorderWidth;
    _gradienLayer.lineWidth = gradientBorderWidth;
}

- (void)showGradientBorder:(BOOL)show{
    _gradienLayer.lineWidth = show ? _gradientBorderWidth : 0.f;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setup];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup{
    self.gradientBorderWidth = 3.f;
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = CGRectGetHeight(self.bounds) / 2;
    
    CGColorRef color1 = [UIColor secondColor].CGColor;
    CGColorRef color2 = [UIColor thirdColor].CGColor;

    NSArray *colors = [NSArray arrayWithObjects:(__bridge id _Nonnull)(color1), color2, nil];

    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.bounds;
    gradient.colors = colors;
    gradient.startPoint = CGPointMake(0.0, 0.0);
    gradient.endPoint = CGPointMake(1, 1);
    
    CAShapeLayer *shapeLayer = [[CAShapeLayer alloc] init];
    shapeLayer.lineWidth = self.gradientBorderWidth;
    shapeLayer.path = [UIBezierPath bezierPathWithRoundedRect:self.bounds cornerRadius:self.layer.cornerRadius].CGPath;
    shapeLayer.fillColor = nil;
    shapeLayer.strokeColor = [UIColor blackColor].CGColor;
    gradient.mask = shapeLayer;
    gradient.cornerRadius = CGRectGetHeight(self.bounds) / 2;
    gradient.masksToBounds = NO;
    [self.layer insertSublayer:gradient atIndex:0];
}

@end
