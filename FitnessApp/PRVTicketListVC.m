//
//  PRVTicketListVC.m
//  FitnessApp
//
//  Created by Ruslan on 7/20/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVTicketListVC.h"
#import "UIScrollView+EmptyDataSet.h"
#import "PRVTicketTableViewCell+Configuration.h"
#import "PRVUserServerModel.h"
#import "PRVAppManager.h"
#import "PRVTicketDetailVC.h"
#import "PRVClubsServerModel.h"
#import "PRVOptionsServerModel.h"

@interface PRVTicketListVC () <DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>

@property (weak, nonatomic) IBOutlet UITableView *ticketsTableView;
@property (nonatomic, strong) NSArray <PRVTicketSeverModel *> *tickets;

@end

@implementation PRVTicketListVC

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Мои билеты";
    NSString *cellIdentifier = NSStringFromClass([PRVTicketTableViewCell class]);
    [self.ticketsTableView registerNib:[UINib nibWithNibName:cellIdentifier bundle:nil] forCellReuseIdentifier:cellIdentifier];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.ticketsTableView reloadData];
}

- (NSArray <PRVTicketSeverModel *> *)tickets{
    NSMutableArray <PRVTicketSeverModel *> *temp = [[NSMutableArray alloc] init];
    for (PRVTicketSeverModel *ticket in [[PRVAppManager sharedInstance] getUser].info.tickets) {
        if (![ticket isCompleted] && ![ticket isCanceled]) {
            [temp addObject:ticket];
        }
    }
    _tickets = [temp copy];
    return _tickets;
}

#pragma mark - DZNEmptyDataSetSource

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"У вас пока нет билетов";
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:18.0f],
                                 NSForegroundColorAttributeName: [UIColor darkGrayColor]};
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return self.tickets.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PRVTicketTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PRVTicketTableViewCell class]) forIndexPath:indexPath];
    [cell configureCellForTicket:self.tickets[indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    PRVTicketDetailVC *vc = [[PRVTicketDetailVC alloc] prv_initWithNib];
    vc.ticket = self.tickets[indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (BOOL)hidesBottomBarWhenPushed
{
    return YES;
}

@end
