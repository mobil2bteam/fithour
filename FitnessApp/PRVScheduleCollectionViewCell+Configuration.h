//
//  PRVScheduleCollectionViewCell+Configuration.h
//  FitnessApp
//
//  Created by Ruslan on 8/24/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//
@class PRVHourServerModel;
@class PRVCoachServerModel;

#import "PRVScheduleCollectionViewCell.h"

@interface PRVScheduleCollectionViewCell (Configuration)

- (void)configureCellForHour:(PRVHourServerModel *)hour;
- (void)configureCellForCoach:(PRVCoachServerModel *)coach;

@end
