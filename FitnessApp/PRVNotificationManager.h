//
//  PRVNotificationManager.h
//  FitnessApp
//
//  Created by Ruslan on 9/25/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PRVTicketSeverModel;

@interface PRVNotificationManager : NSObject

+ (void)registerNotifications;

+ (void)addNotificationForTicket:(PRVTicketSeverModel *)ticket;

+ (void)removeNotificationForTicket:(PRVTicketSeverModel *)ticket;

+ (void)addNotificationWithAlert:(NSString *)alert message:(NSString *)message fireDate:(NSDate *)fireDate notificationId:(NSString *)notificationId;
@end
