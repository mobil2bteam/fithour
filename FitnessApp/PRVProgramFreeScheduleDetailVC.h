//
//  PRVProgramFreeScheduleDetailVC.h
//  FitnessApp
//
//  Created by Ruslan on 9/12/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PRVProgramServerModel;
@class PRVClubServerModel;
@class PRVGroupServerModel;

@interface PRVProgramFreeScheduleDetailVC : UIViewController

@property (nonatomic, strong) PRVProgramServerModel *program;
@property (nonatomic, strong) PRVClubServerModel *club;
@property (nonatomic, strong) PRVGroupServerModel *group;

@end
