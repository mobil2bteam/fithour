//
//  UIAlertController+PRVExtensions.h
//  FitnessApp
//
//  Created by Ruslan on 8/21/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertController (PRVExtensions)

+ (UIAlertController *)prv_alertWithTitle:(NSString *)title message:(NSString *)message
                        cancelButtonTitle:(NSString *)cancelButtonTitle cancelBlock:(void (^)())cancelBlock;

- (void)prv_addActionWithTitle:(NSString *)title style:(UIAlertActionStyle)style actionBlock:(void (^)())actionBlock;

@end
