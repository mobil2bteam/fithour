//
//  PRVTicketAddressVC.m
//  FitnessApp
//
//  Created by Ruslan on 8/19/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVTicketAddressVC.h"
#import "PRVUserServerModel.h"
#import "PRVClubsServerModel.h"
#import "PRVOptionsServerModel.h"

@import GoogleMaps;

@interface PRVTicketAddressVC ()

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *programLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet GMSMapView *mapView;

@end

@implementation PRVTicketAddressVC

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = self.ticket.club.name;
    self.addressLabel.text = self.ticket.club.address;
    self.timeLabel.text = [NSString stringWithFormat:@"%@ в %@ до %@", self.ticket.date, self.ticket.time_from, self.ticket.time_to];
    self.priceLabel.text = [NSString stringWithFormat:@"%ld руб.", (long)self.ticket.price];
    self.programLabel.text = self.ticket.program.name;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(self.ticket.club.lat, self.ticket.club.lng);
    GMSMarker *marker = [GMSMarker markerWithPosition:coordinate];
    marker.title = self.ticket.club.name;
    marker.icon = [UIImage imageNamed:@"map_pin_place"];
    marker.map = self.mapView;
    self.mapView.camera = [GMSCameraPosition cameraWithTarget:coordinate zoom:13];
    [self.mapView animateToLocation:coordinate];
}

@end
