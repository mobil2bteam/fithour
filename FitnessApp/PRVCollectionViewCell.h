//
//  PRVCollectionViewCell.h
//  FitnessApp
//
//  Created by Ruslan on 7/14/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PRVCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *gradientImageView;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@end
