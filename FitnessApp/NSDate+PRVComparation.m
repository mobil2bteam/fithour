//
//  NSDate+PRVComparation.m
//  FitnessApp
//
//  Created by Ruslan on 09.03.2018.
//  Copyright © 2018 Ruslan Palapa. All rights reserved.
//

#import "NSDate+PRVComparation.h"

@implementation NSDate (PRVComparation)

+ (NSDate *)dateFromStringDate:(NSString *)stringDate withDateFormat:(NSString *)dateFormat{
    NSDateFormatter *dateformatter = [[NSDateFormatter alloc] init];
    dateformatter.dateFormat = dateFormat;
    return [dateformatter dateFromString:stringDate];
}

- (BOOL)isEqualToDateByDayAndMonth:(NSDate *)otherDate{
    NSDateFormatter *dateformatter = [[NSDateFormatter alloc] init];
    dateformatter.dateFormat = @"d.MM.y";
    NSCalendar * calendar = [NSCalendar currentCalendar];
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay;
    NSDateComponents *comp1 = [calendar components:unitFlags fromDate:self];
    NSDateComponents *comp2 = [calendar components:unitFlags fromDate:otherDate];
    return comp1.year == comp2.year && comp1.month == comp2.month && comp1.day == comp2.day;
}

@end
