//
//  PRVCityListVC.h
//  FitnessApp
//
//  Created by Ruslan on 7/28/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//
@class PRVCityServerModel;

#import <UIKit/UIKit.h>

@interface PRVCityListVC : UIViewController

@property (nonatomic, copy) void (^callback)(PRVCityServerModel *selectedCity);

@end
