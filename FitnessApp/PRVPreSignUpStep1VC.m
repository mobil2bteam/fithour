//
//  PRVPreSignUpStep1VC.m
//  FitnessApp
//
//  Created by Ruslan on 16.07.2018.
//  Copyright © 2018 Ruslan Palapa. All rights reserved.
//

#import "PRVPreSignUpStep1VC.h"
#import "PRVServerManager.h"
#import "UIView+PRVExtensions.h"
#import "AFNetworking.h"
#import "AFHTTPSessionOperation.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "PRVPreSignUpStep2VC.h"
#import "PRVLogInVC.h"

@interface PRVPreSignUpStep1VC () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

@end

@implementation PRVPreSignUpStep1VC

- (void)viewDidLoad {
    [super viewDidLoad];
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc]initWithTitle:@"Закрыть" style:UIBarButtonItemStylePlain target:self action:@selector(dismissBarButtonPressed:)];
    self.navigationItem.rightBarButtonItem = rightButton;
}

#pragma mark - Actions

- (IBAction)dismissBarButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)nextButtonPressed:(id)sender {
    if (!self.emailTextField.text.length) {
        [self.emailTextField prv_shake];
        return;
    }
    if (!self.passwordTextField.text.length) {
        [self.passwordTextField prv_shake];
        return;
    }
    [self signUp];
}

- (IBAction)loginButtonPressed:(id)sender {
    PRVLogInVC *vc = [[PRVLogInVC alloc] prv_initWithNib];
    vc.hideSignUpButton = YES;
    vc.loginCallback = self.loginCallback;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)signUp {
    [self prv_addMBProgressHUD];
    __weak typeof(self) weakSelf = self;
    NSDictionary *params = @{@"email":self.emailTextField.text,
                             @"password": self.passwordTextField.text,
                             @"login":self.emailTextField.text};
    [[PRVServerManager sharedManager] preSignUpWithParams:params onSuccess:^{
        typeof(self) strongSelf = weakSelf;
        [strongSelf prv_hideMBProgressHUD];
        [strongSelf showStep2];
    } onFailure:^(NSString *error) {
        typeof(self) strongSelf = weakSelf;
        [strongSelf prv_showMessage:error withTitle:@"Ошибка"];
        [strongSelf prv_hideMBProgressHUD];
    }];
}

- (void)showStep2 {
    PRVPreSignUpStep2VC *vc = [[PRVPreSignUpStep2VC alloc] prv_initWithNib];
    vc.loginCallback = self.loginCallback;
    vc.email = self.emailTextField.text;
    vc.password = self.passwordTextField.text;
    vc.sighInForBuyTicket = self.sighInForBuyTicket;
    [self.navigationController pushViewController:vc animated:true];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.emailTextField) {
        [self.passwordTextField becomeFirstResponder];
    }
    [textField resignFirstResponder];
    return  YES;
}

@end
