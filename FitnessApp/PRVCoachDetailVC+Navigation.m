//
//  PRVCoachDetailVC+Navigation.m
//  FitnessApp
//
//  Created by Ruslan on 22.11.2017.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVCoachDetailVC+Navigation.h"
#import "PRVPhoneListAlertVC.h"

@implementation PRVCoachDetailVC (Navigation)

- (void)showPhoneListViewControllerForPhones:(NSArray <PRVPhoneServerModel *> *)phones phoneBlock:(void (^)(NSString *phoneNumber))phoneBlock{
    PRVPhoneListAlertVC *vc = [[PRVPhoneListAlertVC alloc] prv_initWithNib];
    vc.phones = phones;
    vc.phoneBlock = phoneBlock;
    vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:vc animated:YES completion:nil];
}

@end
