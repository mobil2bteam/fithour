//
//  PRVDirectionCollectionViewCell.m
//  FitnessApp
//
//  Created by Ruslan on 7/12/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVDirectionCollectionViewCell.h"
#import "PRVOptionsServerModel.h"
#import "UIImageView+PRVExtension.h"

@implementation PRVDirectionCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.layer.cornerRadius = 20;
    self.layer.borderWidth = 1.f;
    self.layer.borderColor = [UIColor colorWithWhite:0.914 alpha:1.0].CGColor;
}

- (void)prepareForReuse{
    self.iconImageView.image = nil;
    self.gradientImageView.alpha = 0;
    self.nameLabel.text = @"";
}

- (void)configureCellForGroup:(PRVGroupServerModel *)group{
    [self.iconImageView prv_setImageWithURL:group.icon];
}

@end
