//
//  PRVDataManager.h
//  FitnessApp
//
//  Created by Ruslan on 9/11/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PRVClubDataModel;

typedef void(^PRVDataSaveCompletionBlock)(NSError *);

@interface PRVDataManager : NSObject

- (BOOL)isExistClubForId:(NSInteger)clubId;

- (NSArray <PRVClubDataModel *> *)favoriteClubs;

- (void)removeClub:(PRVClubDataModel *)club completionBlock:(PRVDataSaveCompletionBlock)completionBlock;

- (void)addClubWithId:(NSInteger)clubId name:(NSString *)name address:(NSString *)address rating:(CGFloat)rating image:(NSString *)image completionBlock:(PRVDataSaveCompletionBlock)completionBlock;

@end
