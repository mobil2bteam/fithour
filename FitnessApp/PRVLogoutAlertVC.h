//
//  PRVLogoutAlertVC.h
//  FitnessApp
//
//  Created by Ruslan on 9/15/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PRVLogoutAlertVC : UIViewController

@property (nonatomic, copy) void (^logoutCallback)(void);

@end
