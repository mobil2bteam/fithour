//
//  PRVGroupCollectionViewCell.m
//  FitnessApp
//
//  Created by Ruslan on 7/23/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVGroupCollectionViewCell.h"

@implementation PRVGroupCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.groupLabel.textColor = [UIColor secondColorDark];
    self.contentView.backgroundColor = [UIColor colorWithRed:0.973 green:0.957 blue:1.0 alpha:1.0];
    self.layer.borderColor = [UIColor secondColorLight].CGColor;
    self.layer.borderWidth = 0.5f;
    self.layer.cornerRadius = 25.f;
}

- (void)setSelected:(BOOL)selected{
    [super setSelected:selected];
    if (selected) {
        self.contentView.backgroundColor = [UIColor secondColor];
        self.groupLabel.textColor = [UIColor whiteColor];
    } else {
        self.contentView.backgroundColor = [UIColor colorWithRed:0.973 green:0.957 blue:1.0 alpha:1.0];
        self.groupLabel.textColor = [UIColor secondColorDark];
    }
}

- (void)prepareForReuse{
    [super prepareForReuse];
    self.selected = NO;
    self.groupImageView.image = nil;
    self.groupLabel.text = @"";
}

@end
