//
//  NSDate+PRVComparation.h
//  FitnessApp
//
//  Created by Ruslan on 09.03.2018.
//  Copyright © 2018 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (PRVComparation)
- (BOOL)isEqualToDateByDayAndMonth:(NSDate *)otherDate;
+ (NSDate *)dateFromStringDate:(NSString *)stringDate withDateFormat:(NSString *)dateFormat;

@end
