//
//  PRVDraggableView.h
//  FitnessApp
//
//  Created by Ruslan on 8/30/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, PRVDraggableViewState) {
    PRVDraggableViewStateShowing,
    PRVDraggableViewStateShowingOnHalf,
    PRVDraggableViewStateHidden
};

@interface PRVDraggableView : UIView
@property (nonatomic, copy) void (^leftButtonBlock)(void);

- (void)showDraggableViewWithAnimation:(BOOL)animation;
- (void)showOnHalfDraggableViewWithAnimation:(BOOL)animation;
- (void)hideDraggableViewWithAnimation:(BOOL)animation;
- (PRVDraggableViewState)stateOfDraggableView;
- (void)addContentView:(UIView *)view;
- (void)setStatusTitle:(NSString *)title;
- (void)setLeftButtonTitle:(NSString *)title;
- (void)setLeftButtonHidden:(BOOL)hidden;

@end
