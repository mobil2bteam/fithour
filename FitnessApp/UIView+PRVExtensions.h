//
//  UIView+PRVExtensions.h
//  FitnessApp
//
//  Created by Ruslan on 8/18/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (PRVExtensions)

- (void)prv_shake;

@end
