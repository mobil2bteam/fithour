//
//  PRVLocationService.h
//  FitnessApp
//
//  Created by Ruslan on 9/7/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PRVLocationService <NSObject>

/**
 Method request current location from CLLocationManager. Result will be returned from CLLocationManagerDelegate method.
 */
- (void)obtainUserLocation;

@end
