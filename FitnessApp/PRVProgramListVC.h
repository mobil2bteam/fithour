//
//  PRVProgramListVC.h
//  FitnessApp
//
//  Created by Ruslan on 7/28/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//
@class PRVGroupServerModel;
@class PRVClubServerModel;
@class PRVDataSource;

#import <UIKit/UIKit.h>

@interface PRVProgramListVC : UIViewController
@property (nonatomic, strong) PRVGroupServerModel *group;
@property (nonatomic, strong) PRVClubServerModel *club;
@property (nonatomic, weak) IBOutlet UITableView *programsTableView;
@property (nonatomic, strong) PRVDataSource *programsDataSource;
@property (nonatomic, assign) BOOL isFreeSchedule;
@end
