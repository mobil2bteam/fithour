//
//  PRVStartVC.m
//  FitnessApp
//
//  Created by Ruslan on 7/10/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

static NSString * const kFirstLoadKey = @"firstLoad";

#import "PRVStartVC.h"
#import "PRVStartVC+Navigation.h"
#import "PRVServerManager.h"
#import "PRVAppManager.h"

@implementation PRVStartVC

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadData];
}

#pragma mark - Methods

- (void)loadData{
    __weak typeof(self) weakSelf = self;
    [self prv_addMBProgressHUD];
    [[PRVServerManager sharedManager] loadOptionsOnSuccess:^{
        [self checkIfFirstLoad];
    } onFailure:^(NSError *error) {
        [self prv_hideMBProgressHUD];
        [self prv_showMessage:error.localizedDescription withRepeatHandler:^(UIAlertAction *action) {
            [weakSelf loadData];
        }];
    }];
}

- (void)checkIfFirstLoad{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    // if it's not first start
    if ([userDefaults boolForKey:kFirstLoadKey]) {
        // if user has already chosen city
        if ([[PRVAppManager sharedInstance] getUserCity]) {
            [self prv_hideMBProgressHUD];
            [self showMainViewController];
        } else {
            // show VC with city list
            [self prv_hideMBProgressHUD];
            [self showGpsViewController];
        }
    } else {
        [self prv_hideMBProgressHUD];
        // show onboarding
        [userDefaults setBool:YES forKey:kFirstLoadKey];
        [userDefaults synchronize];
        NSString *someDate = @"06.05.2018";
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"d.MM.y"];
        if ([[NSDate date] compare:[dateFormatter dateFromString:someDate]] == NSOrderedAscending) {
            [self showOnboardingViewController];
        } else {
            [self showGpsViewController];
        }
    }
}

@end
