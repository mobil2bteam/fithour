//
//  PRVProgressView.m
//  FitnessApp
//
//  Created by Ruslan on 9/25/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVProgressView.h"
#import "SFCircleGradientView.h"
#import "PRVUserServerModel.h"

static const NSTimeInterval kProgressAnimationDuration = 0.25;

@interface PRVProgressView()

@property (nonatomic, strong) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet SFCircleGradientView *gradientView;
@property (weak, nonatomic) IBOutlet UIView *completeView;
@property (weak, nonatomic) IBOutlet UIView *minuteView;
@property (weak, nonatomic) IBOutlet UILabel *minuteLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, strong) PRVTicketSeverModel *ticket;

@end

@implementation PRVProgressView

#pragma mark - Lifecycle

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        NSString *className = NSStringFromClass([self class]);
        [[NSBundle mainBundle]loadNibNamed:className owner:self options:nil];
        [self addSubview:_contentView];
        _gradientView.lineWidth = 5.f;
        _gradientView.progress = 0.f;
        _contentView.frame = self.bounds;
    }
    return self;
}

#pragma mark - Methods

- (void)prepareViewForTicket:(PRVTicketSeverModel *)ticket{
    _ticket = ticket;
//    
//    _ticket.dateComplete = @"";
//    _ticket.dateActive = @"01.11.2017";
//    _ticket.time_from = @"15:31";
//    _ticket.time_to = @"15:32";
//    _ticket.date = @"01.11.2017";

    if ([ticket isCompleted]) {
        self.completeView.hidden = NO;
        self.gradientView.hidden = YES;
        self.minuteView.hidden = YES;
        return;
    }

    if ([ticket isActive]) {
        self.completeView.hidden = YES;
        self.minuteView.hidden = NO;
        self.statusLabel.text = @"осталось";
        if ([ticket isEnded]) {
            // идут штрафные минуты
            if ([ticket isFailTimeStarted]) {
                self.statusLabel.text = @"штрафные";
                [self runTicket];
            } else {
                // идет время на сборы, чтобы покинуть клуб
                self.statusLabel.text = @"на сборы";
                [self runTicket];
            }
        } else {
            if ([ticket isStarted]) {
                self.gradientView.hidden = NO;
                [self runTicket];
            } else {
                // время до начала
                self.statusLabel.text = @"до начала";
                self.minuteLabel.text = [NSString stringWithFormat:@"%ld", (long)[_ticket minutesToStartTraining]];
                self.gradientView.hidden = YES;
            }
        }
        return;
    }
    
    if ([ticket isNew]) {
        self.completeView.hidden = YES;
        self.gradientView.hidden = YES;
        self.minuteView.hidden = YES;
    }
}

#pragma mark - Timer

- (void)runTicket{
    [self stopTicket];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                  target:self
                                                selector:@selector(timerMethod)
                                                userInfo:nil
                                                 repeats:YES];
}

- (void)stopTicket{
    [self.timer invalidate];
    self.timer = nil;
}

- (void)timerMethod{
    // штрафные минуты идут
    if ([_ticket isFailTimeStarted]) {
        self.gradientView.startColor = [UIColor thirdColorDark];
        self.gradientView.endColor = [UIColor thirdColorDark];

        self.statusLabel.text = @"штрафные";
        self.minuteLabel.text = [NSString stringWithFormat:@"%ld", (long)[_ticket currentFailMinutes]];
        [self.gradientView setProgress:1.0 animateWithDuration:kProgressAnimationDuration];
        return;
    }
    
    // время на сборы
    if ([_ticket isEnded]) {
        self.gradientView.startColor = [UIColor whiteColor];
        self.gradientView.endColor = [UIColor thirdColor];
        
        self.statusLabel.text = @"на сборы";
        self.minuteLabel.text = [NSString stringWithFormat:@"%ld", (long)[_ticket currentMinutesToExit]];
        CGFloat progress = ([_ticket currentSecondsToExit] * 1.f) / [_ticket totalSecondsToExit];
        [self.gradientView setProgress:progress animateWithDuration:kProgressAnimationDuration];
        return;
    }
    
    // идет занятие
    self.gradientView.startColor = [UIColor secondPrimaryColor];
    self.gradientView.endColor = [UIColor primaryColor];
    
    self.minuteLabel.text = [NSString stringWithFormat:@"%ld", (long)[_ticket minutesToFinishTraining]];
    CGFloat progress = 1 - (([self.ticket secondsToFinishTraining] * 1.f)/ [self.ticket trainingDurationInSeconds]);
    [self.gradientView setProgress:progress animateWithDuration:kProgressAnimationDuration];

}

@end
