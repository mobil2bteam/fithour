//
//  PRVClubDataModel+CoreDataProperties.m
//  
//
//  Created by Ruslan on 9/11/17.
//
//

#import "PRVClubDataModel+CoreDataProperties.h"

@implementation PRVClubDataModel (CoreDataProperties)

+ (NSFetchRequest<PRVClubDataModel *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"PRVClubDataModel"];
}

@dynamic clubID;
@dynamic name;
@dynamic rating;
@dynamic image;
@dynamic address;

@end
